#!/bin/bash

function checkEnv() {
  if [[ -z "$DB_HOST" ]]; then
    echo "DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$DB_NAME" ]]; then
    echo "DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$DB_USER" ]]; then
    echo "DB_USER is not set"
    exit 1
  fi
  if [[ -z "$DB_PASSWORD" ]]; then
    echo "DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_HOST" ]]; then
    echo "ID_DB_HOST is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_NAME" ]]; then
    echo "ID_DB_NAME is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_USER" ]]; then
    echo "ID_DB_USER is not set"
    exit 1
  fi
  if [[ -z "$ID_DB_PASSWORD" ]]; then
    echo "ID_DB_PASSWORD is not set"
    exit 1
  fi
  if [[ -z "$NODE_ENV" ]]; then
    echo "NODE_ENV is not set"
    exit 1
  fi
}

function configureServer() {
  if [ ! -f .env ]; then
    envsubst '${NODE_ENV}
      ${DB_HOST}
      ${DB_NAME}
      ${DB_USER}
      ${DB_PASSWORD}
      ${ID_DB_HOST}
      ${ID_DB_NAME}
      ${ID_DB_USER}
      ${ID_DB_PASSWORD}' \
      < docker/env.tmpl > .env

    if [[ ! -z "$EVENTS_PROTO" ]] &&
      [[ ! -z "$EVENTS_USER" ]] &&
      [[ ! -z "$EVENTS_PASSWORD" ]] &&
      [[ ! -z "$EVENTS_HOST" ]] &&
      [[ ! -z "$EVENTS_PORT" ]]; then
      envsubst '${EVENTS_PROTO}
        ${EVENTS_USER}
        ${EVENTS_CLIENT_ID}
        ${EVENTS_PASSWORD}
        ${EVENTS_HOST}
        ${EVENTS_PORT}'\
        < docker/env-events.tmpl >> .env
    fi
  fi
}

if [ "$1" = 'start' ]; then
  # Validate if DB_HOST is set.
  checkEnv
  # Configure server
  configureServer
  # Start server
  node dist/main.js
fi

exec "$@"

import { INestApplication, Logger } from '@nestjs/common';
import { MessageHandler, ServerMqtt } from '@nestjs/microservices';
import {
  MQTT_SEPARATOR,
  MQTT_WILDCARD_ALL,
  MQTT_WILDCARD_SINGLE,
} from '@nestjs/microservices/constants';
import { MqttClientOptions } from '@nestjs/microservices/external/mqtt-options.interface';
import {
  ConfigService,
  EVENTS_CLIENT_ID,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
} from './config/config.service';

export const LISTENING_TO_EVENTS = 'Listening to events using MQTT';
export const RETRY_ATTEMPTS = 3;
export const RETRY_DELAY = 10;
export const MQTT_SHARE = '$';
export type MqttProtocol =
  | 'wss'
  | 'ws'
  | 'mqtt'
  | 'mqtts'
  | 'tcp'
  | 'ssl'
  | 'wx'
  | 'wxs';

export class CustomServerMqtt extends ServerMqtt {
  public matchMqttPattern(pattern: string, topic: string) {
    const patternSegments = pattern.split(MQTT_SEPARATOR);
    const topicSegments = topic.split(MQTT_SEPARATOR);

    const patternSegmentsLength = patternSegments.length;
    const topicSegmentsLength = topicSegments.length;
    const lastIndex = patternSegmentsLength - 1;

    for (let i = 0; i < patternSegmentsLength; i++) {
      const currentPattern = patternSegments[i];
      const patternChar = currentPattern[0];
      const currentTopic = topicSegments[i];
      if (!currentTopic && !currentPattern) {
        continue;
      }
      if (!currentTopic && currentPattern !== MQTT_WILDCARD_ALL) {
        return false;
      }
      if (patternChar === MQTT_WILDCARD_ALL) {
        return i === lastIndex;
      }
      if (
        patternChar === MQTT_SHARE &&
        pattern.split(MQTT_SEPARATOR).pop() === currentTopic
      ) {
        return currentTopic === topic;
      }
      if (
        patternChar !== MQTT_WILDCARD_SINGLE &&
        currentPattern !== currentTopic
      ) {
        return false;
      }
    }
    return patternSegmentsLength === topicSegmentsLength;
  }

  public getHandlerByPattern(pattern: string): MessageHandler | null {
    const route = this.getRouteFromPattern(pattern);
    if (this.messageHandlers.has(route)) {
      return this.messageHandlers.get(route) || null;
    }

    for (const [key, value] of this.messageHandlers) {
      if (this.matchMqttPattern(key, route)) {
        return value;
      }
      if (key.indexOf('+') === -1 && key.indexOf('#') === -1) {
        continue;
      }
    }
    return null;
  }
}

export const eventsServerFactory = (
  config: ConfigService,
): MqttClientOptions => {
  return {
    clientId: config.get(EVENTS_CLIENT_ID),
    protocol: config.get(EVENTS_PROTO) as MqttProtocol,
    username: config.get(EVENTS_USER),
    password: config.get(EVENTS_PASSWORD),
    hostname: config.get(EVENTS_HOST),
    port: Number(config.get(EVENTS_PORT)),
    protocolVersion: 5,
  };
};

export function setupEvents(app: INestApplication) {
  const config = app.get(ConfigService);
  const events = app.connectMicroservice({
    strategy: new CustomServerMqtt(eventsServerFactory(config)),
  });
  events
    .listen()
    .then(() => Logger.log(LISTENING_TO_EVENTS, CustomServerMqtt.name));
}

import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { ListStockLedgerDto } from '../../controllers/stock-ledger/list-stock-ledger.dto';
import { StockLedgerService } from '../../entities/stock-ledger/stock-ledger.service';
import { StockLedgerDto } from '../../entities/stock-ledger/stock-ledger-dto';
import { StockLedger } from '../../entities/stock-ledger/stock-ledger.interface';
import { StockLedgerUpdatedEvent } from '../../events/stock-ledger/stock-ledger-updated/stock-ledger-updated.event';
import { StockLedgerAddedEvent } from '../../events/stock-ledger/stock-ledger-added/stock-ledger-added.event';
import { StockLedgerRemovedEvent } from '../../events/stock-ledger/stock-ledger-removed/stock-ledger-removed.event';

@Injectable()
export class StockLedgerAggregateService extends AggregateRoot {
  constructor(private readonly stockLedger: StockLedgerService) {
    super();
  }

  async addSockLedger(stockLedgerPayload: StockLedgerDto) {
    const stockLedger = {} as StockLedger;
    Object.assign(stockLedger, stockLedgerPayload);
    stockLedger.uuid = uuidv4();
    this.apply(new StockLedgerAddedEvent(stockLedger));
    return of(stockLedger);
  }

  async getStockLedgerList(filter: ListStockLedgerDto) {
    return await this.stockLedger.list(filter);
  }

  async removeStockLedger(uuid: string) {
    const stockLedger = await this.stockLedger.findOne({ uuid });
    if (!stockLedger) {
      throw new NotFoundException();
    }
    this.apply(new StockLedgerRemovedEvent(stockLedger));
  }

  async updateStockLedger(updatePayload: StockLedgerDto) {
    const provider = await this.stockLedger.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new StockLedgerUpdatedEvent(updatePayload));
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { BinService } from '../../entities/bin/bin.service';
import { BinAggregateService } from './bin-aggregate.service';

describe('BinAggregateService', () => {
  let service: BinAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BinAggregateService,
        {
          provide: BinService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<BinAggregateService>(BinAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

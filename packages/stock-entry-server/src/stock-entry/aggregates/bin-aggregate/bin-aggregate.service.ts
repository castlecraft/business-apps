import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { BinService } from '../../entities/bin/bin.service';
import { ListBinDto } from '../../controllers/bin/list-bin.dto';
import { BinAddedEvent } from '../../events/bin/bin-added/bin-added.event';
import { BinRemovedEvent } from '../../events/bin/bin-removed/bin-removed.event';
import { BinUpdatedEvent } from '../../events/bin/bin-updated/bin-updated.event';
import { BinDto } from '../../entities/bin/bin-dto';
import { Bin } from '../../entities/bin/bin.interface';

@Injectable()
export class BinAggregateService extends AggregateRoot {
  constructor(private readonly bin: BinService) {
    super();
  }

  async addBin(binPayload: BinDto) {
    const bin = {} as Bin;
    Object.assign(bin, binPayload);
    bin.uuid = uuidv4();
    this.apply(new BinAddedEvent(bin));
    return of(bin);
  }

  async getBinList(filter: ListBinDto) {
    return await this.bin.list(filter);
  }

  async removeBin(uuid: string) {
    const bin = await this.bin.findOne({ uuid });
    if (!bin) {
      throw new NotFoundException();
    }
    this.apply(new BinRemovedEvent(bin));
  }

  async updateBin(updatePayload: BinDto) {
    const provider = await this.bin.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new BinUpdatedEvent(updatePayload));
  }
}

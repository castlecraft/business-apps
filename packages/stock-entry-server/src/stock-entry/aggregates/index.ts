import { BinAggregateService } from './bin-aggregate/bin-aggregate.service';
import { StockEntryAggregateService } from './stock-entry-aggregate/stock-entry-aggregate.service';
import { StockLedgerAggregateService } from './stock-ledger-aggregate/stock-ledger-aggregate.service';

export const StockEntryAggregatesManager = [
  StockEntryAggregateService,
  StockLedgerAggregateService,
  BinAggregateService,
];

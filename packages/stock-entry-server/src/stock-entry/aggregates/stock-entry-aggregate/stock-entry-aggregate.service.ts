import { Injectable, NotFoundException } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { of } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { StockEntryDto } from '../../entities/stock-entry/stock-entry-dto';
import { StockEntry } from '../../entities/stock-entry/stock-entry.interface';
import { StockEntryService } from '../../entities/stock-entry/stock-entry.service';
import { StockEntryAddedEvent } from '../../events/stock-entry/stock-entry-added/stock-entry-added.event';
import { StockEntryRemovedEvent } from '../../events/stock-entry/stock-entry-removed/stock-entry-removed.event';
import { StockEntryUpdatedEvent } from '../../events/stock-entry/stock-entry-updated/stock-entry-updated.event';
import { ListStockEntryDto } from '../../controllers/stock-entry/list-stock-entry.dto';

@Injectable()
export class StockEntryAggregateService extends AggregateRoot {
  constructor(private readonly stockEntry: StockEntryService) {
    super();
  }

  async addStockEntry(stockEntryPayload: StockEntryDto) {
    const stockEntry = {} as StockEntry;
    Object.assign(stockEntry, stockEntryPayload);
    stockEntry.uuid = uuidv4();
    this.apply(new StockEntryAddedEvent(stockEntry));
    return of(stockEntry);
  }

  async getStockEntryList(filter: ListStockEntryDto) {
    return await this.stockEntry.list(filter);
  }

  async removeStockEntry(uuid: string) {
    const stockEntry = await this.stockEntry.findOne({ uuid });
    if (!stockEntry) {
      throw new NotFoundException();
    }
    this.apply(new StockEntryRemovedEvent(stockEntry));
  }

  async updateStockEntry(updatePayload: StockEntryDto) {
    const provider = await this.stockEntry.findOne({
      uuid: updatePayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    this.apply(new StockEntryUpdatedEvent(updatePayload));
  }
}

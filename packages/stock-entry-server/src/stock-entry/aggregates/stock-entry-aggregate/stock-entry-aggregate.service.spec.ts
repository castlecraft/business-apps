import { Test, TestingModule } from '@nestjs/testing';
import { StockEntryService } from '../../entities/stock-entry/stock-entry.service';
import { StockEntryAggregateService } from './stock-entry-aggregate.service';

describe('StockEntryAggregateService', () => {
  let service: StockEntryAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StockEntryAggregateService,
        {
          provide: StockEntryService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<StockEntryAggregateService>(
      StockEntryAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { AddStockEntryHandler } from './stock-entry/add-stock-entry/add-stock-entry.handler';
import { RemoveStockEntryHandler } from './stock-entry/remove-stock-entry/remove-stock-entry.handler';
import { UpdateStockEntryHandler } from './stock-entry/update-stock-entry/update-stock-entry.handler';

export const StockEntryCommandManager = [
  AddStockEntryHandler,
  RemoveStockEntryHandler,
  UpdateStockEntryHandler,
];

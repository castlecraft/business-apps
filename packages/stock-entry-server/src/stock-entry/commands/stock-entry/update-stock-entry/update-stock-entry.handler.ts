import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { StockEntryAggregateService } from '../../../aggregates/stock-entry-aggregate/stock-entry-aggregate.service';
import { UpdateStockEntryCommand } from './update-stock-entry.command';

@CommandHandler(UpdateStockEntryCommand)
export class UpdateStockEntryHandler
  implements ICommandHandler<UpdateStockEntryCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: StockEntryAggregateService,
  ) {}

  async execute(command: UpdateStockEntryCommand) {
    const { updatePayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.updateStockEntry(updatePayload);
    aggregate.commit();
    return updatePayload;
  }
}

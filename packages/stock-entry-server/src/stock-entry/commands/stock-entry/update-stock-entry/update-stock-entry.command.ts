import { ICommand } from '@nestjs/cqrs';
import { StockEntryDto } from '../../../entities/stock-entry/stock-entry-dto';

export class UpdateStockEntryCommand implements ICommand {
  constructor(public readonly updatePayload: StockEntryDto) {}
}

import { ICommand } from '@nestjs/cqrs';
import { StockEntryDto } from '../../../entities/stock-entry/stock-entry-dto';

export class AddStockEntryCommand implements ICommand {
  constructor(public stockEntryPayload: StockEntryDto) {}
}

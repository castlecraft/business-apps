import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { StockEntryAggregateService } from '../../../aggregates/stock-entry-aggregate/stock-entry-aggregate.service';
import { AddStockEntryCommand } from './add-stock-entry.command';

@CommandHandler(AddStockEntryCommand)
export class AddStockEntryHandler
  implements ICommandHandler<AddStockEntryCommand>
{
  constructor(
    private publisher: EventPublisher,
    private manager: StockEntryAggregateService,
  ) {}
  async execute(command: AddStockEntryCommand) {
    const { stockEntryPayload: stockEntryPayload } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    const account = await aggregate.addStockEntry(stockEntryPayload);
    aggregate.commit();
    return account;
  }
}

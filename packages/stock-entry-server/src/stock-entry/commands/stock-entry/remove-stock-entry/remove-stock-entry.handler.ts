import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { StockEntryAggregateService } from '../../../aggregates/stock-entry-aggregate/stock-entry-aggregate.service';
import { RemoveStockEntryCommand } from './remove-stock-entry.command';

@CommandHandler(RemoveStockEntryCommand)
export class RemoveStockEntryHandler
  implements ICommandHandler<RemoveStockEntryCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: StockEntryAggregateService,
  ) {}
  async execute(command: RemoveStockEntryCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeStockEntry(uuid);
    aggregate.commit();
  }
}

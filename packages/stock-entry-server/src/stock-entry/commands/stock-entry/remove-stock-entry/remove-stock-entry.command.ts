import { ICommand } from '@nestjs/cqrs';

export class RemoveStockEntryCommand implements ICommand {
  constructor(public readonly uuid: string) {}
}

import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockEntryService } from '../../../entities/stock-entry/stock-entry.service';
import { StockEntryAddedEvent } from './stock-entry-added.event';

@EventsHandler(StockEntryAddedEvent)
export class StockEntryAddedHandler
  implements IEventHandler<StockEntryAddedEvent>
{
  constructor(private readonly stockEntryService: StockEntryService) {}
  async handle(event: StockEntryAddedEvent) {
    const { stockEntry } = event;
    await this.stockEntryService.create(stockEntry);
  }
}

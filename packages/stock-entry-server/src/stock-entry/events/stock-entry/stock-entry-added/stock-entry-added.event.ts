import { IEvent } from '@nestjs/cqrs';
import { StockEntry } from '../../../entities/stock-entry/stock-entry.interface';

export class StockEntryAddedEvent implements IEvent {
  constructor(public stockEntry: StockEntry) {}
}

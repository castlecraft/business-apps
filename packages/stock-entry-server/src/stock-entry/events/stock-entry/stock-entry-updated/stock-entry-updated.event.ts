import { IEvent } from '@nestjs/cqrs';
import { StockEntryDto } from '../../../entities/stock-entry/stock-entry-dto';

export class StockEntryUpdatedEvent implements IEvent {
  constructor(public updatePayload: StockEntryDto) {}
}

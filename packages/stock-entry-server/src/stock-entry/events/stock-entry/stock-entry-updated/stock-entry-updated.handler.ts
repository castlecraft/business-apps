import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockEntryService } from '../../../entities/stock-entry/stock-entry.service';
import { StockEntryUpdatedEvent } from './stock-entry-updated.event';

@EventsHandler(StockEntryUpdatedEvent)
export class StockEntryUpdatedHandler
  implements IEventHandler<StockEntryUpdatedEvent>
{
  constructor(private readonly object: StockEntryService) {}

  async handle(event: StockEntryUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}

import { IEvent } from '@nestjs/cqrs';
import { StockEntry } from '../../../entities/stock-entry/stock-entry.interface';

export class StockEntryRemovedEvent implements IEvent {
  constructor(public stockEntry: StockEntry) {}
}

import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockEntryService } from '../../../entities/stock-entry/stock-entry.service';
import { StockEntryRemovedEvent } from './stock-entry-removed.event';

@EventsHandler(StockEntryRemovedEvent)
export class StockEntryRemovedHandler
  implements IEventHandler<StockEntryRemovedEvent>
{
  constructor(private readonly stockEntryService: StockEntryService) {}
  async handle(event: StockEntryRemovedEvent) {
    const { stockEntry } = event;
    await this.stockEntryService.deleteOne({ uuid: stockEntry.uuid });
  }
}

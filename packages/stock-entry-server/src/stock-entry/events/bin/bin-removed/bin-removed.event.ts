import { IEvent } from '@nestjs/cqrs';
import { StockEntry } from '../../../entities/stock-entry/stock-entry.interface';

export class BinRemovedEvent implements IEvent {
  constructor(public bin: StockEntry) {}
}

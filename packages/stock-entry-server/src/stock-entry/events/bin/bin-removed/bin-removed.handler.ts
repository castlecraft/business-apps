import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BinService } from '../../../entities/bin/bin.service';
import { BinRemovedEvent } from './bin-removed.event';

@EventsHandler(BinRemovedEvent)
export class BinRemovedHandler implements IEventHandler<BinRemovedEvent> {
  constructor(private readonly binService: BinService) {}
  async handle(event: BinRemovedEvent) {
    const { bin } = event;
    await this.binService.deleteOne({ uuid: bin.uuid });
  }
}

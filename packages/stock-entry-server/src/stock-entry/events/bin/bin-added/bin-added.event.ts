import { IEvent } from '@nestjs/cqrs';
import { Bin } from '../../../entities/bin/bin.interface';

export class BinAddedEvent implements IEvent {
  constructor(public bin: Bin) {}
}

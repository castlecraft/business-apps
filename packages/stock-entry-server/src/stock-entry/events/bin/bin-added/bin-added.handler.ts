import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BinService } from '../../../entities/bin/bin.service';
import { BinAddedEvent } from './bin-added.event';

@EventsHandler(BinAddedEvent)
export class BinAddedHandler implements IEventHandler<BinAddedEvent> {
  constructor(private readonly binService: BinService) {}
  async handle(event: BinAddedEvent) {
    const { bin } = event;
    await this.binService.create(bin);
  }
}

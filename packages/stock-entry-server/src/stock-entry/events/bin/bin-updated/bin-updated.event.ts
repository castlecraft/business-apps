import { IEvent } from '@nestjs/cqrs';
import { BinDto } from '../../../entities/bin/bin-dto';

export class BinUpdatedEvent implements IEvent {
  constructor(public updatePayload: BinDto) {}
}

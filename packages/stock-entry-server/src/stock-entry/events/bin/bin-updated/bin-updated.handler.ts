import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { BinService } from '../../../entities/bin/bin.service';
import { BinUpdatedEvent } from './bin-updated.event';

@EventsHandler(BinUpdatedEvent)
export class BinUpdatedHandler implements IEventHandler<BinUpdatedEvent> {
  constructor(private readonly object: BinService) {}

  async handle(event: BinUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}

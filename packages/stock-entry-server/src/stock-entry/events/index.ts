import { BinAddedHandler } from './bin/bin-added/bin-added.handler';
import { BinRemovedHandler } from './bin/bin-removed/bin-removed.handler';
import { BinUpdatedHandler } from './bin/bin-updated/bin-updated.handler';
import { StockEntryAddedHandler } from './stock-entry/stock-entry-added/stock-entry-added.handler';
import { StockEntryRemovedHandler } from './stock-entry/stock-entry-removed/stock-entry-removed.handler';
import { StockEntryUpdatedHandler } from './stock-entry/stock-entry-updated/stock-entry-updated.handler';
import { StockLedgerAddedHandler } from './stock-ledger/stock-ledger-added/stock-ledger-added.handler';
import { StockLedgerRemovedHandler } from './stock-ledger/stock-ledger-removed/stock-ledger-removed.handler';
import { StockLedgerUpdatedHandler } from './stock-ledger/stock-ledger-updated/stock-ledger-updated.handler';

export const StockEntryEventManager = [
  StockEntryAddedHandler,
  StockEntryRemovedHandler,
  StockEntryUpdatedHandler,
  StockLedgerAddedHandler,
  StockLedgerRemovedHandler,
  StockLedgerUpdatedHandler,
  BinAddedHandler,
  BinRemovedHandler,
  BinUpdatedHandler,
];

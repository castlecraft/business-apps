import { IEvent } from '@nestjs/cqrs';
import { StockLedger } from '../../../entities/stock-ledger/stock-ledger.interface';

export class StockLedgerAddedEvent implements IEvent {
  constructor(public stockLedger: StockLedger) {}
}

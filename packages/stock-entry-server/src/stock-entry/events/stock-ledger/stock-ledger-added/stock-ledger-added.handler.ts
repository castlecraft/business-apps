import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockLedgerService } from '../../../entities/stock-ledger/stock-ledger.service';
import { StockLedgerAddedEvent } from './stock-ledger-added.event';

@EventsHandler(StockLedgerAddedEvent)
export class StockLedgerAddedHandler
  implements IEventHandler<StockLedgerAddedEvent>
{
  constructor(private readonly stockLedgerService: StockLedgerService) {}
  async handle(event: StockLedgerAddedEvent) {
    const { stockLedger } = event;
    await this.stockLedgerService.create(stockLedger);
  }
}

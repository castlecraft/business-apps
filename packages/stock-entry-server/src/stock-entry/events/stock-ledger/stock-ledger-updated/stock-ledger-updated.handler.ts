import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockLedgerService } from '../../../entities/stock-ledger/stock-ledger.service';
import { StockLedgerUpdatedEvent } from './stock-ledger-updated.event';

@EventsHandler(StockLedgerUpdatedEvent)
export class StockLedgerUpdatedHandler
  implements IEventHandler<StockLedgerUpdatedEvent>
{
  constructor(private readonly object: StockLedgerService) {}

  async handle(event: StockLedgerUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}

import { IEvent } from '@nestjs/cqrs';
import { StockLedgerDto } from '../../../entities/stock-ledger/stock-ledger-dto';

export class StockLedgerUpdatedEvent implements IEvent {
  constructor(public updatePayload: StockLedgerDto) {}
}

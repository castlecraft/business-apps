import { IEvent } from '@nestjs/cqrs';
import { StockLedger } from '../../../entities/stock-ledger/stock-ledger.interface';

export class StockLedgerRemovedEvent implements IEvent {
  constructor(public stockLedger: StockLedger) {}
}

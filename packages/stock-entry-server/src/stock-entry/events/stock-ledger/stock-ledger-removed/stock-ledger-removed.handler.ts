import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { StockLedgerService } from '../../../entities/stock-ledger/stock-ledger.service';
import { StockLedgerRemovedEvent } from './stock-ledger-removed.event';

@EventsHandler(StockLedgerRemovedEvent)
export class StockLedgerRemovedHandler
  implements IEventHandler<StockLedgerRemovedEvent>
{
  constructor(private readonly stockLedgerService: StockLedgerService) {}
  async handle(event: StockLedgerRemovedEvent) {
    const { stockLedger } = event;
    await this.stockLedgerService.deleteOne({ uuid: stockLedger.uuid });
  }
}

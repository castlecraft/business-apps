import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { StockEntryCommandManager } from './commands';
import { StockEntryEntities, StockEntryEntityServices } from './entities';
import { StockEntryEventManager } from './events';
import { StockEntryQueryManager } from './queries';
import { StockEntryAggregatesManager } from './aggregates';
import { StockEntryPoliciesManager } from './policies';
import { StockEntryControllerManager } from './controllers';

@Module({
  imports: [CommonModule],
  providers: [
    ...StockEntryEntities,
    ...StockEntryEntityServices,
    ...StockEntryCommandManager,
    ...StockEntryEventManager,
    ...StockEntryQueryManager,
    ...StockEntryAggregatesManager,
    ...StockEntryPoliciesManager,
  ],
  controllers: [...StockEntryControllerManager],
  exports: [...StockEntryEntityServices],
})
export class StockEntryModule {}

import { IQuery } from '@nestjs/cqrs';
import { ListStockLedgerDto } from '../../../controllers/stock-ledger/list-stock-ledger.dto';

export class RetrieveStockLedgerListQuery implements IQuery {
  constructor(public readonly filter: ListStockLedgerDto) {}
}

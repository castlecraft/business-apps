import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { StockLedgerAggregateService } from '../../../aggregates/stock-ledger-aggregate/stock-ledger-aggregate.service';
import { RetrieveStockLedgerListQuery } from './retrieve-stock-ledger-list.query';

@QueryHandler(RetrieveStockLedgerListQuery)
export class RetrieveStockLedgerHandler
  implements IQueryHandler<RetrieveStockLedgerListQuery>
{
  constructor(private readonly manager: StockLedgerAggregateService) {}
  async execute(query: RetrieveStockLedgerListQuery) {
    return await this.manager.getStockLedgerList(query.filter);
  }
}

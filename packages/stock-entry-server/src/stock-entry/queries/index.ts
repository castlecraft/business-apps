import { RetrieveBinHandler } from './bin/list-bin/retrieve-bin-list.handler';
import { RetrieveStockEntryHandler } from './stock-entry/list-stock-entry/retrieve-stock-entry-list.handler';
import { RetrieveStockLedgerHandler } from './stock-ledger/list-stock-ledger/retrieve-stock-ledger-list.handler';

export const StockEntryQueryManager = [
  RetrieveStockEntryHandler,
  RetrieveStockLedgerHandler,
  RetrieveBinHandler,
];

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { BinAggregateService } from '../../../aggregates/bin-aggregate/bin-aggregate.service';
import { RetrieveBinListQuery } from './retrieve-bin-list.query';

@QueryHandler(RetrieveBinListQuery)
export class RetrieveBinHandler implements IQueryHandler<RetrieveBinListQuery> {
  constructor(private readonly manager: BinAggregateService) {}
  async execute(query: RetrieveBinListQuery) {
    return await this.manager.getBinList(query.filter);
  }
}

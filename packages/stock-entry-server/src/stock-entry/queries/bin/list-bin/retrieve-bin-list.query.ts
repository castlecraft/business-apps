import { IQuery } from '@nestjs/cqrs';
import { ListBinDto } from '../../../controllers/bin/list-bin.dto';

export class RetrieveBinListQuery implements IQuery {
  constructor(public readonly filter: ListBinDto) {}
}

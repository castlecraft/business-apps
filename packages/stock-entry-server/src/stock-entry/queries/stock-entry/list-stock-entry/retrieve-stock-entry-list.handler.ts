import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { StockEntryAggregateService } from '../../../aggregates/stock-entry-aggregate/stock-entry-aggregate.service';
import { RetrieveStockEntryListQuery } from './retrieve-stock-entry-list.query';

@QueryHandler(RetrieveStockEntryListQuery)
export class RetrieveStockEntryHandler
  implements IQueryHandler<RetrieveStockEntryListQuery>
{
  constructor(private readonly manager: StockEntryAggregateService) {}
  async execute(query: RetrieveStockEntryListQuery) {
    return await this.manager.getStockEntryList(query.filter);
  }
}

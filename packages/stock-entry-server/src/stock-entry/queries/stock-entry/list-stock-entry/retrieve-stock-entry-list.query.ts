import { IQuery } from '@nestjs/cqrs';
import { ListStockEntryDto } from '../../../controllers/stock-entry/list-stock-entry.dto';

export class RetrieveStockEntryListQuery implements IQuery {
  constructor(public readonly filter: ListStockEntryDto) {}
}

import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddStockEntryCommand } from '../../commands/stock-entry/add-stock-entry/add-stock-entry.command';
import { RemoveStockEntryCommand } from '../../commands/stock-entry/remove-stock-entry/remove-stock-entry.command';
import { UpdateStockEntryCommand } from '../../commands/stock-entry/update-stock-entry/update-stock-entry.command';
import { StockEntryDto } from '../../entities/stock-entry/stock-entry-dto';
import { RetrieveStockEntryListQuery } from '../../queries/stock-entry/list-stock-entry/retrieve-stock-entry-list.query';
import { ListStockEntryDto } from './list-stock-entry.dto';

@Controller('stock_entry')
export class StockEntryController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async createStockEntry(@Body() stockEntryPayload: StockEntryDto, @Req() req) {
    return await this.commandBus.execute(
      new AddStockEntryCommand(stockEntryPayload),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getStockEntryList(@Query() query: ListStockEntryDto) {
    return await this.queryBus.execute(new RetrieveStockEntryListQuery(query));
  }

  @Post('v1/remove/:uuid')
  removeStockEntry(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveStockEntryCommand(uuid));
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateAccount(@Body() updatePayload: StockEntryDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateStockEntryCommand(updatePayload),
    );
  }
}

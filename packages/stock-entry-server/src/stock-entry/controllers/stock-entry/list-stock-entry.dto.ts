import { IsNumberString, IsOptional, IsString } from 'class-validator';
import { StockEntryDto } from '../../entities/stock-entry/stock-entry-dto';

export class ListStockEntryDto {
  // add as per stock entry fields
  @IsOptional()
  docs: StockEntryDto[];

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

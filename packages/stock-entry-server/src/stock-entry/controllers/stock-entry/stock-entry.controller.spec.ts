import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { StockEntryController } from './stock-entry.controller';

describe('StockEntryController', () => {
  let controller: StockEntryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [StockEntryController],
      providers: [
        {
          provide: CommandBus,
          useValue: {},
        },
        {
          provide: QueryBus,
          useValue: {},
        },
      ],
    }).compile();

    controller = module.get<StockEntryController>(StockEntryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

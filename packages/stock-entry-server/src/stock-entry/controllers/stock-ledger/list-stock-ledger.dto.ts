import { IsNumberString, IsOptional, IsString } from 'class-validator';
import { StockLedgerDto } from '../../entities/stock-ledger/stock-ledger-dto';

export class ListStockLedgerDto {
  // add as per stock ledger fields
  @IsOptional()
  docs: StockLedgerDto[];

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

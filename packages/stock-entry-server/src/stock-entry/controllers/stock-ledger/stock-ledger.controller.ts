import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { RetrieveStockLedgerListQuery } from '../../queries/stock-ledger/list-stock-ledger/retrieve-stock-ledger-list.query';
import { ListStockLedgerDto } from './list-stock-ledger.dto';

@Controller('stock_ledger')
export class StockLedgerController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getStockLedgerList(@Query() query: ListStockLedgerDto) {
    return await this.queryBus.execute(new RetrieveStockLedgerListQuery(query));
  }
}

import { IsNumberString, IsOptional, IsString } from 'class-validator';
import { BinDto } from '../../entities/bin/bin-dto';

export class ListBinDto {
  // add as per stock entry fields
  @IsOptional()
  docs: BinDto[];

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { RetrieveBinListQuery } from '../../queries/bin/list-bin/retrieve-bin-list.query';
import { ListBinDto } from './list-bin.dto';

@Controller('bin')
export class BinController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getBinList(@Query() query: ListBinDto) {
    return await this.queryBus.execute(new RetrieveBinListQuery(query));
  }
}

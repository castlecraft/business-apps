import { BinController } from './bin/bin.controller';
import { StockEntryController } from './stock-entry/stock-entry.controller';
import { StockLedgerController } from './stock-ledger/stock-ledger.controller';

export const StockEntryControllerManager = [
  StockEntryController,
  StockLedgerController,
  BinController,
];

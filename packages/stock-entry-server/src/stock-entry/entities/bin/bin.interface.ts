import { Document } from 'mongoose';

export interface Bin extends Document {
  uuid: string;
  // need to add more fields for stock entry
}

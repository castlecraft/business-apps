import { Test, TestingModule } from '@nestjs/testing';
import { BIN } from './bin.schema';
import { BinService } from './bin.service';

describe('BinService', () => {
  let service: BinService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BinService,
        {
          provide: BIN,
          useValue: {}, // use mock values
        },
      ],
    }).compile();

    service = module.get<BinService>(BinService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

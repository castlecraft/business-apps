import { IsOptional, IsString } from 'class-validator';

export class BinDto {
  @IsOptional()
  @IsString()
  uuid: string;
}

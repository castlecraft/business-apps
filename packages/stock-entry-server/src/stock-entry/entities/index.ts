import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { DATABASE_CONNECTION } from '../../database.provider';
import { BIN, Bin } from './bin/bin.schema';
import { BinService } from './bin/bin.service';
import { StockEntry, STOCK_ENTRY } from './stock-entry/stock-entry.schema';
import { StockEntryService } from './stock-entry/stock-entry.service';
import { StockLedger, STOCK_LEDGER } from './stock-ledger/stock-ledger.schema';
import { StockLedgerService } from './stock-ledger/stock-ledger.service';

export const StockEntryEntities: Provider[] = [
  {
    provide: STOCK_ENTRY,
    useFactory: (connection: Connection) =>
      connection.model(STOCK_ENTRY, StockEntry),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: STOCK_LEDGER,
    useFactory: (connection: Connection) =>
      connection.model(STOCK_LEDGER, StockLedger),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: BIN,
    useFactory: (connection: Connection) => connection.model(BIN, Bin),
    inject: [DATABASE_CONNECTION],
  },
];

export const StockEntryEntityServices: Provider[] = [
  StockEntryService,
  StockLedgerService,
  BinService,
];

import { IsOptional, IsString } from 'class-validator';

export class StockLedgerDto {
  @IsOptional()
  @IsString()
  uuid: string;
}

import { Document } from 'mongoose';

export interface StockLedger extends Document {
  uuid: string;
  // need to add more fields for stock entry
}

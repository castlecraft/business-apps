import { Test, TestingModule } from '@nestjs/testing';
import { STOCK_LEDGER } from './stock-ledger.schema';
import { StockLedgerService } from './stock-ledger.service';

describe('StockLedgerService', () => {
  let service: StockLedgerService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StockLedgerService,
        {
          provide: STOCK_LEDGER,
          useValue: {}, // use mock values
        },
      ],
    }).compile();

    service = module.get<StockLedgerService>(StockLedgerService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { IsOptional, IsString } from 'class-validator';

export class StockEntryDto {
  @IsOptional()
  @IsString()
  uuid: string;
}

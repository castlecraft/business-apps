import * as mongoose from 'mongoose';
import * as mongooseIntl from 'mongoose-intl';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    // need to more fiels for stock entry
  },
  { collection: 'stock-entry', versionKey: false },
);

export const StockEntry = schema.plugin(mongooseIntl, {
  languages: ['en', 'ar'],
  defaultLanguage: 'en',
});

export const STOCK_ENTRY = 'Stock Entry';

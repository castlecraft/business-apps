import { Test, TestingModule } from '@nestjs/testing';
import { STOCK_ENTRY } from './stock-entry.schema';
import { StockEntryService } from './stock-entry.service';

describe('StockEntryService', () => {
  let service: StockEntryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        StockEntryService,
        {
          provide: STOCK_ENTRY,
          useValue: {}, // use mock values
        },
      ],
    }).compile();

    service = module.get<StockEntryService>(StockEntryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

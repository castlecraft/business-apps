import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { StockEntryModule } from './stock-entry/stock-entry.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CommonModule } from './common/common.module';
import { ConfigModule } from './config/config.module';
import { HealthcheckModule } from './healthcheck/healthcheck.module';

@Module({
  imports: [
    HttpModule,
    ConfigModule,
    CommonModule,
    StockEntryModule,
    HealthcheckModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}

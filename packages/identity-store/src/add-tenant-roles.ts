import { TenantRoleService } from '@castlecraft/auth';
import { INestApplication } from '@nestjs/common';
import { ACCOUNTANT, ADMINISTRATOR, AUDITOR } from './constants';

export async function addTenantRoles(app: INestApplication) {
  const role = app.get(TenantRoleService);
  const admin = await role.findOne({ roleName: ADMINISTRATOR });
  if (!admin) {
    await role.insertOne({ roleName: ADMINISTRATOR });
  }
  const accountant = await role.findOne({ roleName: ACCOUNTANT });
  if (!accountant) {
    await role.insertOne({ roleName: ACCOUNTANT });
  }
  const auditor = await role.findOne({ roleName: AUDITOR });
  if (!auditor) {
    await role.insertOne({ roleName: AUDITOR });
  }
}

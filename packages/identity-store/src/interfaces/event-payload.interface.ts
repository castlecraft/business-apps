import { TokenEventData } from './token-event.interface';
import { UserEventData } from './user-event.interface';

export interface EventPayload {
  eventId: string;
  eventName: string;
  eventFromService: string;
  eventDateTime: Date;
  eventData: TokenEventData & UserEventData;
}

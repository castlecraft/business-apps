import { TokenCacheService } from '@castlecraft/auth';
import { INestApplication } from '@nestjs/common';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { TokenController } from '../src/controllers/token/token.controller';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      controllers: [TokenController],
      providers: [
        TokenCacheService,
        {
          provide: TokenCacheService,
          useValue: {
            insertOne: (...args) => Promise.resolve(),
            deleteMany: (...args) => Promise.resolve(),
          },
        },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', done => {
    return request(app.getHttpServer()).get('/').end(done);
  });

  afterAll(async () => {
    await app.close();
  });
});

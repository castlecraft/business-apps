import { DomainEventService } from '@castlecraft/event-log';
import { Controller } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { EventPayload } from '../../interfaces';

@Controller('events')
export class EventsController {
  constructor(private readonly events: DomainEventService) {}

  @EventPattern('+')
  clientAdded(payload: EventPayload) {
    if (payload) {
      this.events
        .insertOne(payload)
        .then(added => {})
        .catch(err => {});
    }
  }
}

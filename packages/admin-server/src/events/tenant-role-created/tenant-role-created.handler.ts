import { TenantRoleService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantRoleCreatedEvent } from './tenant-role-created.event';

@EventsHandler(TenantRoleCreatedEvent)
export class TenantRoleCreatedHandler
  implements IEventHandler<TenantRoleCreatedEvent>
{
  constructor(private readonly tenantRole: TenantRoleService) {}
  handle(event: TenantRoleCreatedEvent) {
    if (event?.tenantRole) {
      this.tenantRole
        .insertOne(event.tenantRole)
        .then(added => {})
        .catch(err => {});
    }
  }
}

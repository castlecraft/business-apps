import { TenantService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantRemovedEvent } from './tenant-removed.event';

@EventsHandler(TenantRemovedEvent)
export class TenantRemovedHandler implements IEventHandler<TenantRemovedEvent> {
  constructor(private readonly tenant: TenantService) {}
  handle(event: TenantRemovedEvent) {
    if (event?.tenant) {
      this.tenant
        .deleteOne({ uuid: event?.tenant?.uuid })
        .then(added => {})
        .catch(err => {});
    }
  }
}

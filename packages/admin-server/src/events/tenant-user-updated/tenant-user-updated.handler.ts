import { TenantUserService } from '@castlecraft/auth';
import { Logger } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantUserUpdatedEvent } from './tenant-user-updated.event';

@EventsHandler(TenantUserUpdatedEvent)
export class TenantUserUpdatedHandler
  implements IEventHandler<TenantUserUpdatedEvent>
{
  constructor(private readonly tenantUser: TenantUserService) {}

  handle(event: TenantUserUpdatedEvent) {
    const { tenant, user, ...payload } = event.payload;
    this.tenantUser
      .updateOne({ tenant, user }, { $set: payload })
      .then()
      .catch(error => Logger.error(error, error, this.constructor.name));
  }
}

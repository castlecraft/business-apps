import { Client, User } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

export class TenantUserUpdatedEvent implements IEvent {
  constructor(
    public readonly actor: User | Client,
    public readonly payload: TenantUserDto,
  ) {}
}

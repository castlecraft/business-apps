import { TenantRoleService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantRoleDeletedEvent } from './tenant-role-deleted.event';

@EventsHandler(TenantRoleDeletedEvent)
export class TenantRoleDeletedHandler
  implements IEventHandler<TenantRoleDeletedEvent>
{
  constructor(private readonly tenantRole: TenantRoleService) {}

  handle(event: TenantRoleDeletedEvent) {
    if (event?.tenantRole) {
      this.tenantRole
        .deleteOne({ roleName: event?.tenantRole?.roleName })
        .then(added => {})
        .catch(err => {});
    }
  }
}

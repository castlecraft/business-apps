import { TenantUserService } from '@castlecraft/auth';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { TenantUserRemovedEvent } from './tenant-user-removed.event';

@EventsHandler(TenantUserRemovedEvent)
export class TenantUserRemovedHandler
  implements IEventHandler<TenantUserRemovedEvent>
{
  constructor(private readonly tenantUser: TenantUserService) {}
  handle(event: TenantUserRemovedEvent) {
    if (event?.tenantUser) {
      this.tenantUser
        .deleteOne({
          tenant: event?.tenantUser?.tenant,
          user: event?.tenantUser?.user,
        })
        .then(added => {})
        .catch(err => {});
    }
  }
}

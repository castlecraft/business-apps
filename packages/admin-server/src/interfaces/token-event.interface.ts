export interface Token {
  accessToken: string;
  redirectUris: string[];
  client: string;
  creation: Date;
  modified: Date;
  refreshToken?: string;
  user: string;
  scope: string[];
  expiresIn: number;
}

export interface Client {
  uuid?: string;
  creation?: Date;
  modified?: Date;
  createdBy?: string;
  modifiedBy?: string;
  name?: string;
  clientId?: string;
  clientSecret?: string;
  isTrusted?: number;
  autoApprove?: boolean;
  redirectUris?: string[];
  allowedScopes?: string[];
  userDeleteEndpoint?: string;
  tokenDeleteEndpoint?: string;
  changedClientSecret?: string;
  authenticationMethod?: ClientAuthentication;
}

export enum ClientAuthentication {
  BasicHeader = 'BASIC_HEADER',
  BodyParam = 'BODY_PARAM',
  PublicClient = 'PUBLIC_CLIENT',
}

export interface TokenEventData {
  token?: Token;
  client?: Client;
  actorUserUuid?: string;
}

import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { IdentityService } from '../../aggregates/identity/identity.service';
import { ListTokenUsersQuery } from './list-token-users.query';

@QueryHandler(ListTokenUsersQuery)
export class ListTokenUsersHandler
  implements IQueryHandler<ListTokenUsersQuery>
{
  constructor(private readonly id: IdentityService) {}

  async execute(query: ListTokenUsersQuery) {
    return await this.id.listTokenUser(query.filterQuery);
  }
}

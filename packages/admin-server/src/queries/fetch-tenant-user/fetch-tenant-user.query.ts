import { IQuery } from '@nestjs/cqrs';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

export class FetchTenantUserQuery implements IQuery {
  constructor(
    public readonly query: Omit<TenantUserDto, 'roles'>,
    public readonly requestTenant?: string,
  ) {}
}

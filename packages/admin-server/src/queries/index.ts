import { FetchErrorLogHandler } from './fetch-error-log/fetch-error-log.handler';
import { FetchEventHandler } from './fetch-event/fetch-event.handler';
import { FetchTenantUserHandler } from './fetch-tenant-user/fetch-tenant-user.handler';
import { FetchTenantHandler } from './fetch-tenant/fetch-tenant.handler';
import { ListErrorLogHandler } from './list-error-logs/list-error-logs.handler';
import { ListEventHandler } from './list-events/list-events.handler';
import { ListTenantForUserHandler } from './list-tenant-for-users/list-tenant-for-users.handler';
import { ListTenantRoleHandler } from './list-tenant-role/list-tenant-role.handler';
import { ListTenantUserHandler } from './list-tenant-user/list-tenant-user.handler';
import { ListTenantHandler } from './list-tenant/list-tenant.handler';
import { ListTokenUsersHandler } from './list-token-users/list-token-users.handler';

export const AdminQueryHandlers = [
  FetchErrorLogHandler,
  FetchEventHandler,
  ListErrorLogHandler,
  ListEventHandler,
  ListTenantRoleHandler,
  ListTenantUserHandler,
  ListTenantHandler,
  ListTenantForUserHandler,
  FetchTenantUserHandler,
  FetchTenantHandler,
  ListTokenUsersHandler,
];

import { CreatedByActor } from '@castlecraft/auth';
import { IQuery } from '@nestjs/cqrs';

export class FetchErrorLogQuery implements IQuery {
  constructor(
    public readonly uuid: string,
    public readonly actorType: CreatedByActor,
    public readonly actorUuid: string,
  ) {}
}

import { IQuery } from '@nestjs/cqrs';
import { User } from '@castlecraft/auth';
import { ListTenantDto } from '../../controllers/tenant/list-tenant.dto';

export class ListTenantForUsersQuery implements IQuery {
  constructor(
    public readonly actor: User,
    public readonly query: ListTenantDto,
  ) {}
}

import { IQuery } from '@nestjs/cqrs';
import { Client, User } from '@castlecraft/auth';
import { ListTenantDto } from '../../controllers/tenant/list-tenant.dto';

export class ListTenantQuery implements IQuery {
  constructor(
    public readonly actor: User | Client,
    public readonly query: ListTenantDto,
  ) {}
}

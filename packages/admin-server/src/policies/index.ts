import { ActorService } from './actor/actor.service';
import { AddOnPoliciesService } from './add-on/add-on-policies.service';
import { PlanPoliciesService } from './plan/plan-policies.service';

export const AdminPolicies = [
    ActorService,
    PlanPoliciesService,
    AddOnPoliciesService,
];

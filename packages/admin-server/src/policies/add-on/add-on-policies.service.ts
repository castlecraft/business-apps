import { AddOnService } from '@castlecraft/auth';
import { Injectable, NotFoundException } from '@nestjs/common';

@Injectable()
export class AddOnPoliciesService {
  constructor(private readonly addOn: AddOnService) {}

  async checkAddOn(uuid: string) {
    const addOn = await this.addOn.findOne({ uuid });
    if (!addOn) {
      throw new NotFoundException('Addon not found.');
    }
    return addOn;
  }
}

import { AddOnService } from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { AddOnPoliciesService } from './add-on-policies.service';

describe('AddOnPoliciesService', () => {
  let service: AddOnPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AddOnPoliciesService,
        { provide: AddOnService, useValue: {} },
      ],
    }).compile();

    service = module.get<AddOnPoliciesService>(AddOnPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

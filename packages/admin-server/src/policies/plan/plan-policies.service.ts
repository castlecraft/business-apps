import { PlanService } from '@castlecraft/auth';
import { BadRequestException, Injectable } from '@nestjs/common';
import { forkJoin, from, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PlanDto } from '../../dto/plan/plan.dto';

@Injectable()
export class PlanPoliciesService {
  constructor(private readonly plan: PlanService) {}

  validatePlanDeactivation(uuid: string, updatedPlanUuid: string) {
    return forkJoin({
      existingPlan: from(this.plan.findOne({ uuid })),
      newPlan: from(this.plan.findOne({ uuid: updatedPlanUuid })),
    }).pipe(
      switchMap(({ existingPlan, newPlan }) => {
        if (!existingPlan || !newPlan) {
          return throwError(
            new BadRequestException(
              `${existingPlan ? 'Updated' : 'Existing'} plan not found`,
            ),
          );
        }

        if (existingPlan.deactivated) {
          return throwError(
            new BadRequestException('Plan is already deactivated.'),
          );
        }
        return of(true);
      }),
    );
  }


  validateNewPlan(payload: PlanDto){
    return of(true)
  }
}

import { PlanService } from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { PlanPoliciesService } from './plan-policies.service';

describe('PlanPoliciesService', () => {
  let service: PlanPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PlanPoliciesService, { provide: PlanService, useValue: {} }],
    }).compile();

    service = module.get<PlanPoliciesService>(PlanPoliciesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

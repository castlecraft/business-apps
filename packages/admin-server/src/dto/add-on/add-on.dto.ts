import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNumber, IsOptional, IsString } from 'class-validator';
import { AddOnType } from '@castlecraft/auth';

export class AddOnDto {
  @IsString()
  @ApiProperty({ required: true })
  name?: string;

  @IsEnum(AddOnType)
  @ApiProperty({ required: true })
  addOnType?: AddOnType;

  @IsNumber()
  @ApiProperty({ required: true })
  amount: number;

  @IsString()
  @ApiProperty({ required: true })
  currency: string;

  @IsString()
  @ApiProperty({ required: true })
  description: string;
}

export class UpdateAddOnDto {
  @IsString()
  @ApiProperty({ required: true })
  uuid: string;

  @IsEnum(AddOnType)
  @IsOptional()
  @ApiProperty()
  addOnType?: AddOnType;

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  amount: number;

  @IsString()
  @IsOptional()
  @ApiProperty()
  currency: string;

  @IsString()
  @IsOptional()
  @ApiProperty()
  description: string;
}

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsEnum, IsNumber, IsString, IsUUID, Min } from 'class-validator';
import { ChargePeriod, CurrencyOptions } from '@castlecraft/auth';

export class PlanDto {
  @IsString()
  @ApiProperty({ required: true })
  name: string;

  @IsNumber()
  @Min(0)
  @Type(() => Number)
  @ApiProperty({ required: true })
  numberOfSitesAllowed: number;

  @IsNumber()
  @Min(0)
  @Type(() => Number)
  @ApiProperty({ required: true })
  interval: number;

  @IsEnum(ChargePeriod)
  @ApiProperty({ required: true })
  period: ChargePeriod;

  @IsNumber()
  @ApiProperty({ required: true })
  amount: number;

  @IsEnum(CurrencyOptions)
  @ApiProperty({ required: true })
  currency: CurrencyOptions;

  @IsString()
  @ApiProperty({ required: true })
  description: string;
}

export class UpdatePlanDto extends PlanDto {
  @IsUUID()
  @ApiProperty({ required: true })
  uuid: string;
}

export class DeactivatePlanDto {
  @IsUUID()
  @ApiProperty({ required: true })
  uuid: string;

  @IsUUID()
  @ApiProperty({ required: true })
  updatedPlan: string;
}

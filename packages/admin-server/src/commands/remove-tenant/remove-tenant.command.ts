import { Client, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';

export class RemoveTenantCommand implements ICommand {
  constructor(
    public readonly actor: User | Client,
    public readonly uuid: string,
  ) {}
}

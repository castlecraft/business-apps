import { Client, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';

export class CreateTenantRoleCommand implements ICommand {
  constructor(
    public readonly actor: User | Client,
    public readonly roleName: string,
  ) {}
}

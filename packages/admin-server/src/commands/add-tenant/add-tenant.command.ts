import { ICommand } from '@nestjs/cqrs';
import { Client, User } from '@castlecraft/auth';

export class AddTenantCommand implements ICommand {
  constructor(
    public readonly actor: User | Client,
    public readonly payload?: unknown & { uuid: string },
  ) {}
}

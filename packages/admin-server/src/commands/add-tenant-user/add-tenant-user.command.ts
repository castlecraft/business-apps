import { Client, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

export class AddTenantUserCommand implements ICommand {
  constructor(
    public readonly actor: User | Client,
    public readonly payload: TenantUserDto,
  ) {}
}

import { Client, TenantUser, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

export class RemoveTenantUserCommand implements ICommand {
  constructor(
    public readonly actor: User | Client | TenantUser,
    public readonly payload: TenantUserDto,
  ) {}
}

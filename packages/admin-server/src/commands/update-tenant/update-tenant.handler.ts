import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenantAggregateService } from '../../aggregates/tenant-aggregate/tenant-aggregate.service';
import { UpdateTenantCommand } from './update-tenant.command';

@CommandHandler(UpdateTenantCommand)
export class UpdateTenantHandler
  implements ICommandHandler<UpdateTenantCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TenantAggregateService,
  ) {}

  async execute(command: UpdateTenantCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.updateTenant(command.actor, command.payload);
    aggregate.commit();
  }
}

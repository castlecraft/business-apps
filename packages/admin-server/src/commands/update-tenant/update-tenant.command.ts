import { Client, User } from '@castlecraft/auth';
import { ICommand } from '@nestjs/cqrs';

export class UpdateTenantCommand implements ICommand {
  constructor(
    public readonly actor: User | Client,
    public readonly payload: unknown & { uuid: string },
  ) {}
}

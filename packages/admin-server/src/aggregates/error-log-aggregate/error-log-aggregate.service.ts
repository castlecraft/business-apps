import { CreatedByActor } from '@castlecraft/auth';
import { ErrorLogService } from '@castlecraft/event-log';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ActorService } from '../../policies/actor/actor.service';

@Injectable()
export class ErrorLogAggregateService {
  constructor(
    private readonly errorLog: ErrorLogService,
    private readonly actorPolicy: ActorService,
  ) {}

  async list(
    actorType: CreatedByActor,
    actorUuid: string,
    uuid: string,
    message: string,
    fromService: string,
    fromDateTime: Date,
    toDataTime: Date,
    offset: number = 0,
    limit: number = 10,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const query = {
      uuid,
      message,
      fromService,
      fromDateTime,
      dateTime:
        fromDateTime && toDataTime
          ? {
              $gte: fromDateTime,
              $lt: toDataTime,
            }
          : undefined,
    };
    const docs = await this.errorLog.model
      .find(query)
      .skip(Number(offset) || 0)
      .limit(Number(limit) || 10);

    return {
      docs,
      length: await this.errorLog.model.countDocuments(query),
      offset: Number(offset),
    };
  }

  async fetchErrorLog(
    actorType: CreatedByActor,
    actorUuid: string,
    uuid: string,
  ) {
    await this.actorPolicy.validateActor(actorType, actorUuid);

    const errorLog = await this.errorLog.findOne({ uuid });
    if (!errorLog) {
      throw new NotFoundException({ ErrorLogNotFound: uuid });
    }
    return errorLog;
  }
}

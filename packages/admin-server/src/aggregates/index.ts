import { ErrorLogAggregateService } from './error-log-aggregate/error-log-aggregate.service';
import { EventAggregateService } from './event-aggregate/event-aggregate.service';
import { TenantAggregateService } from './tenant-aggregate/tenant-aggregate.service';
import { IdentityService } from './identity/identity.service';
import { AddOnAggregateService } from './add-on-aggregate/add-on-aggregate.service';
import { PlanAggregateService } from './plan-aggregate/plan-aggregate.service';

export const AdminAggregates = [
  EventAggregateService,
  ErrorLogAggregateService,
  IdentityService,
  TenantAggregateService,
  AddOnAggregateService,
  PlanAggregateService
];

import {
  TenantRoleService,
  TenantService,
  TenantUser,
  TenantUserService,
  User,
  UserService,
} from '@castlecraft/auth';
import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { ListTokenUsersDto } from '../../controllers/user-tenant/list-users-query.dto';
import { ADMINISTRATOR } from '../../constants/strings';
import { ListRoleDto } from '../../controllers/tenant/list-role.dto';
import { ListTenantUserDto } from '../../controllers/tenant/list-tenant-user.dto';
import { ListTenantDto } from '../../controllers/tenant/list-tenant.dto';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';

@Injectable()
export class IdentityService {
  constructor(
    private readonly tenant: TenantService,
    private readonly tenantUser: TenantUserService,
    private readonly tenantRole: TenantRoleService,
    private readonly user: UserService,
  ) {}

  // Tenant
  async validateTenantBeforeRemove(uuid: string) {
    const tenant = await this.findTenantOrFail(uuid);
    const tenantUsers = await this.tenantUser.find({ tenant: uuid });
    if (tenantUsers && tenantUsers.length > 0) {
      throw new BadRequestException({ TenantUsersExistForTenant: uuid });
    }
    return tenant;
  }

  async findTenantOrFail(uuid: string) {
    const tenant = await this.tenant.findOne({ uuid });
    if (!tenant) {
      throw new NotFoundException({ Tenant: uuid });
    }
    return tenant;
  }

  async listTenants(query: ListTenantDto) {
    const { limit, offset, sort, ...filters } = query;
    let sortOrder: any = { uuid: 1 };
    try {
      sortOrder = sort
        ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
        : { uuid: 1 };
    } catch (error) {}

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined || filters[key] === '') {
        delete filters[key];
      }
      try {
        filters[key] = new RegExp(filters[key], 'i');
      } catch (error) {}
    });

    const docs = await this.tenant.model
      .find(filters)
      .limit(Number(limit) || 10)
      .skip(Number(offset) || 0)
      .sort(sortOrder);

    return {
      docs: docs || [],
      length: await this.tenant.model.countDocuments(filters),
      offset,
    };
  }

  async listTenantForUsers(query: ListTenantDto, user: User) {
    const tenants = await this.tenantUser.find({ user: user?.uuid });
    const { limit, offset, sort, ...filters } = query;
    let sortOrder: any = { uuid: 1 };
    try {
      sortOrder = sort
        ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
        : { uuid: 1 };
    } catch (error) {}

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined || filters[key] === '') {
        delete filters[key];
      }
      try {
        filters[key] = new RegExp(filters[key], 'i');
      } catch (error) {}
    });

    const docs = await this.tenant.model
      .find({
        ...filters,
        $and: [{ uuid: { $in: tenants.map(tenant => tenant.tenant) } }],
      })
      .limit(Number(limit) || 10)
      .skip(Number(offset) || 0)
      .sort(sortOrder);

    return {
      docs: docs || [],
      length: await this.tenant.model.countDocuments(filters),
      offset,
    };
  }

  // Roles
  async validateBeforeAddingRole(roleName: string) {
    const role = await this.tenantRole.findOne({ roleName });
    if (role) {
      throw new BadRequestException({ RoleAlreadyExists: roleName });
    }
  }

  async validateBeforeRemoveRole(roleName: string) {
    const tenantUsers = await this.tenantUser.find({ roles: roleName });
    if (tenantUsers && tenantUsers.length > 0) {
      throw new BadRequestException({ RoleAssignedToTenantUsers: roleName });
    }
  }

  async listRole(query: ListRoleDto) {
    const { limit, offset, sort, ...filters } = query;
    let sortOrder: any = { roleName: 1 };
    try {
      sortOrder = sort
        ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
        : { roleName: 1 };
    } catch (error) {}

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined || filters[key] === '') {
        delete filters[key];
      }
      try {
        filters[key] = new RegExp(filters[key], 'i');
      } catch (error) {}
    });

    const docs = await this.tenantRole.model
      .find(filters)
      .limit(Number(limit) || 10)
      .skip(Number(offset) || 0)
      .sort(sortOrder);

    return {
      docs: docs || [],
      length: await this.tenantRole.model.countDocuments(filters),
      offset,
    };
  }

  // Tenant User
  async validateBeforeAddTenantUser(user: TenantUserDto) {
    const isUserExist = await this.tenantUser.findOne({
      user: user.user,
      tenant: user.tenant,
    });
    if (isUserExist) {
      throw new BadRequestException({
        TenantUserAlreadyExists: { tenant: user.tenant, user: user.user },
      });
    }
    await this.validateAbsentRoles(user.roles);
  }

  async findUserOrFail(uuid: string) {
    const user = await this.user.findOne({ uuid });
    if (!user) {
      throw new NotFoundException({ user: uuid });
    }
    return user;
  }

  async validateBeforeEditTenantUser(
    actor: User | TenantUser,
    user: TenantUserDto,
  ) {
    const tenantUser = await this.tenantUser.findOne({
      user: user.user,
      tenant: user.tenant,
    });

    if (!tenantUser) {
      throw new NotFoundException({
        TenantUserDoesNotExists: { tenant: user.tenant, user: user.user },
      });
    }

    await this.validateAbsentRoles(user.roles);
    await this.doNotAllowSelfRemovalOfAdminRole(
      tenantUser.roles,
      user.roles,
      actor,
      tenantUser,
    );
  }

  async validateBeforeRemoveTenantUser(user: Omit<TenantUserDto, 'roles'>) {
    const tenantUser = await this.tenantUser.findOne({
      user: user.user,
      tenant: user.tenant,
    });

    if (tenantUser?.roles?.includes(ADMINISTRATOR)) {
      throw new BadRequestException({ CannotDeleteAdministrator: user });
    }
  }

  async getTenantUser(user: Omit<TenantUserDto, 'roles'>) {
    const tenantUser = await this.tenantUser.findOne({
      user: user.user,
      tenant: user.tenant,
    });

    const tokenUser = await this.user.findOne({ uuid: user.user });

    if (tenantUser) {
      return {
        ...tenantUser,
        email: tokenUser?.email,
        phone: tokenUser?.phone,
      };
    }
  }

  async listTenantUser(query: ListTenantUserDto) {
    const { limit, offset, sort, ...filters } = query;
    let sortOrder: any = { tenant: 1 };
    try {
      sortOrder = sort
        ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
        : { tenant: 1 };
    } catch (error) {}

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined || filters[key] === '') {
        delete filters[key];
      } else {
        try {
          filters[key] = new RegExp(filters[key], 'i');
        } catch (error) {}
      }
    });

    const docs = await this.tenantUser.model
      .aggregate([
        { $match: filters },
        {
          $lookup: {
            from: 'token_user',
            localField: 'user',
            foreignField: 'uuid',
            as: 'tokenUser',
          },
        },
        { $addFields: { email: '$tokenUser.email' } },
        { $unwind: '$email' },
        { $addFields: { phone: '$tokenUser.phone' } },
        { $unwind: '$phone' },
        { $project: { tokenUser: 0 } },
      ])
      .limit(Number(limit) || 10)
      .skip(Number(offset) || 0)
      .sort(sortOrder);

    return {
      docs: docs || [],
      length: await this.tenantUser.model.countDocuments(filters),
      offset,
    };
  }

  async validateAbsentRoles(roles: string[]) {
    const totalRoles = await this.tenantRole.find({ roleName: { $in: roles } });
    if (totalRoles?.length < roles?.length) {
      const absentRoles = roles.filter(
        role => !totalRoles.map(tRole => tRole.roleName).includes(role),
      );
      if (absentRoles) {
        throw new BadRequestException({ InvalidRoles: absentRoles });
      }
    }
  }

  async findRoleOrFail(roleName: string) {
    const role = await this.tenantRole.findOne({ roleName });
    if (!role) {
      throw new NotFoundException({ TenantRoleNotFound: roleName });
    }
    return role;
  }

  async listTokenUser(query: ListTokenUsersDto) {
    const { limit, offset, sort, ...filters } = query;
    let sortOrder: any = { tenant: 1 };
    try {
      sortOrder = sort
        ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
        : { email: 1 };
    } catch (error) {}

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined || filters[key] === '') {
        delete filters[key];
      }
      try {
        filters[key] = new RegExp(filters[key], 'i');
      } catch (error) {}
    });

    const docs = await this.user.model
      .find(filters)
      .limit(Number(limit) || 10)
      .skip(Number(offset) || 0)
      .sort(sortOrder);

    return {
      docs: docs || [],
      length: await this.user.model.countDocuments(filters),
      offset,
    };
  }

  doNotAllowSelfRemovalOfAdminRole(
    currentRoles: string[],
    updateRoles: string[],
    actor: TenantUser | User,
    updateTenantUser: TenantUser,
  ) {
    if (
      (actor as User)?.roles?.includes(ADMINISTRATOR) &&
      (actor as User)?.uuid
    ) {
      return;
    }
    if (
      (actor as TenantUser)?.user === updateTenantUser?.user &&
      (actor as TenantUser)?.tenant === updateTenantUser?.tenant &&
      currentRoles?.includes(ADMINISTRATOR) &&
      !updateRoles?.includes(ADMINISTRATOR)
    ) {
      throw new BadRequestException({
        CannotRemoveAdministratorRoleFrom: currentRoles,
      });
    }
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { AddOnPoliciesService } from '../../policies/add-on/add-on-policies.service';
import { AddOnService } from '../../entities/add-on/add-on.service';
import { AddOnAggregateService } from './add-on-aggregate.service';

describe('AddOnAggregateService', () => {
  let service: AddOnAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AddOnAggregateService,
        {
          provide: AddOnService,
          useValue: {},
        },
        {
          provide: AddOnPoliciesService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AddOnAggregateService>(AddOnAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Injectable, NotFoundException } from '@nestjs/common';
import { AddOnPoliciesService } from '../../policies/add-on/add-on-policies.service';
import { AddOnDto, UpdateAddOnDto } from '../../dto/add-on/add-on.dto';
import { AddOnService } from '@castlecraft/auth';

@Injectable()
export class AddOnAggregateService {
  constructor(
    private readonly addOn: AddOnService,
    private readonly addOnPolicies: AddOnPoliciesService,
  ) {}

  async createAddOn(payload: AddOnDto) {
    return await this.addOn.create(payload);
  }

  async deleteAddOn(uuid: string) {
    await this.addOnPolicies.checkAddOn(uuid);
    return await this.addOn.deleteOne({ uuid });
  }

  async updateAddOn(payload: UpdateAddOnDto) {
    await this.checkAddOn(payload.uuid);
    return await this.addOn.updateOne(
      { uuid: payload.uuid },
      { $set: payload },
    );
  }

  async retrieveAddOn(uuid: string) {
    const addOn = await this.checkAddOn(uuid);
    return addOn;
  }

  async listAddOns(offset: number, limit: number, search: string) {
    const query = {};
    return await this.addOn.list(offset, limit, search, query, { uuid: 'ASC' });
  }

  async checkAddOn(uuid: string) {
    const plan = await this.addOn.findOne({ uuid });
    if (!plan) {
      throw new NotFoundException({ AddOnNotFound: uuid });
    }
    return plan;
  }
}

import { HttpService } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { RazorPayService } from '../../../auth/aggregates/razorpay-manager/razorpay.service';
import { PlanPoliciesService } from '../../policies/plan/plan-policies.service';
import { PlanService } from '../../entities/plan/plan.service';
import { PlanAggregateService } from './plan-aggregate.service';
import { TokenCacheService } from '../../../auth/entities/token-cache/token-cache.service';

describe('PlanAggregateService', () => {
  let service: PlanAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        PlanAggregateService,
        {
          provide: PlanService,
          useValue: {},
        },
        {
          provide: RazorPayService,
          useValue: {},
        },
        {
          provide: HttpService,
          useValue: {},
        },
        {
          provide: PlanPoliciesService,
          useValue: {},
        },
        {
          provide: TokenCacheService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<PlanAggregateService>(PlanAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

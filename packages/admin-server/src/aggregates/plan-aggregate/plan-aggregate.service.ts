import { PlanService } from '@castlecraft/auth';
import { Injectable, NotFoundException } from '@nestjs/common';
import { switchMap } from 'rxjs/operators';
import { from } from 'rxjs';
import { PlanPoliciesService } from '../../policies/plan/plan-policies.service';
import { PlanDto, UpdatePlanDto } from '../../dto/plan/plan.dto';

@Injectable()
export class PlanAggregateService {
  constructor(
    private readonly plan: PlanService,
    private readonly planPolicies: PlanPoliciesService,
  ) {}

  createPlan(payload: PlanDto) {
    return this.planPolicies.validateNewPlan(payload).pipe(
      switchMap((success: boolean) => {
        return from(
          this.plan.create({
            ...payload,
          }),
        );
      }),
    );
  }

  async deactivatePlan(uuid: string, updatedPlanUuid: string) {
    await this.planPolicies
      .validatePlanDeactivation(uuid, updatedPlanUuid)
      .toPromise();
    return await this.plan.updateOne(
      { uuid },
      { $set: { deactivated: true, updatedPlan: updatedPlanUuid } },
    );
  }

  async updatePlan(uuid: string, payload: UpdatePlanDto) {
    await this.checkPlan(uuid);
    await this.plan.updateOne({ uuid }, { $set: payload });
    return payload;
  }

  async retrievePlan(uuid: string) {
    const plan = await this.checkPlan(uuid);
    return plan;
  }

  async listPlans(offset: number, limit: number, search: string) {
    const query = {};
    return await this.plan.list(offset, limit, search, query, { uuid: 'ASC' });
  }

  async checkPlan(uuid: string) {
    const plan = await this.plan.findOne({ uuid });
    if (!plan) {
      throw new NotFoundException({ PlanNotFound: uuid });
    }
    return plan;
  }
}

import {
  Client,
  CreatedByActor,
  Tenant,
  TenantRole,
  TenantUser,
  User,
} from '@castlecraft/auth';
import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { v4 as uuidv4 } from 'uuid';
import { ADMINISTRATOR } from '../../constants/strings';
import { ListRoleDto } from '../../controllers/tenant/list-role.dto';
import { ListTenantUserDto } from '../../controllers/tenant/list-tenant-user.dto';
import { ListTenantDto } from '../../controllers/tenant/list-tenant.dto';
import { TenantUserDto } from '../../controllers/tenant/tenant-user.dto';
import { TenantAddedEvent } from '../../events/tenant-added/tenant-added.event';
import { TenantRemovedEvent } from '../../events/tenant-removed/tenant-removed.event';
import { TenantRoleCreatedEvent } from '../../events/tenant-role-created/tenant-role-created.event';
import { TenantRoleDeletedEvent } from '../../events/tenant-role-deleted/tenant-role-deleted.event';
import { TenantUpdatedEvent } from '../../events/tenant-updated/tenant-updated.event';
import { TenantUserAddedEvent } from '../../events/tenant-user-added/tenant-user-added.event';
import { TenantUserRemovedEvent } from '../../events/tenant-user-removed/tenant-user-removed.event';
import { TenantUserUpdatedEvent } from '../../events/tenant-user-updated/tenant-user-updated.event';
import { IdentityService } from '../identity/identity.service';

@Injectable()
export class TenantAggregateService extends AggregateRoot {
  constructor(private readonly identity: IdentityService) {
    super();
  }

  async addTenant(actor: User | Client, payload: unknown & { uuid?: string }) {
    delete payload.uuid;

    const tenant = {} as Tenant;
    tenant.uuid = uuidv4();
    tenant.createdByActor = (actor as Client).clientId
      ? CreatedByActor.Client
      : CreatedByActor.User;
    tenant.createdById = (actor as Client).clientId
      ? (actor as Client).clientId
      : actor.uuid;
    Object.assign(tenant, payload);

    this.apply(new TenantAddedEvent(tenant, actor));

    return tenant;
  }

  async updateTenant(
    actor: User | Client,
    payload: unknown & { uuid?: string },
  ) {
    const tenant = await this.identity.findTenantOrFail(payload?.uuid);
    Object.assign(tenant, payload);
    this.apply(
      new TenantUpdatedEvent(actor, { uuid: tenant.uuid, ...payload }),
    );
  }

  async removeTenant(tenantId: string, actor: User | Client) {
    const tenant = await this.identity.validateTenantBeforeRemove(tenantId);
    this.apply(new TenantRemovedEvent(tenant, actor));
  }

  async fetchTenant(uuid: string, requestTenant?: string) {
    return await this.identity.findTenantOrFail(
      requestTenant ? requestTenant : uuid,
    );
  }

  async fetchTenantUser(
    tenantUser: Omit<TenantUserDto, 'roles'>,
    requestTenant?: string,
  ) {
    if (requestTenant) {
      tenantUser.tenant = requestTenant;
    }
    return await this.identity.getTenantUser(tenantUser);
  }

  async listTenants(actor: Client | User, query: ListTenantDto) {
    return await this.identity.listTenants(query);
  }

  async listTenantForUsers(
    actor: User,
    query: ListTenantDto & { tenantName?: string },
  ) {
    return await this.identity.listTenantForUsers(query, actor);
  }

  async addTenantUser(actor: User | Client, payload: TenantUserDto) {
    await this.identity.findTenantOrFail(payload.tenant);
    await this.identity.findUserOrFail(payload.user);
    await this.identity.validateBeforeAddTenantUser(payload);
    const tenantUser = {} as TenantUser;
    tenantUser.tenant = payload.tenant;
    tenantUser.user = payload.user;

    this.apply(new TenantUserAddedEvent(tenantUser, actor));

    return tenantUser;
  }

  async removeTenantUser(actor: User | Client, payload: TenantUserDto) {
    await this.identity.findTenantOrFail(payload.tenant);
    await this.identity.findUserOrFail(payload.user);
    await this.identity.validateBeforeRemoveTenantUser(payload);
    const tenantUser = {} as TenantUser;
    tenantUser.tenant = payload.tenant;
    tenantUser.user = payload.user;

    this.apply(new TenantUserRemovedEvent(tenantUser, actor));
  }

  async listTenantUsers(
    actor: User | Client,
    query: ListTenantUserDto,
    requestTenant: string,
  ) {
    if (
      !Object.keys(actor).includes('clientId') &&
      !(actor as User)?.roles?.includes(ADMINISTRATOR)
    ) {
      query.tenant = requestTenant;
    }
    return await this.identity.listTenantUser(query);
  }

  async listTenantRoles(query: ListRoleDto) {
    return await this.identity.listRole(query);
  }

  async createTenantRole(actor: User | Client, roleName: string) {
    await this.identity.validateBeforeAddingRole(roleName);
    const tenantRole = {
      uuid: uuidv4(),
      roleName: roleName.trim(),
    } as TenantRole;

    this.apply(new TenantRoleCreatedEvent(tenantRole, actor));
  }

  async deleteTenantRole(actor: User | Client, roleName: string) {
    const tenantRole = await this.identity.findRoleOrFail(roleName);
    await this.identity.validateBeforeRemoveRole(roleName);
    this.apply(new TenantRoleDeletedEvent(tenantRole, actor));
    return tenantRole;
  }

  async updateTenantUser(actor: User | TenantUser, payload: TenantUserDto) {
    await this.identity.validateBeforeEditTenantUser(actor, payload);
    this.apply(new TenantUserUpdatedEvent(actor, payload));
  }
}

import { AuthModule } from '@castlecraft/auth';
import { Global, Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule } from '@nestjs/microservices';
import {
  ConfigService,
  ID_DB_HOST,
  ID_DB_NAME,
  ID_DB_PASSWORD,
  ID_DB_USER,
  MONGO_URI_PREFIX,
} from '../config/config.service';
import { CommonCommandHandlers } from './commands';
import { eventsClient } from './events-microservice.client';
import { CommonSagas } from './sagas';

@Global()
@Module({
  imports: [
    ClientsModule.registerAsync([eventsClient]),
    CqrsModule,
    AuthModule.registerAsync({
      useFactory: (config: ConfigService) => ({
        mongoUriPrefix: config.get(MONGO_URI_PREFIX) || 'mongodb',
        identityDbHost: config.get(ID_DB_HOST),
        identityDbUser: config.get(ID_DB_USER),
        identityDbPassword: config.get(ID_DB_PASSWORD),
        identityDbName: config.get(ID_DB_NAME),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [...CommonCommandHandlers, ...CommonSagas],
  exports: [CqrsModule, AuthModule],
})
export class CommonModule {}

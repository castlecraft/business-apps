import { Inject } from '@nestjs/common';
import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { ClientMqtt } from '@nestjs/microservices';
import { SERVICE } from '../../../constants/strings';
import { v4 as uuidv4 } from 'uuid';
import {
  ConfigService,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
} from '../../../config/config.service';
import { BROADCAST_EVENT } from '../../events-microservice.client';
import { SaveEventCommand } from './save-event.command';

@CommandHandler(SaveEventCommand)
export class SaveEventHandler implements ICommandHandler<SaveEventCommand> {
  constructor(
    @Inject(BROADCAST_EVENT)
    private readonly publisher: ClientMqtt,
    private readonly config: ConfigService,
  ) {}

  async execute(command: SaveEventCommand) {
    const { event } = command;
    const payload = {
      eventId: uuidv4(),
      eventName: event.constructor.name,
      eventFromService: SERVICE,
      eventDateTime: new Date(),
      eventData: event,
    };
    const proto = this.config.get(EVENTS_PROTO);
    const user = this.config.get(EVENTS_USER);
    const password = this.config.get(EVENTS_PASSWORD);
    const host = this.config.get(EVENTS_HOST);
    const port = this.config.get(EVENTS_PORT);

    if (proto && user && password && host && port) {
      this.publisher.emit(event.constructor.name, payload);
    }
  }
}

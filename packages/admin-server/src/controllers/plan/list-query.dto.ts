import { ApiProperty } from '@nestjs/swagger';
import { IsOptional, IsNumberString } from 'class-validator';

export class ListQueryDto {
  @IsOptional()
  @IsNumberString()
  @ApiProperty()
  offset: number;

  @IsOptional()
  @IsNumberString()
  @ApiProperty()
  limit: number;

  @IsOptional()
  @ApiProperty()
  search: string;

  @IsOptional()
  @ApiProperty()
  sort: string;
}

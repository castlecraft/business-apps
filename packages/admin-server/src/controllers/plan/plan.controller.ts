import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Param,
  Body,
  Query,
  Get,
} from '@nestjs/common';
import { PlanAggregateService } from '../../aggregates/plan-aggregate/plan-aggregate.service';
import { ListQueryDto } from './list-query.dto';
import { RoleGuard, Roles, TokenGuard, UserGuard } from '@castlecraft/auth';
import { DeactivatePlanDto, PlanDto, UpdatePlanDto } from '../../dto/plan/plan.dto';
import { ADMINISTRATOR } from '../../constants/strings';
import { Reflector } from '@nestjs/core';

@Controller('plan')
@UseGuards(TokenGuard, UserGuard, new RoleGuard(new Reflector()))
export class PlanController {
  constructor(private readonly aggregate: PlanAggregateService) {}

  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() planPayload: PlanDto) {
    return this.aggregate.createPlan(planPayload);
  }

  @Post('v1/deactivate')
  @Roles(ADMINISTRATOR)
  remove(@Body() body: DeactivatePlanDto) {
    return this.aggregate.deactivatePlan(body.uuid, body.updatedPlan);
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  update(@Body() updatePayload: UpdatePlanDto) {
    return this.aggregate.updatePlan(updatePayload.uuid, updatePayload);
  }

  // TODO: Move below to public controller

  @Get('v1/get/:uuid')
  async getPlan(@Param('uuid') uuid) {
    return await this.aggregate.retrievePlan(uuid);
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  listPlans(@Query() query: ListQueryDto) {
    const { offset, limit, search } = query;
    return this.aggregate.listPlans(offset, limit, search);
  }
}

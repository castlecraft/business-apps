import {
  IsDateString,
  IsNumberString,
  IsString,
  IsUUID,
} from 'class-validator';

export class ListErrorLogDto {
  @IsUUID()
  uuid: string;
  @IsString()
  message: string;
  @IsString()
  fromService: string;
  @IsDateString()
  fromDateTime: string;
  @IsDateString()
  toDateTime: string;
  @IsNumberString()
  offset: string;
  @IsNumberString()
  limit: string;
}

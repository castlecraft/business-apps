import { TokenCacheService, UserService } from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { AdminTenantController } from './admin-tenant.controller';

describe('AdminTenantController', () => {
  let controller: AdminTenantController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AdminTenantController],
      imports: [CqrsModule],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
      ],
    }).compile();

    controller = module.get<AdminTenantController>(AdminTenantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

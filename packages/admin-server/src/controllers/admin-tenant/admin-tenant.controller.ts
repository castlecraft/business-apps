import {
  RoleGuard,
  Roles,
  TokenGuard,
  User,
  UserGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddTenantUserCommand } from '../../commands/add-tenant-user/add-tenant-user.command';
import { AddTenantCommand } from '../../commands/add-tenant/add-tenant.command';
import { CreateTenantRoleCommand } from '../../commands/create-tenant-role/create-tenant-role.command';
import { DeleteTenantRoleCommand } from '../../commands/delete-tenant-role/delete-tenant-role.command';
import { EditTenantUserCommand } from '../../commands/edit-tenant-user/edit-tenant-user.command';
import { RemoveTenantUserCommand } from '../../commands/remove-tenant-user/remove-tenant-user.command';
import { RemoveTenantCommand } from '../../commands/remove-tenant/remove-tenant.command';
import { UpdateTenantCommand } from '../../commands/update-tenant/update-tenant.command';
import { ADMINISTRATOR } from '../../constants/strings';
import { FetchTenantUserQuery } from '../../queries/fetch-tenant-user/fetch-tenant-user.query';
import { FetchTenantQuery } from '../../queries/fetch-tenant/fetch-tenant.query';
import { ListTenantUserQuery } from '../../queries/list-tenant-user/list-tenant-user.query';
import { ListTenantQuery } from '../../queries/list-tenant/list-tenant.query';
import { ListTenantUserDto } from '../tenant/list-tenant-user.dto';
import { ListTenantDto } from '../tenant/list-tenant.dto';
import { TenantRoleDto } from '../tenant/tenant-role.dto';
import { TenantUserDto } from '../tenant/tenant-user.dto';
import { TenantDto } from '../tenant/tenant.dto';

@Controller('admin/tenant')
@UseGuards(TokenGuard, UserGuard, new RoleGuard(new Reflector()))
@UsePipes(new ValidationPipe())
export class AdminTenantController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/add')
  @Roles(ADMINISTRATOR)
  async addTenant(
    @Body() payload: TenantDto & { [key: string]: any },
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new AddTenantCommand(req?.user, payload),
    );
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  async updateTenant(
    @Body() payload: TenantDto & { [key: string]: any },
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new UpdateTenantCommand(req?.user, payload),
    );
  }

  @Post('v1/remove/:uuid')
  @Roles(ADMINISTRATOR)
  async removeTenant(
    @Param('uuid') uuid: string,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new RemoveTenantCommand(req?.user, uuid),
    );
  }

  @Get('v1/fetch/:uuid')
  @Roles(ADMINISTRATOR)
  async getTenant(
    @Param('uuid') uuid: string,
    @Req() req: unknown & { user: User },
  ) {
    return await this.queryBus.execute(new FetchTenantQuery(uuid));
  }

  @Get('v1/list')
  @Roles(ADMINISTRATOR)
  async listTenants(
    @Query() query: ListTenantDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.queryBus.execute(new ListTenantQuery(req?.user, query));
  }

  @Post('role/v1/add')
  @Roles(ADMINISTRATOR)
  async addRole(
    @Body() payload: TenantRoleDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new CreateTenantRoleCommand(req.user, payload.roleName),
    );
  }

  @Post('role/v1/remove/:roleName')
  @Roles(ADMINISTRATOR)
  async removeRole(
    @Param('roleName') roleName: string,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new DeleteTenantRoleCommand(req.user, roleName),
    );
  }

  @Post('user/v1/add')
  @Roles(ADMINISTRATOR)
  async addTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new AddTenantUserCommand(req.user, payload),
    );
  }

  @Post('user/v1/update')
  @Roles(ADMINISTRATOR)
  async editTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new EditTenantUserCommand(req.user, payload),
    );
  }

  @Post('user/v1/remove')
  @Roles(ADMINISTRATOR)
  async removeTenantUser(
    @Body() payload: TenantUserDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.commandBus.execute(
      new RemoveTenantUserCommand(req.user, payload),
    );
  }

  @Get('user/v1/fetch')
  @Roles(ADMINISTRATOR)
  async getTenantUser(
    @Query() query: Omit<TenantUserDto, 'roles'>,
    @Req() req: unknown & { user: User },
  ) {
    return await this.queryBus.execute(new FetchTenantUserQuery(query));
  }

  @Get('user/v1/list')
  @Roles(ADMINISTRATOR)
  async listTenantUser(
    @Query() query: ListTenantUserDto,
    @Req() req: unknown & { user: User },
  ) {
    return await this.queryBus.execute(
      new ListTenantUserQuery(req.user, query),
    );
  }
}

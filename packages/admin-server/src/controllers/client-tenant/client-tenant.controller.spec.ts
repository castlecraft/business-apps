import { ClientService, TokenCacheService } from '@castlecraft/auth';
import { CqrsModule } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { ClientTenantController } from './client-tenant.controller';

describe('ClientTenantController', () => {
  let controller: ClientTenantController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ClientTenantController],
      imports: [CqrsModule],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    controller = module.get<ClientTenantController>(ClientTenantController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

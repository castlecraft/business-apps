import {
  Controller,
  Post,
  UseGuards,
  UsePipes,
  ValidationPipe,
  Param,
  Body,
  Req,
  Query,
  Get,
} from '@nestjs/common';
import { ListQueryDto } from '../plan/list-query.dto';
import { AddOnAggregateService } from '../../aggregates/add-on-aggregate/add-on-aggregate.service';
import { RoleGuard, Roles, TokenGuard, UserGuard } from '@castlecraft/auth';
import { ADMINISTRATOR } from '../../constants/strings';
import { AddOnDto, UpdateAddOnDto } from '../../dto/add-on/add-on.dto';
import { Reflector } from '@nestjs/core';

@Controller('add-on')
@UseGuards(TokenGuard, UserGuard, new RoleGuard(new Reflector()))
export class AddOnController {
  constructor(private readonly aggregate: AddOnAggregateService) {}

  @Post('v1/create')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  create(@Body() addOnPayload: AddOnDto) {
    return this.aggregate.createAddOn(addOnPayload);
  }

  @Post('v1/delete/:uuid')
  @Roles(ADMINISTRATOR)
  remove(@Param('uuid') uuid: string) {
    return this.aggregate.deleteAddOn(uuid);
  }

  @Post('v1/update')
  @Roles(ADMINISTRATOR)
  @UsePipes(new ValidationPipe({ whitelist: true }))
  update(@Body() updatePayload: UpdateAddOnDto, @Req() req) {
    return this.aggregate.updateAddOn(updatePayload);
  }

  // TODO: Move below to public controller

  @Get('v1/get/:uuid')
  async getAdd0n(@Param('uuid') uuid) {
    return await this.aggregate.retrieveAddOn(uuid);
  }

  @Get('v1/list')
  listAdd0ns(@Query() query: ListQueryDto) {
    const { offset, limit, search } = query;
    return this.aggregate.listAddOns(offset, limit, search);
  }
}

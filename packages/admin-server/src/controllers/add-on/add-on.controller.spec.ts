import { TokenGuard } from '@castlecraft/auth';
import { Test, TestingModule } from '@nestjs/testing';
import { AddOnAggregateService } from '../../aggregates/add-on-aggregate/add-on-aggregate.service';
import { AddOnController } from './add-on.controller';

describe('AddOnController', () => {
  let controller: AddOnController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AddOnController],
      providers: [
        {
          provide: AddOnAggregateService,
          useValue: {},
        },
      ],
    })
      .overrideGuard(TokenGuard)
      .useValue({})
      .compile();

    controller = module.get<AddOnController>(AddOnController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

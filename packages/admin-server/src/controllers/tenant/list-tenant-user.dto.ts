import { IsNumberString, IsOptional, IsUUID } from 'class-validator';

export class ListTenantUserDto {
  @IsOptional()
  @IsUUID()
  tenant: string;
  @IsOptional()
  @IsUUID()
  user: string;
  @IsNumberString()
  @IsOptional()
  offset: string;
  @IsNumberString()
  @IsOptional()
  limit: string;
  @IsOptional()
  sort: string;
}

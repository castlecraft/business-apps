import { IsOptional, IsUUID } from 'class-validator';

export class TenantDto {
  @IsUUID()
  @IsOptional()
  uuid: string;
}

import { IsOptional, IsString, IsUUID } from 'class-validator';

export class TenantUserDto {
  @IsUUID()
  user: string;
  @IsUUID()
  tenant: string;
  @IsOptional()
  @IsString({ each: true })
  roles: string[];
}

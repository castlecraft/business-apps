import { IsString, IsUUID } from 'class-validator';

export class TenantUserRoleDto {
  @IsString()
  roleName: string;
  @IsUUID()
  tenantId: string;
  @IsUUID()
  userUuid: string;
}

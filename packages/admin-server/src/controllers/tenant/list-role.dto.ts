import { IsNumberString, IsOptional, IsUUID } from 'class-validator';

export class ListRoleDto {
  @IsUUID()
  @IsOptional()
  roleName: string;
  @IsNumberString()
  @IsOptional()
  offset: string;
  @IsNumberString()
  @IsOptional()
  limit: string;
  @IsOptional()
  sort: string;
}

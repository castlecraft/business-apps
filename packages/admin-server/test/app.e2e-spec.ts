import {
  ClientService,
  TokenCacheService,
  UserService,
} from '@castlecraft/auth';
import { INestApplication } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { Test } from '@nestjs/testing';
import * as request from 'supertest';
import { AdminTenantController } from '../src/controllers/admin-tenant/admin-tenant.controller';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [CqrsModule],
      controllers: [AdminTenantController],
      providers: [
        { provide: TokenCacheService, useValue: {} },
        { provide: UserService, useValue: {} },
        { provide: ClientService, useValue: {} },
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', async () => {
    return await request(app.getHttpServer()).get('/');
  });

  afterAll(async () => {
    await app.close();
  });
});

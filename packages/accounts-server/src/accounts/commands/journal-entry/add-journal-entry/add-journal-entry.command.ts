import { ICommand } from '@nestjs/cqrs';
import { TenantUser } from '@castlecraft/auth';
import { JournalEntryDto } from '../../../entities/journal-entry/journal-entry.dto';

export class AddJournalEntryCommand implements ICommand {
  constructor(
    public journalEntryPayload: JournalEntryDto,
    public readonly tenantUser: TenantUser,
  ) {}
}

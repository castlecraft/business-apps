import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { JournalEntryAggregateService } from '../../../aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { RemoveJournalEntryCommand } from './remove-journal-entry.command';

@CommandHandler(RemoveJournalEntryCommand)
export class RemoveJournalEntryHandler
  implements ICommandHandler<RemoveJournalEntryCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: JournalEntryAggregateService,
  ) {}

  async execute(command: RemoveJournalEntryCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeJournalEntry(uuid);
    aggregate.commit();
  }
}

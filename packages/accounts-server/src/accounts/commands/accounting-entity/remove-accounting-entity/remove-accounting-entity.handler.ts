import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { RemoveAccountingEntityCommand } from './remove-accounting-entity.command';

@CommandHandler(RemoveAccountingEntityCommand)
export class RemoveAccountingEntityHandler
  implements ICommandHandler<RemoveAccountingEntityCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: AccountingEntityAggregateService,
  ) {}
  async execute(command: RemoveAccountingEntityCommand) {
    const { uuid } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await this.manager.removeAccountingEntity(uuid);
    aggregate.commit();
  }
}

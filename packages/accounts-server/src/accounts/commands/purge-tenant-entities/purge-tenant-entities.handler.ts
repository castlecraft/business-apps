import { CommandHandler, EventPublisher, ICommandHandler } from '@nestjs/cqrs';
import { TenancyAggregateService } from '../../aggregates/tenancy-aggregate/tenancy-aggregate.service';
import { PurgeTenantEntitiesCommand } from './purge-tenant-entities.command';

@CommandHandler(PurgeTenantEntitiesCommand)
export class PurgeTenantEntitiesHandler
  implements ICommandHandler<PurgeTenantEntitiesCommand>
{
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: TenancyAggregateService,
  ) {}

  async execute(command: PurgeTenantEntitiesCommand) {
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await aggregate.purgeTenantResources(command.tenantId);
    aggregate.commit();
  }
}

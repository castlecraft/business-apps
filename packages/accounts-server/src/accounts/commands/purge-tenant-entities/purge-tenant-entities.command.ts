import { ICommand } from '@nestjs/cqrs';

export class PurgeTenantEntitiesCommand implements ICommand {
  constructor(public readonly tenantId: string) {}
}

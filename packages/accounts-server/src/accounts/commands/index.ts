import { AddAccountHandler } from './account/add-account/add-account.handler';
import { RemoveAccountHandler } from './account/remove-account/remove-account.handler';
import { UpdateAccountHandler } from './account/update-account/update-account.handler';
import { AddAccountingEntityHandler } from './accounting-entity/add-accounting-entity/add-accounting-entity.handler';
import { RemoveAccountingEntityHandler } from './accounting-entity/remove-accounting-entity/remove-accounting-entity.handler';
import { UpdateAccountingEntityHandler } from './accounting-entity/update-accounting-entity/update-accounting-entity.handler';
import { AddJournalEntryHandler } from './journal-entry/add-journal-entry/add-journal-entry.handler';
import { RemoveJournalEntryHandler } from './journal-entry/remove-journal-entry/remove-journal-entry.handler';
import { UpdateJournalEntryHandler } from './journal-entry/update-journal-entry/update-journal-entry.handler';
import { PurgeAccountingEntityHandler } from './purge-accounting-entity/purge-accounting-entity.handler';
import { PurgeTenantEntitiesHandler } from './purge-tenant-entities/purge-tenant-entities.handler';

export const AccountsCommandManager = [
  AddAccountHandler,
  RemoveAccountHandler,
  UpdateAccountHandler,

  AddAccountingEntityHandler,
  RemoveAccountingEntityHandler,
  UpdateAccountingEntityHandler,
  PurgeAccountingEntityHandler,

  AddJournalEntryHandler,
  RemoveJournalEntryHandler,
  UpdateJournalEntryHandler,

  PurgeTenantEntitiesHandler,
];

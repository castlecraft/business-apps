import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from '../../../entities/account/account.service';
import { AccountingEntityService } from '../../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryPoliciesService } from './journal-entry-policies.service';

describe('JournalEntryPoliciesService', () => {
  let service: JournalEntryPoliciesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryPoliciesService,
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: AccountingEntityService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<JournalEntryPoliciesService>(
      JournalEntryPoliciesService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

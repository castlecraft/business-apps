import { Test, TestingModule } from '@nestjs/testing';
import { ACCOUNTING_ENTITY } from './accounting-entity.schema';
import { AccountingEntityService } from './accounting-entity.service';

describe('AccountingEntityService', () => {
  let service: AccountingEntityService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountingEntityService,
        {
          provide: ACCOUNTING_ENTITY,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AccountingEntityService>(AccountingEntityService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

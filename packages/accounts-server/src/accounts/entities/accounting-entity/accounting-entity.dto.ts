import {
  IsDateString,
  IsEnum,
  IsOptional,
  IsString,
  IsUUID,
} from 'class-validator';

export enum EntityType {
  SoleProprietorship = 'Sole Proprietorship',
  Partnership = 'Partnership',
  CCorporation = 'C Corporation',
  SCorporation = 'S Corporation',
  LimitedLiabilityCompany = 'Limited Liability Company',
  LimitedLiabilityPartnership = 'Limited Liability Partnership',
  PrivateLimited = 'Private Limited',
  PublicLimited = 'Public Limited',
  Other = 'Other',
}

export class AccountingEntityDto {
  // TODO: remove optional
  @IsOptional()
  @IsUUID()
  tenantId: string;

  @IsOptional()
  @IsUUID()
  uuid: string;

  @IsString()
  name: string;

  @IsEnum(EntityType)
  entityType: string;

  @IsString()
  taxId: string;

  @IsOptional()
  @IsString()
  baseCurrency: string;

  @IsOptional()
  @IsString()
  country: string;

  @IsOptional()
  @IsString()
  abbr: string;

  @IsOptional()
  @IsDateString()
  dateOfBirth: string;
}

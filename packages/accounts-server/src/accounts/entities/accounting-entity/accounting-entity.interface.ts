import { Document } from 'mongoose';

export interface AccountingEntity extends Document {
  tenantId: string;
  uuid: string;
  name: string;
  entityType: string;
  taxId: string;
  baseCurrency: string;
  country: string;
  abbr: string;
  dateOfBirth: Date;
  isImmutable: boolean;
}

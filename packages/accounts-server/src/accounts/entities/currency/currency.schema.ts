import * as mongoose from 'mongoose';

const schema = new mongoose.Schema(
  {
    name: { type: String },
    code: { type: String, unique: true, index: true },
    minorUnit: { type: Number },
  },
  { collection: 'currencies', versionKey: false },
);

export const Currency = schema;

export const CURRENCY = 'Currency';

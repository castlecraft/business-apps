import { IsNotEmpty, IsNumberString, IsString } from 'class-validator';
// import { NumberFormat } from './currency.interface';

export class CurrencyDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  code: string;

  @IsNotEmpty()
  @IsNumberString()
  minorUnit: string;
}

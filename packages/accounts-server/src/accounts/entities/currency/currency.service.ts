import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { stringEscape } from '../../../common/utils';
import { ListCurrencyDto } from '../../controllers/currency/list-currency.dto';
import { Currency } from './currency.interface';
import { CURRENCY } from './currency.schema';

@Injectable()
export class CurrencyService {
  constructor(
    @Inject(CURRENCY)
    public readonly model: Model<Currency>,
  ) {}

  async create(currency: Currency) {
    return await this.model.create(currency);
  }

  async list(query: ListCurrencyDto) {
    const { offset, limit, sort, ...filterQuery } = query;

    Object.keys(filterQuery).forEach(key => {
      if (filterQuery[key] === undefined) {
        delete filterQuery[key];
      } else {
        try {
          filterQuery[key] = new RegExp(stringEscape(filterQuery[key]), 'i');
        } catch (error) {}
      }
    });

    const sortOrder = sort
      ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
      : { code: 1 };

    const results = await this.model
      .find(filterQuery)
      .skip(Number(offset) || 0)
      .limit(Number(limit) || 10)
      .sort(sortOrder);

    return {
      docs: results || [],
      length: await this.model.countDocuments(filterQuery),
      offset,
    };
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async updateOne(query, param) {
    return await this.model.updateOne(query, param);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }
}

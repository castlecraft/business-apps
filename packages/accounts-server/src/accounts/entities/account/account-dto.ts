import { IsNotEmpty, IsOptional, IsString, IsUUID } from 'class-validator';

export class AccountDto {
  @IsOptional()
  @IsString()
  uuid: string;

  @IsNotEmpty()
  @IsString()
  tenantId: string;

  @IsOptional()
  level: string;

  @IsOptional()
  @IsUUID()
  accountingEntity: string;

  @IsOptional()
  @IsString()
  currency: string;

  @IsOptional()
  @IsUUID()
  parentAccount: string;

  @IsOptional()
  @IsString()
  accountName: string;

  @IsOptional()
  @IsString()
  accountNumber: string;

  @IsOptional()
  @IsUUID()
  parentAccountNumber: string;
}

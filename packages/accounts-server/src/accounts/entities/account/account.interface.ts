import { Document } from 'mongoose';
import { TranslateStringAny } from '../../../common/utils';

export enum BalanceType {
  Debit = 'Debit',
  Credit = 'Credit',
  DebitOrCredit = 'Debit or Credit',
}

export interface Account extends Document {
  uuid: string;
  tenantId: string;
  accountingEntity: string;
  currency: string;
  parentAccount: string;
  parentAccountNumber: string;
  accountName: TranslateStringAny;
  accountNumber: string;
  level: number;
  balanceType: BalanceType;
  classification: string;
}

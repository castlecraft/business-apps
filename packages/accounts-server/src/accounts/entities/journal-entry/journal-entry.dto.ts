import { Type } from 'class-transformer';
import {
  IsOptional,
  IsUUID,
  IsString,
  IsDateString,
  ValidateNested,
  IsNumber,
  IsNotEmpty,
} from 'class-validator';

export class JournalEntryDto {
  @IsOptional()
  @IsUUID()
  uuid: string;

  @IsNotEmpty()
  @IsUUID()
  tenantId: string;

  @IsNotEmpty()
  @IsUUID()
  accountingEntity: string;

  @IsOptional()
  @IsDateString()
  date: Date;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => JournalAccount)
  accounts: JournalAccount[];
}

export class JournalAccount {
  @IsNotEmpty()
  @IsUUID()
  account: string;

  @IsOptional()
  @IsString()
  accountNameEn: string;

  @IsOptional()
  @IsNumber()
  debit: number;

  @IsOptional()
  @IsNumber()
  credit: number;

  @IsOptional()
  @IsNumber()
  debitInAccountCurrency: number;

  @IsOptional()
  @IsNumber()
  creditInAccountCurrency: number;

  @IsOptional()
  @IsString()
  transactionLink: string;

  @IsOptional()
  @IsString()
  remark: string;
}

import { Test, TestingModule } from '@nestjs/testing';
import { JOURNAL_ENTRY } from './journal-entry.schema';
import { JournalEntryService } from './journal-entry.service';

describe('JournalEntryService', () => {
  let service: JournalEntryService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryService,
        {
          provide: JOURNAL_ENTRY,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<JournalEntryService>(JournalEntryService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

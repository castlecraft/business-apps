import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4, index: true },
    tenantId: { type: String, index: true },
    accountingEntity: { type: String, index: true },
    date: { type: Date, default: () => new Date() },
    accounts: { type: [mongoose.Schema.Types.Mixed] },
  },
  { collection: 'journal_entries', versionKey: false },
);

export const JournalEntry = schema;

export const JOURNAL_ENTRY = 'JournalEntry';

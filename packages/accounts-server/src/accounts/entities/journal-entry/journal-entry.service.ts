import { Inject, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { JournalEntry } from './journal-entry.interface';
import { JOURNAL_ENTRY } from './journal-entry.schema';

@Injectable()
export class JournalEntryService {
  constructor(
    @Inject(JOURNAL_ENTRY)
    public readonly model: Model<JournalEntry>,
  ) {}

  async create(journalEntry: JournalEntry) {
    return await this.model.create(journalEntry);
  }

  async findOne(query) {
    return await this.model.findOne(query);
  }

  async deleteOne(query) {
    return await this.model.deleteOne(query);
  }

  async updateOne(query, param) {
    return await this.model.updateOne(query, param);
  }

  async deleteMany(filter, options?) {
    return await this.model.deleteMany(filter, options);
  }
}

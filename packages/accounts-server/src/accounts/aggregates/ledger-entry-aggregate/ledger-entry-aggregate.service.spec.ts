import { Test, TestingModule } from '@nestjs/testing';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { LedgerEntryAggregateService } from './ledger-entry-aggregate.service';

describe('LedgerEntryAggregateService', () => {
  let service: LedgerEntryAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        LedgerEntryAggregateService,
        {
          provide: LedgerEntryService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<LedgerEntryAggregateService>(
      LedgerEntryAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

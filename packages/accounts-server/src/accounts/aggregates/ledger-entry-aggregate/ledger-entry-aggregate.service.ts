import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { ListLedgerEntryDto } from '../../controllers/ledger-entry/list-ledger-entry.dto';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { stringEscape } from '../../../common/utils';

@Injectable()
export class LedgerEntryAggregateService extends AggregateRoot {
  constructor(private readonly ledgerEntryService: LedgerEntryService) {
    super();
  }

  async listLedgerEntry(query: ListLedgerEntryDto) {
    const { limit, offset, sort, ...filters } = query;
    const sortOrder = sort
      ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
      : { uuid: 1 };

    if (filters?.fromDate && filters?.toDate) {
      filters.transactionDate = {
        $gte: new Date(Number(filters.fromDate)),
        $lte: new Date(Number(filters.toDate)),
      };
    }

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined) {
        delete filters[key];
      } else if (key === 'fromDate' || key === 'toDate') {
        delete filters[key];
      } else {
        try {
          filters[key] = new RegExp(stringEscape(filters[key]), 'i');
        } catch (error) {}
      }
    });

    const results = await this.ledgerEntryService.model
      .aggregate([
        { $match: filters },
        {
          $lookup: {
            from: 'accounts',
            localField: 'account',
            foreignField: 'uuid',
            as: 'accountDetails',
          },
        },
        {
          $addFields: {
            accountNameEn: '$accountDetails.accountName.en',
            account: '$accountDetails.uuid',
          },
        },
        { $unwind: '$accountNameEn' },
        { $unwind: '$account' },
        { $project: { accountDetails: 0 } },
        {
          $lookup: {
            from: 'accounting_entities',
            localField: 'accountingEntity',
            foreignField: 'uuid',
            as: 'accountingEntity',
          },
        },
        { $addFields: { entity: '$accountingEntity.name' } },
        { $unwind: '$entity' },
        { $project: { accountingEntity: 0 } },
      ])
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(Number(limit || 10))
      .skip(Number(offset || 0))
      .sort(sortOrder);

    return {
      docs: results || [],
      length: await this.ledgerEntryService.model.countDocuments(filters),
      offset,
    };
  }
}

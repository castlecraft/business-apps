import { Test, TestingModule } from '@nestjs/testing';
import { CurrencyService } from '../../entities/currency/currency.service';
import { AccountService } from '../../entities/account/account.service';
import { AccountsAggregateService } from './accounts-aggregate.service';

describe('AccountsAggregateService', () => {
  let service: AccountsAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AccountsAggregateService,
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: CurrencyService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<AccountsAggregateService>(AccountsAggregateService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

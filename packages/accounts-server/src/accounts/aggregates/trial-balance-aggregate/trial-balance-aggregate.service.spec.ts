import { Test, TestingModule } from '@nestjs/testing';
import { AccountService } from '../../entities/account/account.service';
import { TrialBalanceAggregateService } from './trial-balance-aggregate.service';

describe('TrialBalanceAggregateService', () => {
  let service: TrialBalanceAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TrialBalanceAggregateService,
        {
          provide: AccountService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<TrialBalanceAggregateService>(
      TrialBalanceAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

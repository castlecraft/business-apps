import { Injectable } from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { ListTrialBalanceDto } from '../../controllers/trial-balance/trial-balance.dto';
import { AccountService } from '../../entities/account/account.service';

@Injectable()
export class TrialBalanceAggregateService extends AggregateRoot {
  constructor(private readonly accountService: AccountService) {
    super();
  }

  async list(query: ListTrialBalanceDto) {
    return this.accountService.list(query);
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { JournalEntryPoliciesService } from '../../policies/journal-entry-policies/journal-entry-policies/journal-entry-policies.service';
import { AccountService } from '../../entities/account/account.service';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { JournalEntryAggregateService } from './journal-entry-aggregate.service';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';

describe('JournalEntryAggregateService', () => {
  let service: JournalEntryAggregateService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        JournalEntryAggregateService,
        {
          provide: JournalEntryService,
          useValue: {},
        },
        {
          provide: AccountingEntityService,
          useValue: {},
        },
        {
          provide: AccountService,
          useValue: {},
        },
        {
          provide: JournalEntryPoliciesService,
          useValue: {},
        },
        {
          provide: LedgerEntryService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<JournalEntryAggregateService>(
      JournalEntryAggregateService,
    );
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { JournalEntryDto } from '../../entities/journal-entry/journal-entry.dto';
import { JournalEntry } from '../../entities/journal-entry/journal-entry.interface';
import { JournalEntryService } from '../../entities/journal-entry/journal-entry.service';
import { JournalEntryAddedEvent } from '../../events/journal-entry/journal-entry-added/journal-entry-added.event';
import { JournalEntryUpdatedEvent } from '../../events/journal-entry/journal-entry-updated/journal-entry-updated.event';
import { stringEscape } from '../../../common/utils';
import { v4 as uuidv4 } from 'uuid';
import { JournalEntryRemovedEvent } from '../../events/journal-entry/journal-entry-removed/journal-entry-removed.event';
import { AccountingEntityService } from '../../entities/accounting-entity/accounting-entity.service';
import { AccountService } from '../../entities/account/account.service';
import { JournalEntryPoliciesService } from '../../policies/journal-entry-policies/journal-entry-policies/journal-entry-policies.service';
import { LedgerEntry } from '../../entities/ledger-entry/ledger-entry.interface';
import { LedgerEntryService } from '../../entities/ledger-entry/ledger-entry.service';
import { ListJournalEntryDto } from '../../controllers/journal-entry/list-Journal-entry.dto';

@Injectable()
export class JournalEntryAggregateService extends AggregateRoot {
  constructor(
    private readonly journalEntryService: JournalEntryService,
    private readonly accountingEntityService: AccountingEntityService,
    private readonly accountService: AccountService,
    private readonly journalEntryPolicies: JournalEntryPoliciesService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {
    super();
  }
  async addJournalEntry(journalEntryPayload: JournalEntryDto) {
    const ledgerEntries: LedgerEntry[] = [];

    const accountingEntityExists = await this.accountingEntityService.findOne({
      uuid: journalEntryPayload.accountingEntity,
    });
    if (!accountingEntityExists) {
      throw new NotFoundException({
        AccountingEntityNotFound: journalEntryPayload.accountingEntity,
      });
    }

    journalEntryPayload.accounts.forEach(async journalEntry => {
      const accountsExists = await this.accountService.findOne({
        uuid: journalEntry.account,
      });
      if (!accountsExists) {
        throw new NotFoundException({
          AccountNotFound: journalEntry.account,
        });
      }

      const ledgerEntry = {} as LedgerEntry;
      ledgerEntry.uuid = uuidv4();
      ledgerEntry.account = journalEntry.account;
      ledgerEntry.debitAmount = journalEntry.debit;
      ledgerEntry.creditAmount = journalEntry.credit;
      ledgerEntry.accountCurrency = accountsExists.currency;
      ledgerEntry.debitInAccountCurrency = journalEntry?.debitInAccountCurrency;
      ledgerEntry.creditInAccountCurrency =
        journalEntry?.creditInAccountCurrency;
      ledgerEntry.journalEntry = journalEntryPayload?.uuid;
      ledgerEntry.remarks = journalEntry?.remark;
      ledgerEntry.accountingEntity = journalEntryPayload.accountingEntity;
      ledgerEntries.push(ledgerEntry);
    });

    let sumOfCredit = 0;
    let sumOfDebit = 0;
    let sumOfCreditInAccountCurrency = 0;
    let sumOfDebitInAccountCurrency = 0;
    journalEntryPayload.accounts.forEach(async account => {
      sumOfCredit = sumOfCredit + account.credit;
      sumOfDebit = sumOfDebit + account.debit;

      sumOfCreditInAccountCurrency =
        sumOfCreditInAccountCurrency + account.creditInAccountCurrency;
      sumOfDebitInAccountCurrency =
        sumOfDebitInAccountCurrency + account.debitInAccountCurrency;

      const childrenExists = await this.accountService.findOne({
        parentAccount: account.account,
      });
      if (childrenExists) {
        return new BadRequestException({
          ChildrenExistsFor: account.account,
        });
      }
    });

    if (sumOfCredit !== sumOfDebit) {
      throw new BadRequestException({
        CreditNotEqualToDebit: { sumOfDebit, sumOfCredit },
      });
    }

    if (sumOfCreditInAccountCurrency !== sumOfDebitInAccountCurrency) {
      throw new BadRequestException({
        sumOfCreditInAccountCurrencyNotEqualTosumOfDebitInAccountCurrency: {
          sumOfCreditInAccountCurrency,
          sumOfDebitInAccountCurrency,
        },
      });
    }

    journalEntryPayload = await this.journalEntryPolicies.validateCurrency(
      journalEntryPayload,
    );

    if (this.journalEntryPolicies.validateCurrency(journalEntryPayload)) {
      const journalEntry = {} as JournalEntry;
      Object.assign(journalEntry, journalEntryPayload);
      journalEntry.uuid = uuidv4();
      journalEntry.date = journalEntryPayload.date
        ? new Date(journalEntryPayload.date)
        : new Date();
      this.apply(new JournalEntryAddedEvent(journalEntry, ledgerEntries));
      return journalEntry;
    }
  }

  async listJournalEntry(query: ListJournalEntryDto) {
    const { limit, offset, sort, ...filters } = query;
    const sortOrder = sort
      ? { [sort.split(' ')[0]]: sort.split(' ')[1] === 'asc' ? 1 : -1 }
      : { uuid: 1 };

    if (filters?.fromDate && filters?.toDate) {
      filters.date = {
        $gte: new Date(Number(filters.fromDate)),
        $lte: new Date(Number(filters.toDate)),
      };
      delete filters.fromDate;
      delete filters.toDate;
    }

    Object.keys(filters).forEach(key => {
      if (filters[key] === undefined) {
        delete filters[key];
      } else if (key === 'fromDate' || key === 'toDate') {
        delete filters[key];
      } else {
        try {
          filters[key] = new RegExp(stringEscape(filters[key]), 'i');
        } catch (error) {}
      }
    });

    const results = await this.journalEntryService.model
      .aggregate([
        { $match: filters },
        {
          $lookup: {
            from: 'accounting_entities',
            localField: 'accountingEntity',
            foreignField: 'uuid',
            as: 'accountingEntity',
          },
        },
        { $addFields: { name: '$accountingEntity.name' } },
        { $unwind: '$name' },
        { $project: { accountingEntity: 0 } },
      ])
      .collation({ locale: 'en_US', numericOrdering: true })
      .limit(Number(limit || 10))
      .skip(Number(offset || 0))
      .sort(sortOrder);

    return {
      docs: results || [],
      length: await this.journalEntryService.model.countDocuments(filters),
      offset,
    };
  }

  async removeJournalEntry(uuid: string) {
    const journalEntry = await this.journalEntryService.findOne({
      uuid,
    });
    if (!journalEntry) {
      throw new NotFoundException();
    }
    this.apply(new JournalEntryRemovedEvent(journalEntry));
  }

  async updateJournalEntry(journalEntryPayload: JournalEntryDto) {
    const provider = await this.journalEntryService.findOne({
      uuid: journalEntryPayload.uuid,
    });

    if (!provider) {
      throw new NotFoundException();
    }

    let sumOfCredit = 0;
    let sumOfDebit = 0;
    journalEntryPayload.accounts.forEach(async account => {
      sumOfCredit = sumOfCredit + account.credit;
      sumOfDebit = sumOfDebit + account.debit;
    });
    if (sumOfCredit !== sumOfDebit) {
      throw new BadRequestException({
        CreditNotEqualToDebit: { sumOfDebit, sumOfCredit },
      });
    }

    const ledgerEntryPayload = await this.ledgerEntryService.find({
      journalEntry: journalEntryPayload.uuid,
    });

    ledgerEntryPayload.forEach(async (ledgerEntry, index) => {
      ledgerEntry.uuid = uuidv4();
      ledgerEntry.journalEntry = journalEntryPayload?.uuid;
      ledgerEntry.accountingEntity = journalEntryPayload?.accountingEntity;
      ledgerEntry.transactionDate = new Date();
      ledgerEntry.account = journalEntryPayload.accounts[index]?.account;
      ledgerEntry.debitAmount = journalEntryPayload.accounts[index]?.debit;
      ledgerEntry.creditAmount = journalEntryPayload.accounts[index]?.credit;
      ledgerEntry.debitInAccountCurrency =
        journalEntryPayload.accounts[index]?.debitInAccountCurrency;
      ledgerEntry.creditInAccountCurrency =
        journalEntryPayload.accounts[index]?.creditInAccountCurrency;
      ledgerEntry.remarks = journalEntryPayload.accounts[index]?.remark;
      const accountDetails = await this.accountService.findOne({
        uuid: journalEntryPayload.accounts[index].account,
      });
      ledgerEntry.accountCurrency = accountDetails?.currency;
    });

    this.apply(
      new JournalEntryUpdatedEvent(journalEntryPayload, ledgerEntryPayload),
    );
  }

  async getJournalEntry(uuid: string) {
    const entity = await this.journalEntryService.findOne(uuid);
    if (!entity) {
      throw new NotFoundException({ JournalEntryNotFound: uuid });
    }
    return entity;
  }
}

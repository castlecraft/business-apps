import { IEvent } from '@nestjs/cqrs';
import { LedgerEntry } from '../../../entities/ledger-entry/ledger-entry.interface';
import { JournalEntry } from '../../../entities/journal-entry/journal-entry.interface';

export class JournalEntryAddedEvent implements IEvent {
  constructor(
    public readonly journalEntry: JournalEntry,
    public readonly ledgerEntries: LedgerEntry[],
  ) {}
}

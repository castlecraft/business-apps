import { IEvent } from '@nestjs/cqrs';
import { JournalEntryDto } from '../../../entities/journal-entry/journal-entry.dto';
import { LedgerEntry } from '../../../entities/ledger-entry/ledger-entry.interface';

export class JournalEntryUpdatedEvent implements IEvent {
  constructor(
    public readonly journalEntryPayload: JournalEntryDto,
    public readonly ledgerEntryPayload: LedgerEntry[],
  ) {}
}

import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { LedgerEntryService } from '../../../entities/ledger-entry/ledger-entry.service';
import { JournalEntryService } from '../../../entities/journal-entry/journal-entry.service';
import { JournalEntryUpdatedEvent } from './journal-entry-updated.event';

@EventsHandler(JournalEntryUpdatedEvent)
export class JournalEntryUpdatedHandler
  implements IEventHandler<JournalEntryUpdatedEvent>
{
  constructor(
    private readonly journalEntryService: JournalEntryService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {}

  async handle(event: JournalEntryUpdatedEvent) {
    const { journalEntryPayload, ledgerEntryPayload } = event;
    this.journalEntryService.updateOne(
      { uuid: journalEntryPayload.uuid },
      { $set: journalEntryPayload },
    );
    await this.ledgerEntryService.deleteMany({
      journalEntry: journalEntryPayload.uuid,
    });
    await this.ledgerEntryService.insertMany(ledgerEntryPayload);
  }
}

import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { CurrencyService } from '../../../../accounts/entities/currency/currency.service';
import { CurrencyUpdatedEvent } from './currency-updated.event';

@EventsHandler(CurrencyUpdatedEvent)
export class CurrencyUpdatedHandler
  implements IEventHandler<CurrencyUpdatedEvent>
{
  constructor(private readonly object: CurrencyService) {}

  async handle(event: CurrencyUpdatedEvent) {
    const { updatePayload } = event;
    await this.object.updateOne(
      { code: updatePayload.code },
      { $set: updatePayload },
    );
  }
}

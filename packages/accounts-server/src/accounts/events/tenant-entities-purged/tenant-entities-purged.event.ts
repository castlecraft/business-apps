import { IEvent } from '@nestjs/cqrs';

export class TenantEntitiesPurgedEvent implements IEvent {
  constructor(public readonly tenantId: string) {}
}

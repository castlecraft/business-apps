import { IEvent } from '@nestjs/cqrs';
import { AccountingEntity } from '../../../entities/accounting-entity/accounting-entity.interface';

export class AccountingEntityRemovedEvent implements IEvent {
  constructor(public accountingEntity: AccountingEntity) {}
}

import { BadRequestException } from '@nestjs/common';
import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import * as csv from 'fast-csv';
import * as fs from 'fs';
import * as path from 'path';
import { from, throwError } from 'rxjs';
import { filter, mergeMap, switchMap, toArray } from 'rxjs/operators';
import { fromStream } from '../../../../common/utils';
import { Account } from '../../../entities/account/account.interface';
import { AccountService } from '../../../entities/account/account.service';
import { AccountingEntityService } from '../../../entities/accounting-entity/accounting-entity.service';
import { AccountingEntityAddedEvent } from './accounting-entity-added.event';

@EventsHandler(AccountingEntityAddedEvent)
export class AccountingEntityAddedHandler
  implements IEventHandler<AccountingEntityAddedEvent>
{
  constructor(
    private readonly accountingEntityService: AccountingEntityService,
    private readonly accountService: AccountService,
  ) {}

  async handle(event: AccountingEntityAddedEvent) {
    const { accountingEntity, tenantUser, chartType } = event;
    await this.accountingEntityService.create(accountingEntity);
    await this.generateTree(
      accountingEntity.uuid,
      tenantUser.tenant,
      accountingEntity.baseCurrency,
      chartType,
    ).toPromise();
  }

  generateTree(
    accountingEntity: string,
    tenantId: string,
    currency: string = 'INR',
    chartType: string = 'accounts-gaap.csv',
  ) {
    return from(
      this.accountService.model.countDocuments({ accountingEntity, currency }),
    ).pipe(
      switchMap(count => {
        if (count !== 0) {
          return throwError(
            new BadRequestException({
              AccountsAlreadyExistsFor: accountingEntity,
            }),
          );
        }
        return fromStream(
          fs
            .createReadStream(path.resolve('data', chartType))
            .pipe(csv.parse({ headers: true })),
        );
      }),
      mergeMap(account => {
        return from(
          this.accountService.create({
            ...(account as Account),
            tenantId,
            accountingEntity,
            currency,
          } as Account),
        );
      }),
      toArray(),
      switchMap(array => from(array)),
      filter(account => account?.parentAccountNumber?.length !== 0),
      mergeMap(account => {
        return from(
          this.accountService.findOne({
            accountingEntity,
            currency,
            accountNumber: account.parentAccountNumber,
          }),
        ).pipe(
          switchMap(ac => {
            return from(
              this.accountService.updateOne(
                { uuid: account.uuid },
                { $set: { parentAccount: ac.uuid } },
              ),
            );
          }),
        );
      }),
    );
  }
}

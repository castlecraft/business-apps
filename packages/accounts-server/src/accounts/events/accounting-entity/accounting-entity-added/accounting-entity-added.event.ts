import { TenantUser } from '@castlecraft/auth';
import { IEvent } from '@nestjs/cqrs';
import { AccountingEntity } from '../../../entities/accounting-entity/accounting-entity.interface';

export class AccountingEntityAddedEvent implements IEvent {
  constructor(
    public readonly accountingEntity: AccountingEntity,
    public readonly tenantUser: TenantUser,
    public readonly chartType?: string,
  ) {}
}

import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { AccountingEntityService } from '../../../entities/accounting-entity/accounting-entity.service';
import { AccountingEntityUpdatedEvent } from './accounting-entity-updated.event';

@EventsHandler(AccountingEntityUpdatedEvent)
export class AccountingEntityUpdatedHandler
  implements IEventHandler<AccountingEntityUpdatedEvent>
{
  constructor(
    private readonly accountingEntityService: AccountingEntityService,
  ) {}

  async handle(event: AccountingEntityUpdatedEvent) {
    const { updatePayload } = event;
    await this.accountingEntityService.updateOne(
      { uuid: updatePayload.uuid },
      { $set: updatePayload },
    );
  }
}

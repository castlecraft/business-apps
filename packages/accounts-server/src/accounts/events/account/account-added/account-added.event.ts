import { IEvent } from '@nestjs/cqrs';
import { Account } from '../../../entities/account/account.interface';

export class AccountAddedEvent implements IEvent {
  constructor(public account: Account) {}
}

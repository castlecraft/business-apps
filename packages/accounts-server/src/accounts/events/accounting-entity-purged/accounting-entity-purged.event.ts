import { IEvent } from '@nestjs/cqrs';
import { AccountingEntity } from '../../entities/accounting-entity/accounting-entity.interface';

export class AccountingEntityPurgedEvent implements IEvent {
  constructor(public readonly accountingEntity: AccountingEntity) {}
}

import { Module } from '@nestjs/common';
import { CommonModule } from '../common/common.module';
import { AccountingEntityAggregateService } from './aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { AccountsAggregateService } from './aggregates/accounts-aggregate/accounts-aggregate.service';
import { CurrencyAggregateService } from './aggregates/currency-aggregate/currency-aggregate.service';
import { JournalEntryAggregateService } from './aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { AccountsCommandManager } from './commands';
import { AccountController } from './controllers/account/account.controller';
import { AccountingEntityController } from './controllers/accounting-entity/accounting-entity.controller';
import { CurrencyController } from './controllers/currency/currency.controller';
import { JournalEntryController } from './controllers/journal-entry/journal-entry.controller';
import { AccountEntities, AccountEntityServices } from './entities';
import { AccountEventManager } from './events';
import { JournalEntryPoliciesService } from './policies/journal-entry-policies/journal-entry-policies/journal-entry-policies.service';
import { AccountQueryManager } from './queries';
import { TenancyAggregateService } from './aggregates/tenancy-aggregate/tenancy-aggregate.service';
import { LedgerEntryController } from './controllers/ledger-entry/ledger-entry.controller';
import { LedgerEntryAggregateService } from './aggregates/ledger-entry-aggregate/ledger-entry-aggregate.service';
import { TrialBalanceAggregateService } from './aggregates/trial-balance-aggregate/trial-balance-aggregate.service';
import { TrialBalanceController } from './controllers/trial-balance/trial-balance.controller';

@Module({
  imports: [CommonModule],
  providers: [
    ...AccountEntities,
    ...AccountEntityServices,
    ...AccountsCommandManager,
    ...AccountEventManager,
    ...AccountQueryManager,
    AccountsAggregateService,
    JournalEntryAggregateService,
    CurrencyAggregateService,
    AccountingEntityAggregateService,
    JournalEntryPoliciesService,
    TenancyAggregateService,
    LedgerEntryAggregateService,
    TrialBalanceAggregateService,
  ],
  controllers: [
    AccountController,
    CurrencyController,
    AccountingEntityController,
    JournalEntryController,
    LedgerEntryController,
    TrialBalanceController,
  ],
  exports: [...AccountEntityServices],
})
export class AccountsModule {}

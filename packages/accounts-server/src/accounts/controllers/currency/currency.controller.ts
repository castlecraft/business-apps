import {
  Controller,
  Param,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { RetrieveCurrencyListQuery } from '../../queries/currency/list-currency/retrieve-currency-list.query';
import { GetCurrencyQuery } from '../../queries/currency/get-currency/get-currency.query';
import { ListCurrencyDto } from './list-currency.dto';

@Controller('currency')
export class CurrencyController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getCurrencyList(@Query() query: ListCurrencyDto) {
    return await this.queryBus.execute(new RetrieveCurrencyListQuery(query));
  }

  @Get('v1/get/:code')
  async getCurrency(@Param('code') code) {
    return await this.queryBus.execute(new GetCurrencyQuery(code));
  }
}

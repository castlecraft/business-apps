import { IsNumberString, IsOptional, IsString, IsUUID } from 'class-validator';

export class ListTrialBalanceDto {
  @IsUUID()
  @IsOptional()
  accountingEntity: string;

  @IsString()
  @IsOptional()
  currency: string;

  @IsString()
  @IsOptional()
  parentAccountNumber: string;

  @IsString()
  @IsOptional()
  accountName: string;

  @IsString()
  @IsOptional()
  accountNumber: string;

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

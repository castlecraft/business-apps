import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { ListTrialBalanceQuery } from '../../queries/trial-balance/list-trial-balance/list-trial-balance.query';
import { ListTrialBalanceDto } from './trial-balance.dto';

@Controller('trial_balance')
export class TrialBalanceController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getAccountList(@Query() query: ListTrialBalanceDto) {
    return await this.queryBus.execute(new ListTrialBalanceQuery(query));
  }
}

import { IsNumberString, IsOptional, IsString, IsUUID } from 'class-validator';

export class ListLedgerEntryDto {
  @IsString()
  @IsOptional()
  uuid: string;

  @IsUUID()
  @IsOptional()
  accountingEntity: string;

  @IsString()
  @IsOptional()
  transactionDate: any;

  @IsString()
  @IsOptional()
  fromDate: string;

  @IsString()
  @IsOptional()
  toDate: string;

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

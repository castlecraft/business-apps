import {
  Controller,
  Get,
  Query,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { QueryBus } from '@nestjs/cqrs';
import { ListLedgerEntryQuery } from '../../queries/ledger-entry/list-ledger-entry/list-ledger-entry.query';
import { ListLedgerEntryDto } from './list-ledger-entry.dto';

@Controller('ledger_entry')
export class LedgerEntryController {
  constructor(private readonly queryBus: QueryBus) {}

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getLedgerEntryList(@Query() query: ListLedgerEntryDto) {
    return await this.queryBus.execute(new ListLedgerEntryQuery(query));
  }
}

import { QueryBus } from '@nestjs/cqrs';
import { Test, TestingModule } from '@nestjs/testing';
import { LedgerEntryController } from './ledger-entry.controller';

describe('LedgerEntryController', () => {
  let controller: LedgerEntryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LedgerEntryController],
      providers: [{ provide: QueryBus, useValue: {} }],
    }).compile();

    controller = module.get<LedgerEntryController>(LedgerEntryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

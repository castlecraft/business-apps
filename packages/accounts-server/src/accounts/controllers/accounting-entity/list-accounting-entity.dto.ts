import { IsNumberString, IsOptional, IsString, IsUUID } from 'class-validator';

export class ListAccountingEntityDto {
  @IsUUID()
  @IsOptional()
  uuid: string;

  @IsString()
  @IsOptional()
  name: string;

  @IsString()
  @IsOptional()
  taxId: string;

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

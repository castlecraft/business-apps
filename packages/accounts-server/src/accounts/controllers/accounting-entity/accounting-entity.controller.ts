import {
  Roles,
  TenantGuard,
  TenantRoleGuard,
  TenantUser,
  TokenGuard,
  UserGuard,
} from '@castlecraft/auth';
import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { EventPattern, Payload } from '@nestjs/microservices';
import {
  ACCOUNTANT,
  ADMINISTRATOR,
  SERVICE,
  SHARE,
  TenantRemovedEvent,
} from '../../../constants/app-strings';
import { EventPayload } from '../../../constants/event-payload.interface';
import { AddAccountingEntityCommand } from '../../commands/accounting-entity/add-accounting-entity/add-accounting-entity.command';
import { RemoveAccountingEntityCommand } from '../../commands/accounting-entity/remove-accounting-entity/remove-accounting-entity.command';
import { UpdateAccountingEntityCommand } from '../../commands/accounting-entity/update-accounting-entity/update-accounting-entity.command';
import { PurgeAccountingEntityCommand } from '../../commands/purge-accounting-entity/purge-accounting-entity.command';
import { PurgeTenantEntitiesCommand } from '../../commands/purge-tenant-entities/purge-tenant-entities.command';
import { AccountingEntityDto } from '../../entities/accounting-entity/accounting-entity.dto';
import { RetrieveAccountingEntityListQuery } from '../../queries/accounting-entity/retrieve-accounting-entity-list/retrieve-accounting-entity-list.query'; // eslint-disable-line
import { RetrieveAccountingEntityQuery } from '../../queries/accounting-entity/retrieve-accounting-entity/retrieve-accounting-entity.query';
import { FetchAccountingEntityDto } from './fetch-accounting-entity.dto';
import { ListAccountingEntityDto } from './list-accounting-entity.dto';

@Controller('accounting_entity')
export class AccountingEntityController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  @Roles(ADMINISTRATOR, ACCOUNTANT)
  @UseGuards(
    TokenGuard,
    UserGuard,
    TenantGuard,
    new TenantRoleGuard(new Reflector()),
  )
  async createAccountingEntity(
    @Body() accountingEntityPayload: AccountingEntityDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.commandBus.execute(
      new AddAccountingEntityCommand(accountingEntityPayload, req.tenantUser),
    );
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  @Roles(ADMINISTRATOR, ACCOUNTANT)
  @UseGuards(
    TokenGuard,
    UserGuard,
    TenantGuard,
    new TenantRoleGuard(new Reflector()),
  )
  async getAccountingEntityList(
    @Query() query: ListAccountingEntityDto,
    @Req() req: unknown & { tenantUser: TenantUser },
  ) {
    return await this.queryBus.execute(
      new RetrieveAccountingEntityListQuery(query, req.tenantUser),
    );
  }

  @Get('v1/fetch/:uuid')
  @UsePipes(new ValidationPipe())
  async getAccountingEntity(@Param() params: FetchAccountingEntityDto) {
    return await this.queryBus.execute(
      new RetrieveAccountingEntityQuery(params.uuid),
    );
  }

  @Post('v1/remove/:uuid')
  async removeAccountingEntity(@Param('uuid') uuid) {
    return await this.commandBus.execute(
      new RemoveAccountingEntityCommand(uuid),
    );
  }

  @Post('v1/purge/:uuid')
  async purgeAccountingEntity(@Param('uuid') uuid) {
    return await this.commandBus.execute(
      new PurgeAccountingEntityCommand(uuid),
    );
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateAccountingEntity(
    @Body() updatePayload: AccountingEntityDto,
    @Req() req,
  ) {
    return await this.commandBus.execute(
      new UpdateAccountingEntityCommand(updatePayload),
    );
  }

  @EventPattern(`${SHARE}/${SERVICE}/${TenantRemovedEvent}`)
  async handleTenantRemoved(@Payload() payload: EventPayload) {
    await this.commandBus.execute(
      new PurgeTenantEntitiesCommand(payload?.eventData?.tenant?.uuid),
    );
  }
}

import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Query,
  Req,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { AddAccountCommand } from '../../commands/account/add-account/add-account.command';
import { RemoveAccountCommand } from '../../commands/account/remove-account/remove-account.command';
import { UpdateAccountCommand } from '../../commands/account/update-account/update-account.command';
import { AccountDto } from '../../entities/account/account-dto';
import { RetrieveAccountListQuery } from '../../queries/account/list-account/retrieve-account-list.query';
import { LoadChildrenQuery } from '../../queries/account/load-children/load-children.query';
import { LoadRootQuery } from '../../queries/account/load-root/load-root.query';
import { ListAccountDto } from './list-account.dto';

@Controller('account')
export class AccountController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Post('v1/create')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async createAccount(@Body() accountPayload: AccountDto, @Req() req) {
    return await this.commandBus.execute(new AddAccountCommand(accountPayload));
  }

  @Get('v1/list')
  @UsePipes(new ValidationPipe())
  async getAccountList(@Query() query: ListAccountDto) {
    return await this.queryBus.execute(new RetrieveAccountListQuery(query));
  }

  @Post('v1/remove/:uuid')
  removeAccount(@Param('uuid') uuid) {
    return this.commandBus.execute(new RemoveAccountCommand(uuid));
  }

  @Post('v1/update')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async updateAccount(@Body() updatePayload: AccountDto, @Req() req) {
    return await this.commandBus.execute(
      new UpdateAccountCommand(updatePayload),
    );
  }

  @Get('v1/load_root/:uuid')
  @UsePipes(new ValidationPipe({ whitelist: true }))
  async loadRoot(@Param('uuid') uuid: string) {
    return await this.queryBus.execute(new LoadRootQuery(uuid));
  }

  @Get('v1/load_children/:uuid')
  async loadChildren(@Param('uuid') uuid: string) {
    return await this.queryBus.execute(new LoadChildrenQuery(uuid));
  }
}

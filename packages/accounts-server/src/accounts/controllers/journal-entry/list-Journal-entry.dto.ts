import { IsNumberString, IsOptional, IsString } from 'class-validator';

export class ListJournalEntryDto {
  @IsString()
  @IsOptional()
  uuid: string;

  @IsString()
  @IsOptional()
  tenantId: string;

  @IsString()
  @IsOptional()
  date: any;

  @IsString()
  @IsOptional()
  fromDate: string;

  @IsString()
  @IsOptional()
  toDate: string;

  @IsNumberString()
  @IsOptional()
  offset: string;

  @IsNumberString()
  @IsOptional()
  limit: string;

  @IsString()
  @IsOptional()
  sort: string;
}

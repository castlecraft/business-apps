import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { AccountingEntityAggregateService } from '../../../aggregates/accounting-entity-aggregate/accounting-entity-aggregate.service';
import { RetrieveAccountingEntityListQuery } from './retrieve-accounting-entity-list.query';

@QueryHandler(RetrieveAccountingEntityListQuery)
export class RetrieveAccountingEntityListHandler
  implements IQueryHandler<RetrieveAccountingEntityListQuery>
{
  constructor(private readonly manager: AccountingEntityAggregateService) {}
  async execute(query: RetrieveAccountingEntityListQuery) {
    return await this.manager.getAccountingEntityList(
      query.filter,
      query.tenantUser,
    );
  }
}

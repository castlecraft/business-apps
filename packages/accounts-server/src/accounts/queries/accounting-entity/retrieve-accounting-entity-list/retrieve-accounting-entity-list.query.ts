import { IQuery } from '@nestjs/cqrs';
import { TenantUser } from '@castlecraft/auth';
import { ListAccountingEntityDto } from '../../../controllers/accounting-entity/list-accounting-entity.dto';

export class RetrieveAccountingEntityListQuery implements IQuery {
  constructor(
    public readonly filter: ListAccountingEntityDto,
    public readonly tenantUser: TenantUser,
  ) {}
}

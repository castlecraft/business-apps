import { IQuery } from '@nestjs/cqrs';

export class RetrieveAccountingEntityQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}

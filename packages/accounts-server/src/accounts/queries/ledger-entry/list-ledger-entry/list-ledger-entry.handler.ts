import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { LedgerEntryAggregateService } from '../../../aggregates/ledger-entry-aggregate/ledger-entry-aggregate.service';
import { ListLedgerEntryQuery } from './list-ledger-entry.query';

@QueryHandler(ListLedgerEntryQuery)
export class ListLedgerEntryHandler
  implements IQueryHandler<ListLedgerEntryQuery>
{
  constructor(private readonly manager: LedgerEntryAggregateService) {}

  async execute(query: ListLedgerEntryQuery) {
    return await this.manager.listLedgerEntry(query.filter);
  }
}

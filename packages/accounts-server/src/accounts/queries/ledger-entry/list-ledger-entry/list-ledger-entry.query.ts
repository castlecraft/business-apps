import { IQuery } from '@nestjs/cqrs';
import { ListLedgerEntryDto } from '../../../controllers/ledger-entry/list-ledger-entry.dto';

export class ListLedgerEntryQuery implements IQuery {
  constructor(public readonly filter: ListLedgerEntryDto) {}
}

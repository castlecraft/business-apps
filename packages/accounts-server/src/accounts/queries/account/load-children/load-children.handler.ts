import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { AccountsAggregateService } from '../../../aggregates/accounts-aggregate/accounts-aggregate.service';
import { LoadChildrenQuery } from './load-children.query';

@QueryHandler(LoadChildrenQuery)
export class LoadChildrenHandler implements IQueryHandler<LoadChildrenQuery> {
  constructor(private readonly manager: AccountsAggregateService) {}

  async execute(query: LoadChildrenQuery) {
    const { uuid } = query;
    return this.manager.loadChildren(uuid);
  }
}

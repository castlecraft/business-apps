import { QueryHandler, IQueryHandler } from '@nestjs/cqrs';
import { JournalEntryAggregateService } from '../../../aggregates/journal-entry-aggregate/journal-entry-aggregate.service';
import { GetJournalEntryQuery } from './get-journal-entry.query';

@QueryHandler(GetJournalEntryQuery)
export class GetJournalEntryHandler
  implements IQueryHandler<GetJournalEntryQuery>
{
  constructor(private readonly entity: JournalEntryAggregateService) {}

  async execute(query: GetJournalEntryQuery) {
    return await this.entity.getJournalEntry(query.uuid);
  }
}

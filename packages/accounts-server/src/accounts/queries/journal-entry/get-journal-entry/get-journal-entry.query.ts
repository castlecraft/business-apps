import { IQuery } from '@nestjs/cqrs';

export class GetJournalEntryQuery implements IQuery {
  constructor(public readonly uuid: string) {}
}

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { CurrencyAggregateService } from '../../../aggregates/currency-aggregate/currency-aggregate.service';
import { GetCurrencyQuery } from './get-currency.query';

@QueryHandler(GetCurrencyQuery)
export class GetCurrencyHandler implements IQueryHandler<GetCurrencyQuery> {
  constructor(private readonly manager: CurrencyAggregateService) {}
  async execute(query: GetCurrencyQuery) {
    return await this.manager.retrieveCurrency(query.code);
  }
}

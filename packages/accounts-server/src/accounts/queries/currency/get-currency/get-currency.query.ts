import { IQuery } from '@nestjs/cqrs';

export class GetCurrencyQuery implements IQuery {
  constructor(public readonly code: string) {}
}

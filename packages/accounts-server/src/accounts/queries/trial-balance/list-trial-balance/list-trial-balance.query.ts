import { IQuery } from '@nestjs/cqrs';
import { ListTrialBalanceDto } from '../../../controllers/trial-balance/trial-balance.dto';

export class ListTrialBalanceQuery implements IQuery {
  constructor(public readonly filter: ListTrialBalanceDto) {}
}

import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { TrialBalanceAggregateService } from '../../../aggregates/trial-balance-aggregate/trial-balance-aggregate.service';
import { ListTrialBalanceQuery } from './list-trial-balance.query';

@QueryHandler(ListTrialBalanceQuery)
export class ListTrialBalanceHandler
  implements IQueryHandler<ListTrialBalanceQuery>
{
  constructor(private readonly manager: TrialBalanceAggregateService) {}
  async execute(query: ListTrialBalanceQuery) {
    return await this.manager.list(query.filter);
  }
}

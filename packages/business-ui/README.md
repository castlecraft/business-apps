## App Config

Change the `environment.prod.ts` and `environment.ts` as follows:

```TypeScript
export const environment = {
  production: false,

  // Configure Variables
  appURL: 'http://cap.localhost:4200',
  authorizationURL: '<frappe_auth_server>/api/method/frappe.integrations.oauth2.authorize',
  authServerURL: '<frappe_auth_server>',
  callbackProtocol: 'frappe',
  callbackUrl: 'http://cap.localhost:4200/callback',
  clientId: 'test_client',
  profileURL: '<frappe_auth_server>/api/method/frappe.integrations.oauth2.openid_profile',
  revocationURL: '<frappe_auth_server>/api/method/frappe.integrations.oauth2.revoke_token',
  scope: 'openid all',
  tokenURL: '<frappe_auth_server>/api/method/frappe.integrations.oauth2.get_token',
  isAuthRequiredToRevoke: false,
};
```
Note:

- `appURL` is url used to access ReST API. Use `appURL` in your app code later. It is not used in case of web ui.
- Refer `environment.*.ts` for configuration options.

## For OAuth 2 Client settings


- Set redirect uri to `GET` endpoint which should redirect to callback protocol for mobile or desktop app.
- Example redirect uri `https://example.com/device/callback` should further redirect to `<callbackProtocol>://callback?code=420&state=710` if direct registration of client is not possible for given protocol. Native app then processes the code and exchanges it for the token.
- Set redirect uri to `<baseURL>/callback` for web app, where `baseURL` is location from where app is served.
- Set scope to `profile openid email roles phone`, or change constant in `environment`.

This app uses authorization code grant + pkce for seamless refresh of session.

## For android

Change the `custom_url_scheme` from `strings.xml` to the callback protocol

```xml
<string name="custom_url_scheme">castlecraft</string>
```

Add `intent-filter` in `AndroidManifest.xml`

```xml
  ...
  <intent-filter>
    <data android:scheme="@string/custom_url_scheme"/>
    <action android:name="android.intent.action.VIEW"/>
    <category android:name="android.intent.category.DEFAULT"/>
    <category android:name="android.intent.category.BROWSABLE"/>
  </intent-filter>
  ...
```

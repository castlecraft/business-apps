// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  // Configure Variables
  accountsServerUrl: 'http://accounts-server.localhost:8000',
  adminServerUrl: 'http://admin-server.localhost:7000',
  authorizationURL: 'http://accounts.localhost:4210/oauth2/confirmation',
  authServerURL: 'http://accounts.localhost:4210',
  callbackProtocol: 'blocks',
  callbackUrl: 'http://apps.localhost:4200/callback',
  clientId: 'client_id',
  profileURL: 'http://accounts.localhost:4210/oauth2/profile',
  revocationURL:
    'http://myaccount.localhost:4420/connected_device/revoke_token',
  scope: 'profile openid email roles phone',
  tokenURL: 'http://accounts.localhost:4210/oauth2/token',
  isAuthRequiredToRevoke: true,
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

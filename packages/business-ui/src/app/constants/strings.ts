export const DARK_MODE = 'dark_mode';
export const ON = 'on';
export const OFF = 'off';
export const ADMINISTRATOR = 'administrator';
export const UNDO_DURATION = 10000;
export const UNDO = 'Undo';
export const CLOSE = 'Close';
export const X_TENANT_ID = 'X-Tenant-ID';
export const DARK_MODE_CLASS_NAME = 'darkMode';
export const ENTITY_TYPE = {
  SoleProprietorship: 'Sole Proprietorship',
  Partnership: 'Partnership',
  CCorporation: 'C Corporation',
  SCorporation: 'S Corporation',
  LimitedLiabilityCompany: 'Limited Liability Company',
  LimitedLiabilityPartnership: 'Limited Liability Partnership',
  PrivateLimited: 'Private Limited',
  PublicLimited: 'Public Limited',
  Other: 'Other',
};

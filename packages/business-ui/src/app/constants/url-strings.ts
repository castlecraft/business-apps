// Accounting
export const LOAD_ROOT = '/api/account/v1/load_root';
export const LOAD_CHILDREN = '/api/account/v1/load_children';
export const LIST_ACCOUNTS = '/api/account/v1/list';
export const LIST_CURRENCIES = '/api/currency/v1/list';
export const LIST_ACCOUNTING_ENTITIES = '/api/accounting_entity/v1/list';
export const GET_ACCOUNTING_ENTITY = '/api/accounting_entity/v1/fetch';
export const CREATE_ACCOUNTING_ENTITY = '/api/accounting_entity/v1/create';
export const PURGE_ACCOUNTING_ENTITY = '/api/accounting_entity/v1/purge';
export const LIST_JOURNAL_ENTRIES = '/api/journal_entry/v1/list';
export const CREATE_JOURNAL_ENTRY = '/api/journal_entry/v1/create';
export const UPDATE_JOURNAL_ENTRY = '/api/journal_entry/v1/update';
export const REMOVE_JOURNAL_ENTRY = '/api/journal_entry/v1/remove';
export const LIST_LEDGER_ENTRY = '/api/ledger_entry/v1/list';
export const LIST_TRIAL_BALANCE = '/api/trial_balance/v1/list';

// Admin
export const TENANT_LIST_FOR_ADMIN = '/api/admin/tenant/v1/list';
export const TENANT_LIST_FOR_USER = '/api/user/tenant/v1/list';
export const CREATE_TENANT_FOR_ADMIN = '/api/admin/tenant/v1/add';
export const CREATE_TENANT_FOR_USER = '/api/user/tenant/v1/add';
export const LIST_TENANT_USERS_FOR_ADMIN = '/api/admin/tenant/user/v1/list';
export const LIST_TENANT_USERS_FOR_USER = '/api/user/tenant/user/v1/list';
export const LIST_TOKEN_USERS_FOR_USER = '/api/user/tenant/token_user/v1/list';
export const ADD_TENANT_USER_FOR_ADMIN = '/api/admin/tenant/user/v1/add';
export const ADD_TENANT_USER_FOR_USER = '/api/user/tenant/user/v1/add';
export const LIST_TENANT_ROLES_FOR_USER = '/api/user/tenant/role/v1/list';
export const REMOVE_TENANT_USER_FOR_ADMIN = '/api/admin/tenant/user/v1/remove';
export const REMOVE_TENANT_USER_FOR_USER = '/api/user/tenant/user/v1/remove';
export const UPDATE_TENANT_USER_FOR_ADMIN = '/api/admin/tenant/user/v1/update';
export const UPDATE_TENANT_USER_FOR_USER = '/api/user/tenant/user/v1/update';
export const DELETE_TENANT_FOR_ADMIN = '/api/admin/tenant/v1/remove';
export const DELETE_TENANT_FOR_USER = '/api/user/tenant/v1/remove';
export const GET_TENANT_USER_PROFILE_FOR_USER = '/api/user/tenant/v1/profile';

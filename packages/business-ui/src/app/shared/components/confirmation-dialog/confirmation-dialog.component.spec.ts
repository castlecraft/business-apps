import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { AddEntityComponent } from '../../../account/accounting-entity/add-entity/add-entity.component';
import { MaterialModule } from '../../../material.module';
import { ConfirmationDialogComponent } from './confirmation-dialog.component';

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ConfirmationDialogComponent],
        imports: [MaterialModule, NoopAnimationsModule],
        providers: [
          {
            provide: MatDialogRef,
            useValue: {
              close: (...args) => {},
            } as MatDialogRef<AddEntityComponent>,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ConfirmationDialogComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

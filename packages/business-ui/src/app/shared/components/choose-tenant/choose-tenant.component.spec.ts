import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { MaterialModule } from '../../../material.module';
import { AddEntityComponent } from '../../../account/accounting-entity/add-entity/add-entity.component';
import { StorageService } from '../../../auth/storage/storage.service';
import { ListingService } from '../data-list/listing.service';
import { ChooseTenantComponent } from './choose-tenant.component';

describe('ChooseTenantComponent', () => {
  let component: ChooseTenantComponent;
  let fixture: ComponentFixture<ChooseTenantComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ChooseTenantComponent],
        imports: [
          HttpClientTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: MatDialogRef,
            useValue: {
              close: (...args) => {},
            } as MatDialogRef<AddEntityComponent>,
          },
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
            } as ListingService,
          },
          {
            provide: StorageService,
            useValue: {
              getItem: (...args) => Promise.resolve(''),
            } as StorageService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(ChooseTenantComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

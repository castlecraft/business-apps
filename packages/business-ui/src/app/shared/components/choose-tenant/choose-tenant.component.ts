import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { AddEntityComponent } from '../../../account/accounting-entity/add-entity/add-entity.component';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
} from '../../../constants/strings';
import { TENANT_LIST_FOR_USER } from '../../../constants/url-strings';
import { ListingService } from '../data-list/listing.service';

@Component({
  selector: 'app-choose-tenant',
  templateUrl: './choose-tenant.component.html',
  styleUrls: ['./choose-tenant.component.scss'],
})
export class ChooseTenantComponent implements OnInit {
  tenant = new FormControl('', [Validators.required]);
  tenants: Observable<any>;

  constructor(
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private readonly dialogRef: MatDialogRef<AddEntityComponent>,
    private readonly list: ListingService,
  ) {}

  ngOnInit() {
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
    this.tenants = this.tenant.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(tenantName =>
        this.list.findModels(
          environment.adminServerUrl + TENANT_LIST_FOR_USER,
          { tenantName },
        ),
      ),
      map(res => res.docs),
    );
  }

  close() {
    this.dialogRef.close(false);
  }

  select() {
    this.dialogRef.close(this.tenant.value);
  }

  displayTenantName(tenant: { tenantName: string }) {
    return tenant.tenantName;
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Pipe, PipeTransform } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { JournalEntryInlineEditComponent } from './journal-entry-inline-edit.component';

@Pipe({ name: 'titleCaseFilter' })
class MockTitleCaseFilterPipe implements PipeTransform {
  transform(value: string) {
    return value;
  }
}

describe('InlineEditComponent', () => {
  let component: JournalEntryInlineEditComponent;
  let fixture: ComponentFixture<JournalEntryInlineEditComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [
          JournalEntryInlineEditComponent,
          MockTitleCaseFilterPipe,
        ],
        imports: [HttpClientTestingModule],
      }).compileComponents();

      fixture = TestBed.createComponent(JournalEntryInlineEditComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

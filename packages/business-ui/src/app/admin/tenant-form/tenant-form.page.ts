import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { filter, map, switchMap } from 'rxjs/operators';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { environment } from '../../../environments/environment';
import { StorageService } from '../../auth/storage/storage.service';
import { CreatedByActor } from '../../constants/enums';
import {
  CANNOT_DELETE,
  CONFIRM_DELETE_TENANT,
  DELETING,
  TENANT,
  TYPE_DELETE_TO_PROCEED,
  UPPERCASE_DELETE,
} from '../../constants/messages';
import {
  CLOSE,
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
  UNDO,
  UNDO_DURATION,
} from '../../constants/strings';
import {
  ADD_TENANT_USER_FOR_ADMIN,
  DELETE_TENANT_FOR_ADMIN,
  LIST_TENANT_USERS_FOR_ADMIN,
  REMOVE_TENANT_USER_FOR_ADMIN,
  TENANT_LIST_FOR_ADMIN,
  UPDATE_TENANT_USER_FOR_ADMIN,
} from '../../constants/url-strings';
import { DataListComponent } from '../../shared/components/data-list/data-list.component';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { AddTenantUserComponent } from '../shared-components/add-tenant-user/add-tenant-user.component';
import { EditTenantUserComponent } from '../shared-components/edit-tenant-user/edit-tenant-user.component';

@Component({
  selector: 'app-tenant-form',
  templateUrl: './tenant-form.page.html',
  styleUrls: ['./tenant-form.page.scss'],
})
export class TenantFormPage implements OnInit {
  uuid = '';
  tenantName = '';
  createdById = new FormControl();
  createdByActor = new FormControl();
  @ViewChild(DataListComponent)
  listComponent: DataListComponent;
  flagDeleteUser = false;
  fields = ['email', 'phone', 'roles', 'user'];
  endpoint = environment.adminServerUrl + LIST_TENANT_USERS_FOR_ADMIN;
  filters = {};

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly list: ListingService,
    private readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar,
    private readonly store: StorageService,
    private readonly overlay: OverlayContainer,
  ) {}

  ngOnInit() {
    this.uuid = this.route.snapshot.params.uuid;
    this.filters = { tenant: this.uuid };
    this.createdByActor.disable();
    this.createdById.disable();
    this.list
      .getRequest(environment.adminServerUrl + TENANT_LIST_FOR_ADMIN, {
        uuid: this.uuid,
        offset: 0,
        limit: 1,
      })
      .pipe(
        map(data => data.docs),
        filter(docs => docs.length),
        map(docs => docs[0]),
      )
      .subscribe(res => {
        this.tenantName = res.tenantName;
        this.createdByActor.setValue(res.createdByActor);
        this.loadActor(res.createdById, res.createdByActor);
      });

    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
  }

  loadActor(actorUuid: string, createdByActor: CreatedByActor) {
    if (createdByActor === CreatedByActor.Client) {
      this.createdById.setValue(actorUuid);
    }
    if (createdByActor === CreatedByActor.User) {
      this.list
        .getRequest(environment.adminServerUrl + LIST_TENANT_USERS_FOR_ADMIN, {
          uuid: actorUuid,
        })
        .pipe(map(res => (res?.docs?.length ? res?.docs[0] : {})))
        .subscribe(res =>
          this.createdById.setValue(`${res.email} (${res.uuid})`),
        );
    }
  }

  addTenantUser() {
    const dialogRef = this.dialog.open(AddTenantUserComponent, {
      data: this.uuid,
      width: '100vh',
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(tenantUser => tenantUser && this.uuid !== ''),
        switchMap(tenantUser => {
          return this.list.postRequest(
            environment.adminServerUrl + ADD_TENANT_USER_FOR_ADMIN,
            {
              user: tenantUser.uuid,
              tenant: this.uuid,
            },
          );
        }),
      )
      .subscribe({
        next: res => {
          this.listComponent.dataSource.loadItems(this.filters);
        },
        error: err => {},
      });
  }

  editUser($event) {
    const dialogRef = this.dialog.open(EditTenantUserComponent, {
      data: $event,
    });

    dialogRef.afterClosed().subscribe(user => {
      if (user && user.delete) {
        this.deleteUser(user);
      }

      if (user && user.user && !user.delete) {
        this.updateRoles(user);
      }
    });
  }

  deleteUser(user) {
    this.flagDeleteUser = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar
      .afterDismissed()
      .pipe(
        filter(() => this.flagDeleteUser),
        switchMap(() => {
          return this.list.postRequest(
            environment.adminServerUrl + REMOVE_TENANT_USER_FOR_ADMIN,
            {
              user: user.user,
              tenant: this.uuid,
            },
          );
        }),
      )
      .subscribe({
        next: dismissed => {
          this.listComponent.dataSource.loadItems(this.filters);
        },
        error: error => {},
      });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUser = false;
      },
      error: error => {},
    });
  }

  updateRoles(user) {
    this.list
      .postRequest(environment.adminServerUrl + UPDATE_TENANT_USER_FOR_ADMIN, {
        user: user.user,
        tenant: this.uuid,
        roles: user.roles,
      })
      .subscribe({
        next: dismissed => {
          this.listComponent.dataSource.loadItems(this.filters);
        },
        error: error => {},
      });
  }

  confirmPurge() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.confirmationInput = UPPERCASE_DELETE;
    dialogRef.componentInstance.message = TYPE_DELETE_TO_PROCEED;
    dialogRef.componentInstance.title = CONFIRM_DELETE_TENANT;
    dialogRef.afterClosed().subscribe(isConfirm => {
      if (isConfirm) {
        this.deleteTenant();
      }
    });
  }

  deleteTenant() {
    this.list
      .postRequest(
        environment.adminServerUrl + DELETE_TENANT_FOR_ADMIN + '/' + this.uuid,
      )
      .subscribe({
        next: success => {
          this.router.navigate(['/tenants']);
        },
        error: error => {
          this.snackBar.open(`${CANNOT_DELETE} ${TENANT}`, CLOSE, {
            duration: UNDO_DURATION,
          });
        },
      });
  }
}

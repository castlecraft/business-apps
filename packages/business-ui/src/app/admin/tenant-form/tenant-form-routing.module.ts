import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TenantFormPage } from './tenant-form.page';

const routes: Routes = [
  {
    path: '',
    component: TenantFormPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TenantFormPageRoutingModule {}

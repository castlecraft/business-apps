import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';
import { TenantSharedComponentsModule } from '../shared-components/tenant-shared-components.module';
import { TenantFormPageRoutingModule } from '../tenant-form/tenant-form-routing.module';
import { TeamFormPageRoutingModule } from './team-form-routing.module';
import { TeamFormPage } from './team-form.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TeamFormPageRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    TenantFormPageRoutingModule,
    SharedModule,
    TenantSharedComponentsModule,
  ],
  declarations: [TeamFormPage],
})
export class TeamFormPageModule {}

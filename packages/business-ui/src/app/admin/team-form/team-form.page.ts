import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { SET_ITEM, StorageService } from '../../auth/storage/storage.service';
import { SELECTED_TENANT } from '../../auth/token/constants';
import {
  CANNOT_DELETE,
  CONFIRM_DELETE_TENANT,
  DELETING,
  TENANT,
  TYPE_DELETE_TO_PROCEED,
  UPPERCASE_DELETE,
} from '../../constants/messages';
import {
  CLOSE,
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
  UNDO,
  UNDO_DURATION,
  X_TENANT_ID,
} from '../../constants/strings';
import {
  ADD_TENANT_USER_FOR_USER,
  DELETE_TENANT_FOR_USER,
  LIST_TENANT_USERS_FOR_USER,
  REMOVE_TENANT_USER_FOR_USER,
  TENANT_LIST_FOR_USER,
  UPDATE_TENANT_USER_FOR_USER,
} from '../../constants/url-strings';
import { ConfirmationDialogComponent } from '../../shared/components/confirmation-dialog/confirmation-dialog.component';
import { DataListComponent } from '../../shared/components/data-list/data-list.component';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { AddTenantUserComponent } from '../shared-components/add-tenant-user/add-tenant-user.component';
import { EditTenantUserComponent } from '../shared-components/edit-tenant-user/edit-tenant-user.component';

@Component({
  selector: 'app-team-form',
  templateUrl: './team-form.page.html',
  styleUrls: ['./team-form.page.scss'],
})
export class TeamFormPage implements OnInit, OnDestroy {
  uuid = this.route.snapshot.params.uuid;
  tenantName = '';
  @ViewChild(DataListComponent)
  listComponent: DataListComponent;
  flagDeleteUser = false;
  fields = ['email', 'phone', 'roles', 'user'];
  endpoint = environment.adminServerUrl + LIST_TENANT_USERS_FOR_USER;
  filters = { tenant: this.uuid };
  headers = { [X_TENANT_ID]: this.uuid };
  teamChangeWatch: Subscription;

  constructor(
    private readonly route: ActivatedRoute,
    private readonly router: Router,
    private readonly list: ListingService,
    private readonly dialog: MatDialog,
    private readonly snackBar: MatSnackBar,
    private readonly store: StorageService,
    private readonly overlay: OverlayContainer,
  ) {}

  ngOnInit() {
    this.uuid = this.route.snapshot.params.uuid;
    this.filters = { tenant: this.uuid };
    this.headers = { [X_TENANT_ID]: this.uuid };
    this.teamChangeWatch = this.watchTeamChange();
    this.loadTenant();
    this.setupDarkMode();
  }

  ngOnDestroy() {
    this.teamChangeWatch?.unsubscribe();
  }

  addTenantUser() {
    const dialogRef = this.dialog.open(AddTenantUserComponent, {
      data: this.uuid,
      width: '100vh',
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter(tenantUser => tenantUser && this.uuid !== ''),
        switchMap(tenantUser => {
          return this.list.postRequest(
            environment.adminServerUrl + ADD_TENANT_USER_FOR_USER,
            {
              user: tenantUser.uuid,
              tenant: this.uuid,
            },
            this.headers,
          );
        }),
      )
      .subscribe({
        next: res => {
          this.reloadData();
        },
        error: err => {},
      });
  }

  editUser($event) {
    const dialogRef = this.dialog.open(EditTenantUserComponent, {
      data: $event,
    });

    dialogRef.afterClosed().subscribe(user => {
      if (user && user.delete) {
        this.deleteUser(user);
      }

      if (user && user.user && !user.delete) {
        this.updateRoles(user);
      }
    });
  }

  deleteUser(user) {
    this.flagDeleteUser = true;
    const snackBar = this.snackBar.open(DELETING, UNDO, {
      duration: UNDO_DURATION,
    });

    snackBar
      .afterDismissed()
      .pipe(
        filter(() => this.flagDeleteUser),
        switchMap(() => {
          return this.list.postRequest(
            environment.adminServerUrl + REMOVE_TENANT_USER_FOR_USER,
            {
              user: user.user,
              tenant: this.uuid,
            },
            this.headers,
          );
        }),
      )
      .subscribe({
        next: dismissed => {
          this.reloadData();
        },
        error: error => {},
      });

    snackBar.onAction().subscribe({
      next: success => {
        this.flagDeleteUser = false;
      },
      error: error => {},
    });
  }

  updateRoles(user) {
    this.list
      .postRequest(
        environment.adminServerUrl + UPDATE_TENANT_USER_FOR_USER,
        {
          user: user.user,
          tenant: this.uuid,
          roles: user.roles,
        },
        this.headers,
      )
      .subscribe({
        next: dismissed => {
          this.reloadData();
        },
        error: error => {},
      });
  }

  confirmPurge() {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent);
    dialogRef.componentInstance.confirmationInput = UPPERCASE_DELETE;
    dialogRef.componentInstance.message = TYPE_DELETE_TO_PROCEED;
    dialogRef.componentInstance.title = CONFIRM_DELETE_TENANT;
    dialogRef.afterClosed().subscribe(isConfirm => {
      if (isConfirm) {
        this.deleteTenant();
      }
    });
  }

  deleteTenant() {
    this.list
      .postRequest(
        environment.adminServerUrl + DELETE_TENANT_FOR_USER + '/' + this.uuid,
        undefined,
        this.headers,
      )
      .subscribe({
        next: success => {
          this.router.navigate(['/tenants']);
        },
        error: error => {
          this.snackBar.open(`${CANNOT_DELETE} ${TENANT}`, CLOSE, {
            duration: UNDO_DURATION,
          });
        },
      });
  }

  reloadData() {
    this.listComponent.dataSource.headers = this.headers;
    this.listComponent.dataSource.loadItems(
      this.filters,
      undefined,
      undefined,
      undefined,
    );
  }

  loadTenant() {
    this.list
      .getRequest(
        environment.adminServerUrl + TENANT_LIST_FOR_USER,
        {
          uuid: this.uuid,
          offset: 0,
          limit: 1,
        },
        this.headers,
      )
      .pipe(
        map(data => data.docs),
        filter(docs => docs.length),
        map(docs => docs[0]),
      )
      .subscribe(res => {
        this.tenantName = res.tenantName;
        this.uuid = res.uuid;
      });
  }

  watchTeamChange() {
    return this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === SELECTED_TENANT) {
          const tenant = this.parseTenant(res?.value?.value);
          this.uuid = tenant.tenantId;
          this.tenantName = tenant.tenantName;
          this.filters = { tenant: this.uuid };
          this.headers = { [X_TENANT_ID]: this.uuid };
          this.router
            .navigate(['team', this.uuid])
            .then(() => this.reloadData());
        }
      },
      error: error => {},
    });
  }

  parseTenant(tenantStr: string) {
    try {
      return JSON.parse(tenantStr);
    } catch (error) {
      return {};
    }
  }

  setupDarkMode() {
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
  }
}

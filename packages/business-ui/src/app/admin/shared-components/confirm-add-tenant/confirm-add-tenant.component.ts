import { Component, OnInit } from '@angular/core';
import { OverlayContainer } from '@angular/cdk/overlay';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
} from '../../../constants/strings';
import { MatDialogRef } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-confirm-add-tenant',
  templateUrl: './confirm-add-tenant.component.html',
  styleUrls: ['./confirm-add-tenant.component.scss'],
})
export class ConfirmAddTenantComponent implements OnInit {
  tenantName = new FormControl('', [Validators.required]);

  constructor(
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private dialogRef: MatDialogRef<ConfirmAddTenantComponent>,
  ) {}

  ngOnInit() {
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });
  }

  close() {
    this.dialogRef.close(false);
  }

  add() {
    this.dialogRef.close({ tenantName: this.tenantName.value });
  }
}

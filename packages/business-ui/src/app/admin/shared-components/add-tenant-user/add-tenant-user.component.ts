import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { startWith, debounceTime, switchMap, map } from 'rxjs/operators';
import { LIST_TOKEN_USERS_FOR_USER } from '../../../constants/url-strings';
import { ListingService } from '../../../shared/components/data-list/listing.service';
import { environment } from '../../../../environments/environment';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  ON,
} from '../../../constants/strings';
import { ConfirmAddTenantComponent } from '../confirm-add-tenant/confirm-add-tenant.component';

@Component({
  selector: 'app-add-tenant-user',
  templateUrl: './add-tenant-user.component.html',
  styleUrls: ['./add-tenant-user.component.scss'],
})
export class AddTenantUserComponent implements OnInit {
  user = new FormControl();
  users: Observable<any>;

  constructor(
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService,
    private readonly dialogRef: MatDialogRef<ConfirmAddTenantComponent>,
    private readonly list: ListingService,
  ) {}

  ngOnInit() {
    this.store.getItem(DARK_MODE).then(darkMode => {
      if (darkMode === ON) {
        this.overlay.getContainerElement().classList.add(DARK_MODE_CLASS_NAME);
      } else {
        this.overlay
          .getContainerElement()
          .classList.remove(DARK_MODE_CLASS_NAME);
      }
    });

    this.users = this.user.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(email => {
        return this.list.findModels(
          environment.adminServerUrl + LIST_TOKEN_USERS_FOR_USER,
          {
            email,
          },
        );
      }),
      map(r => r.docs),
    );
  }

  close() {
    this.dialogRef.close(false);
  }

  add() {
    this.dialogRef.close(this.user.value);
  }

  displayUser(user?) {
    return user?.email;
  }
}

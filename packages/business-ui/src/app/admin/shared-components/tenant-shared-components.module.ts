import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';
import { AddTenantUserComponent } from '../shared-components/add-tenant-user/add-tenant-user.component';
import { ConfirmAddTenantComponent } from '../shared-components/confirm-add-tenant/confirm-add-tenant.component';
import { EditTenantUserComponent } from '../shared-components/edit-tenant-user/edit-tenant-user.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    MaterialModule,
  ],
  declarations: [
    AddTenantUserComponent,
    ConfirmAddTenantComponent,
    EditTenantUserComponent,
  ],
})
export class TenantSharedComponentsModule {}

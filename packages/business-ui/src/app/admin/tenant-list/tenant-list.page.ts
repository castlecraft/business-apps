import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { CreatedByActor } from '../../constants/enums';
import {
  CREATE_TENANT_FOR_ADMIN,
  TENANT_LIST_FOR_ADMIN,
} from '../../constants/url-strings';
import { DataListComponent } from '../../shared/components/data-list/data-list.component';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { ConfirmAddTenantComponent } from '../shared-components/confirm-add-tenant/confirm-add-tenant.component';

@Component({
  selector: 'app-tenant-list',
  templateUrl: './tenant-list.page.html',
  styleUrls: ['./tenant-list.page.scss'],
})
export class TenantListPage implements OnInit {
  @ViewChild(DataListComponent)
  listComponent: DataListComponent;

  accountingEntity = '';
  linkCol = 'uuid';
  fields = ['uuid', 'tenantName', 'createdById', 'createdByActor'];
  endpoint = environment.adminServerUrl + TENANT_LIST_FOR_ADMIN;
  linkPrefix = '/tenant/';
  filters = {};
  uuid = new FormControl();
  createdById = new FormControl();
  createdByActor = new FormControl();
  tenantName = new FormControl();
  uuids: Observable<any>;
  createdByIds: Observable<any>;
  createdByActors = Object.keys(CreatedByActor);
  tenantNames: Observable<any>;

  constructor(
    private readonly list: ListingService,
    private readonly dialog: MatDialog,
    private readonly router: Router,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
  }

  setFilter() {
    this.filters = {
      ...this.filters,
      uuid: this.uuid.value ? this.uuid.value : undefined,
      tenantName: this.tenantName.value ? this.tenantName.value : undefined,
      createdById: this.createdById.value ? this.createdById.value : undefined,
      createdByActor: this.createdByActor.value
        ? this.createdByActor.value
        : undefined,
    };

    Object.keys(this.filters).forEach(key => {
      this.filters[key] === undefined && delete this.filters[key];
    });
    this.listComponent.dataSource.loadItems(this.filters);
  }

  clearFilter() {
    this.uuid.setValue('');
    this.createdById.setValue('');
    this.createdByActor.setValue('');
    this.tenantName.setValue('');
    this.setFilter();
  }

  setAutoComplete() {
    this.uuids = this.uuid.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(uuid => this.list.findModels(this.endpoint, { uuid })),
      map(r => r.docs),
    );
    this.createdByIds = this.createdById.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(createdById =>
        this.list.findModels(this.endpoint, { createdById }),
      ),
      map(r => r.docs),
    );
    this.tenantNames = this.tenantName.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(tenantName =>
        this.list.findModels(this.endpoint, { tenantName }),
      ),
      map(r => r.docs),
    );
  }

  confirmTenantCreation() {
    const dialogRef = this.dialog.open(ConfirmAddTenantComponent, {
      width: '100vh',
    });
    dialogRef
      .afterClosed()
      .pipe(
        switchMap(confirm => {
          return confirm
            ? this.list.postRequest(
                environment.adminServerUrl + CREATE_TENANT_FOR_ADMIN,
                { tenantName: confirm.tenantName },
              )
            : of({});
        }),
      )
      .subscribe(created => {
        if (created?.uuid) {
          this.router
            .navigate(['/tenant', created?.uuid], { state: created })
            .then();
        }
      });
  }
}

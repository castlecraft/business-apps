import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';
import { TenantListPageRoutingModule } from './tenant-list-routing.module';
import { TenantListPage } from './tenant-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    TenantListPageRoutingModule,
    MaterialModule,
    SharedModule,
    ReactiveFormsModule,
  ],
  declarations: [TenantListPage],
})
export class TenantListPageModule {}

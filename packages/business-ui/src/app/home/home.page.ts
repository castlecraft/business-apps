import { Component, OnInit } from '@angular/core';
import { TokenService } from '../auth/token/token.service';
import { LOGGED_IN } from '../auth/token/constants';
import { SET_ITEM, StorageService } from '../auth/storage/storage.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  name: string;
  email: string;
  picture: string;
  loggedIn: boolean;
  phone: string;

  constructor(
    private readonly token: TokenService,
    private readonly store: StorageService,
  ) {}

  ngOnInit() {
    this.store.getItem(LOGGED_IN).then(loggedIn => {
      this.loggedIn = loggedIn === 'true';
      if (this.loggedIn) {
        this.loadProfile();
      }
    });

    this.store.changes.subscribe({
      next: res => {
        if (res.event === SET_ITEM && res.value?.key === LOGGED_IN) {
          this.loggedIn = res.value?.value === 'true';
          if (this.loggedIn) {
            this.loadProfile();
          }
        }
      },
      error: error => {},
    });

    if (this.loggedIn) {
      this.loadProfile();
    }
  }

  loadProfile() {
    this.token.loadProfile().subscribe({
      next: profile => {
        this.name = profile.name;
        this.email = profile.email;
        this.picture = profile.picture;
        this.phone = profile.phone_number;
      },
      error: error => {},
    });
  }
}

import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then(m => m.HomePageModule),
  },
  {
    path: 'callback',
    loadChildren: () =>
      import('./callback/callback.module').then(m => m.CallbackPageModule),
  },
  {
    path: 'chart-accounts/:accountingEntity',
    loadChildren: () =>
      import('./account/account-tree/account-tree.module').then(
        m => m.AccountTreePageModule,
      ),
  },
  {
    path: 'account/:account',
    loadChildren: () =>
      import('./account/account-form/account-form.module').then(
        m => m.AccountFormPageModule,
      ),
  },
  {
    path: 'new/account',
    loadChildren: () =>
      import('./account/account-form/account-form.module').then(
        m => m.AccountFormPageModule,
      ),
  },
  {
    path: 'list/account/:accountingEntity',
    loadChildren: () =>
      import('./account/account-list/account-list.module').then(
        m => m.AccountListPageModule,
      ),
  },
  {
    path: 'accounting-entity',
    loadChildren: () =>
      import('./account/accounting-entity/accounting-entity.module').then(
        m => m.AccountingEntityPageModule,
      ),
  },
  {
    path: 'accounting-entity/:accountingEntity',
    loadChildren: () =>
      import('./account/accounting-entity/accounting-entity.module').then(
        m => m.AccountingEntityPageModule,
      ),
  },
  {
    path: 'tenants',
    loadChildren: () =>
      import('./admin/tenant-list/tenant-list.module').then(
        m => m.TenantListPageModule,
      ),
  },
  {
    path: 'tenant/:uuid',
    loadChildren: () =>
      import('./admin/tenant-form/tenant-form.module').then(
        m => m.TenantFormPageModule,
      ),
  },
  {
    path: 'journal-entry',
    loadChildren: () =>
      import('./account/journal-entry/journal-entry.module').then(
        m => m.JournalEntryPageModule,
      ),
  },
  {
    path: 'journal-entry/:calledFrom',
    loadChildren: () =>
      import('./account/journal-entry-form/journal-entry-form.module').then(
        m => m.JournalEntryFormPageModule,
      ),
  },
  {
    path: 'journal-entry/:calledFrom/:uuid',
    loadChildren: () =>
      import('./account/journal-entry-form/journal-entry-form.module').then(
        m => m.JournalEntryFormPageModule,
      ),
  },
  {
    path: 'team/:uuid',
    loadChildren: () =>
      import('./admin/team-form/team-form.module').then(
        m => m.TeamFormPageModule,
      ),
  },
  {
    path: 'setup',
    loadChildren: () =>
      import('./setup/setup.module').then(m => m.SetupPageModule),
  },
  {
    path: 'ledger-entry',
    loadChildren: () =>
      import('./account/ledger-entry/ledger-entry.module').then(
        m => m.LedgerEntryPageModule,
      ),
  },
  {
    path: 'trial-balance',
    loadChildren: () =>
      import('./account/trial-balance/trial-balance.module').then(
        m => m.TrialBalancePageModule,
      ),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatDialogRef } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { of } from 'rxjs';
import { ListingService } from '../../../shared/components/data-list/listing.service';
import { MaterialModule } from '../../../material.module';
import { AddEntityComponent } from './add-entity.component';

describe('AddEntityComponent', () => {
  let component: AddEntityComponent;
  let fixture: ComponentFixture<AddEntityComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AddEntityComponent],
        imports: [MaterialModule, NoopAnimationsModule],
        providers: [
          {
            provide: MatDialogRef,
            useValue: {
              close: (...args) => {},
            } as MatDialogRef<AddEntityComponent>,
          },
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AddEntityComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

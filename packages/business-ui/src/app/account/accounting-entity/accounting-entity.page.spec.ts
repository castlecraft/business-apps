import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { MaterialModule } from '../../material.module';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { AccountingEntityPage } from './accounting-entity.page';

@Component({
  template: '',
  selector: 'app-mock',
})
export class MockComponent {}

describe('AccountingEntityPage', () => {
  let component: AccountingEntityPage;
  let fixture: ComponentFixture<AccountingEntityPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [AccountingEntityPage],
        imports: [
          RouterTestingModule.withRoutes([
            { path: 'accounting-entity', component: MockComponent },
          ]),
          HttpClientTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: ListingService,
            useValue: {
              findModels: (...args) => of({ docs: [] }),
              getRequest: (...args) => of({}),
              postRequest: (...args) => of({}),
            } as ListingService,
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(AccountingEntityPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

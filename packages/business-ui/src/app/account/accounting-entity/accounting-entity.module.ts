import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { AccountingEntityPageRoutingModule } from './accounting-entity-routing.module';
import { AccountingEntityPage } from './accounting-entity.page';
import { AddEntityComponent } from './add-entity/add-entity.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AccountingEntityPageRoutingModule,
    MaterialModule,
    RouterModule,
  ],
  declarations: [AccountingEntityPage, AddEntityComponent],
})
export class AccountingEntityPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { JournalEntryFormPageRoutingModule } from './journal-entry-form-routing.module';

import { JournalEntryFormPage } from './journal-entry-form.page';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    SharedModule,
    JournalEntryFormPageRoutingModule,
  ],
  declarations: [JournalEntryFormPage],
})
export class JournalEntryFormPageModule {}

import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LedgerEntryService } from './services/ledger-entry.service';

export interface ListingData {
  uuid: string;
  account: string;
  debitAmount: number;
  creditAmount: number;
  accountCurrency: string;
  debitInAccountCurrency: number;
  creditInAccountCurrency: number;
  journalEntry: string;
  remarks: string;
  accountingEntity: string;
  transactionDate: Date;
}

export interface ListResponse {
  docs: ListingData[];
  offset: number;
  length: number;
}

export class LedgerEntryDataSource extends DataSource<ListingData> {
  data: ListingData[];
  length: number;
  offset: number;
  itemSubject = new BehaviorSubject<ListingData[]>([]);
  debitTotal = new BehaviorSubject<number>(0);
  creditTotal = new BehaviorSubject<number>(0);
  acDebitTotal = new BehaviorSubject<number>(0);
  acCreditTotal = new BehaviorSubject<number>(0);

  constructor(private readonly ledgerEntryService: LedgerEntryService) {
    super();
  }

  connect(): Observable<ListingData[]> {
    return this.itemSubject.asObservable();
  }
  disconnect() {
    this.itemSubject.complete();
  }

  loadItems(query?, sortOrder?, pageIndex = 0, pageSize = 30) {
    this.ledgerEntryService
      .getLedgerEntryList(query, sortOrder, pageIndex, pageSize)
      .pipe(
        map((res: ListResponse) => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
      )
      .subscribe((items: any) => {
        this.itemSubject.next(items);
        this.calculateTotal(items);
      });
  }

  update(data) {
    this.itemSubject.next(data);
  }

  calculateTotal(data) {
    let debitTotal = 0;
    let creditTotal = 0;
    let acDebitTotal = 0;
    let acCreditTotal = 0;
    data?.forEach(element => {
      debitTotal += element.debitAmount;
      creditTotal += element.creditAmount;
      acDebitTotal += element.debitInAccountCurrency;
      acCreditTotal += element.creditInAccountCurrency;
    });
    this.debitTotal.next(debitTotal);
    this.creditTotal.next(creditTotal);
    this.acDebitTotal.next(acDebitTotal);
    this.acCreditTotal.next(acCreditTotal);
  }
}

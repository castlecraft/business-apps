import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { StorageService } from '../../../auth/storage/storage.service';
import {
  ACCESS_TOKEN,
  AUTHORIZATION,
  BEARER_HEADER_PREFIX,
} from '../../../auth/token/constants';
import { LIST_LEDGER_ENTRY } from '../../../constants/url-strings';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LedgerEntryService {
  constructor(
    private readonly storage: StorageService,
    private readonly http: HttpClient,
  ) {}

  getLedgerEntryList(query?, sortOrder?, pageNumber = 0, pageSize = 30) {
    if (!query) query = {};

    const url = environment.accountsServerUrl + LIST_LEDGER_ENTRY;
    let params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString());
    Object.keys(query).forEach(key => {
      params = params.append(key, query[key]);
    });
    if (sortOrder) {
      params = params.append('sort', sortOrder);
    }

    return this.getHeaders().pipe(
      switchMap(headers => {
        return this.http.get(url, {
          params,
          headers,
        });
      }),
    );
  }

  getHeaders() {
    return from(this.storage.getItem(ACCESS_TOKEN)).pipe(
      map(token => {
        return {
          [AUTHORIZATION]: BEARER_HEADER_PREFIX + token,
        };
      }),
    );
  }
}

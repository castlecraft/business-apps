export class LedgerEntry {
  uuid?: string;
  debitAmount: number;
  creditAmount: number;
  accountCurrency: string;
  debitInAccountCurrency: number;
  creditInAccountCurrency: number;
  journalEntry: string;
  remarks: string;
  accountingEntity: string;
  transactionDate: Date;
  accountNameEn: string;
}

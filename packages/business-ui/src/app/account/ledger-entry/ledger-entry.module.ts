import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LedgerEntryPageRoutingModule } from './ledger-entry-routing.module';

import { LedgerEntryPage } from './ledger-entry.page';
import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    LedgerEntryPageRoutingModule,
    MaterialModule,
  ],
  declarations: [LedgerEntryPage],
})
export class LedgerEntryPageModule {}

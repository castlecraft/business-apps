import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Observable } from 'rxjs';
import {
  debounceTime,
  filter,
  map,
  startWith,
  switchMap,
} from 'rxjs/operators';
import { StorageService } from '../../auth/storage/storage.service';
import { DEFAULT_ACCOUNTING_ENTITY } from '../../auth/token/constants';
import { LIST_ACCOUNTS, LIST_LEDGER_ENTRY } from '../../constants/url-strings';
import { ListingService } from '../../shared/components/data-list/listing.service';
import { environment } from '../../../environments/environment';
import { LedgerEntryDataSource } from './ledger-entry-datasource';
import { LedgerEntryService } from './services/ledger-entry.service';

@Component({
  selector: 'app-ledger-entry',
  templateUrl: './ledger-entry.page.html',
  styleUrls: ['./ledger-entry.page.scss'],
})
export class LedgerEntryPage implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: LedgerEntryDataSource;
  displayedColumns = [
    'transaction-date',
    'account',
    'debit',
    'credit',
    'ac-debit',
    'ac-credit',
  ];
  filters = {};
  account = new FormControl();
  accounts: Observable<any>;
  fromDate = new FormControl();
  toDate = new FormControl();
  ledgerEndpoint = environment.accountsServerUrl + LIST_LEDGER_ENTRY;
  accountEndpoint = environment.accountsServerUrl + LIST_ACCOUNTS;
  debitTotal: number = 0;
  creditTotal: number = 0;
  acDebitTotal: number = 0;
  acCreditTotal: number = 0;
  accountingEntity = '';

  constructor(
    private readonly list: ListingService,
    private readonly storage: StorageService,
    private readonly ledgerEntryService: LedgerEntryService,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
    this.fromDate.disable();
    this.toDate.disable();
    this.dataSource = new LedgerEntryDataSource(this.ledgerEntryService);
    this.storage.getItem(DEFAULT_ACCOUNTING_ENTITY).then(entity => {
      if (entity) {
        const defaultEntity = JSON.parse(entity);
        this.accountingEntity = defaultEntity?.uuid;
      }
    });
    this.list
      .getRequest(this.ledgerEndpoint)
      .pipe(
        map(data => data.docs),
        filter(docs => docs.length),
      )
      .subscribe({
        next: res => {
          this.dataSource.loadItems({
            accountingEntity: this.accountingEntity,
          });
          this.getTotal();
        },
        error: err => {},
      });
  }

  getTotal() {
    this.dataSource.acCreditTotal.subscribe({
      next: acCreditTotal => {
        this.acCreditTotal = acCreditTotal;
      },
    });

    this.dataSource.acDebitTotal.subscribe({
      next: acDebitTotal => {
        this.acDebitTotal = acDebitTotal;
      },
    });

    this.dataSource.creditTotal.subscribe({
      next: creditTotal => {
        this.creditTotal = creditTotal;
      },
    });

    this.dataSource.debitTotal.subscribe({
      next: debitTotal => {
        this.debitTotal = debitTotal;
      },
    });
  }

  setAutoComplete() {
    this.accounts = this.account.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(account => {
        return this.filterAccountValueChange(account);
      }),
      map(r => {
        return r.docs;
      }),
    );
  }

  filterAccountValueChange(val) {
    if (!isNaN(val)) {
      return this.list.findModels(this.accountEndpoint, {
        accountingEntity: this.accountingEntity,
      });
    }
    return this.list.findModels(this.accountEndpoint, {
      accountingEntity: this.accountingEntity,
      'accountName.en': val,
    });
  }

  getAccountOption(option) {
    if (option) {
      return option.accountNameEn;
    }
  }

  setFilter() {
    this.filters = {
      ...this.filters,
      accountingEntity: this.accountingEntity,
      account: this.account.value?.uuid ? this.account.value?.uuid : undefined,
      fromDate: this.fromDate.value
        ? new Date(this.fromDate.value).setHours(0, 0, 0, 0)
        : undefined,
      toDate: this.toDate.value
        ? new Date(this.toDate.value).setHours(23, 59, 59, 59)
        : undefined,
    };

    Object.keys(this.filters).forEach(key => {
      this.filters[key] === undefined && delete this.filters[key];
    });
    this.dataSource.loadItems(this.filters);
  }

  getUpdate(event?) {
    const sortOrder =
      this.sort?.active && this.sort?.direction
        ? `${this.sort?.active} ${this.sort?.direction}`
        : undefined;

    this.filters = {
      ...this.filters,
      accountingEntity: this.accountingEntity,
      account: this.account.value?.uuid ? this.account.value?.uuid : undefined,
      fromDate: this.fromDate.value
        ? new Date(this.fromDate.value).setHours(0, 0, 0, 0)
        : undefined,
      toDate: this.toDate.value
        ? new Date(this.toDate.value).setHours(23, 59, 59, 59)
        : undefined,
    };

    Object.keys(this.filters).forEach(key => {
      this.filters[key] === undefined && delete this.filters[key];
    });

    this.paginator.pageIndex = event?.pageIndex || 0;
    this.paginator.pageSize = event?.pageSize || 30;

    this.dataSource.loadItems(
      this.filters,
      sortOrder,
      event?.pageIndex || undefined,
      event?.pageSize || undefined,
    );
  }

  clearFilter() {
    this.account.setValue('');
    this.fromDate.setValue('');
    this.toDate.setValue('');
    this.setFilter();
  }
}

import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../material.module';

import { LedgerEntryPage } from './ledger-entry.page';

describe('LedgerEntryPage', () => {
  let component: LedgerEntryPage;
  let fixture: ComponentFixture<LedgerEntryPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [LedgerEntryPage],
        imports: [
          HttpClientTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: MatAutocomplete,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(LedgerEntryPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

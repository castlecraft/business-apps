import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../../material.module';
import { SharedModule } from '../../shared/shared.module';
import { AccountListPageRoutingModule } from './account-list-routing.module';
import { AccountListPage } from './account-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AccountListPageRoutingModule,
    SharedModule,
    RouterModule,
    MaterialModule,
    ReactiveFormsModule,
  ],
  declarations: [AccountListPage],
})
export class AccountListPageModule {}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TrialBalancePageRoutingModule } from './trial-balance-routing.module';

import { SharedModule } from '../../shared/shared.module';
import { MaterialModule } from '../../material.module';
import { TrialBalancePage } from './trial-balance.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    TrialBalancePageRoutingModule,
    MaterialModule,
  ],
  declarations: [TrialBalancePage],
})
export class TrialBalancePageModule {}

import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LIST_TRIAL_BALANCE } from '../../../constants/url-strings';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class TrialBalanceService {
  constructor(private readonly http: HttpClient) {}

  listTrialBalance(query?, sortOrder?, pageNumber = 0, pageSize = 30) {
    if (!query) query = {};

    const url = environment.accountsServerUrl + LIST_TRIAL_BALANCE;
    let params = new HttpParams()
      .set('limit', pageSize.toString())
      .set('offset', (pageNumber * pageSize).toString());
    Object.keys(query).forEach(key => {
      params = params.append(key, query[key]);
    });
    if (sortOrder) {
      params = params.append('sort', sortOrder);
    }

    return this.http.get(url, { params });
  }
}

import { Component, OnInit } from '@angular/core';
import { TrialBalanceDataSource } from './trial-balance-datasource';
import { TrialBalanceService } from './services/trial-balance.service';

@Component({
  selector: 'app-trial-balance',
  templateUrl: './trial-balance.page.html',
  styleUrls: ['./trial-balance.page.scss'],
})
export class TrialBalancePage implements OnInit {
  displayedColumns = ['name'];
  dataSource = new TrialBalanceDataSource(this.trialBalanceService);

  constructor(private readonly trialBalanceService: TrialBalanceService) {}

  ngOnInit() {
    this.dataSource.loadItems();
  }

  getUpdate(event?) {
    this.dataSource.loadItems();
  }
}

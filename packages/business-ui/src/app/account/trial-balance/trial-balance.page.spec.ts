import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../material.module';
import { TrialBalancePage } from './trial-balance.page';

describe('TrialBalancePage', () => {
  let component: TrialBalancePage;
  let fixture: ComponentFixture<TrialBalancePage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [TrialBalancePage],
        imports: [
          HttpClientTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(TrialBalancePage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

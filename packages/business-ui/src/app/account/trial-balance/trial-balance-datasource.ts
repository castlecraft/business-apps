import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TrialBalanceService } from './services/trial-balance.service';

export interface ListingData {
  uuid: string;
  accountNameEn: string;
}

export interface ListResponse {
  docs: ListingData[];
  offset: number;
  length: number;
}

export class TrialBalanceDataSource extends DataSource<ListingData> {
  data: ListingData[];
  length: number;
  offset: number;
  itemSubject = new BehaviorSubject<ListingData[]>([]);

  constructor(private readonly trialBalanceService: TrialBalanceService) {
    super();
  }

  connect(): Observable<ListingData[]> {
    return this.itemSubject.asObservable();
  }
  disconnect() {
    this.itemSubject.complete();
  }

  loadItems(query?, sortOrder?, pageIndex = 0, pageSize = 30) {
    this.trialBalanceService
      .listTrialBalance(query, sortOrder, pageIndex, pageSize)
      .pipe(
        map((res: ListResponse) => {
          this.data = res.docs;
          this.offset = res.offset;
          this.length = res.length;
          return res.docs;
        }),
      )
      .subscribe((items: any) => {
        this.itemSubject.next(items);
      });
  }

  update(data) {
    this.itemSubject.next(data);
  }
}

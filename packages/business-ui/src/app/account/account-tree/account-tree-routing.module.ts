import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AccountTreePage } from './account-tree.page';

const routes: Routes = [
  {
    path: '',
    component: AccountTreePage,
  },
];

@NgModule({
  imports: [CommonModule, FormsModule, RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AccountTreePageRoutingModule {}

import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { MatTreeFlattener } from '@angular/material/tree';
import { ActivatedRoute } from '@angular/router';
import { AccountNode } from '../../interfaces/account.node';
import { FlatNode } from '../../interfaces/flat.node';
import { AccountTreeDataSource } from './account-tree.datasource';
import { AccountTreeService } from './account-tree.service';

@Component({
  selector: 'account-tree',
  templateUrl: './account-tree.page.html',
  styleUrls: ['./account-tree.page.scss'],
})
export class AccountTreePage implements OnInit {
  displayedColumns: string[] = ['ID', 'Number', 'Name', 'Balance Type'];
  accountingEntity = '';
  private transformer = (node: AccountNode, level: number): FlatNode => {
    return {
      expandable: node.children && node.children.length > 0,
      accountName: node.accountName,
      level,
      uuid: node.uuid,
      tenantId: node.tenantId,
      accountingEntity: node.accountingEntity,
      currency: node.currency,
      parentAccount: node.parentAccount,
      accountNumber: node.accountNumber,
      parentAccountNumber: node.parentAccountNumber,
      balanceType: node.balanceType,
      classification: node.classification,
    };
  };

  treeControl = new FlatTreeControl<FlatNode, FlatNode>(
    node => node.level,
    node => node.expandable,
  );

  treeFlattener = new MatTreeFlattener<AccountNode, FlatNode>(
    this.transformer,
    node => node.level,
    node => node.expandable,
    node => node.children,
  );

  dataSource = new AccountTreeDataSource(
    this.treeControl,
    this.treeFlattener,
    this.accountTreeService,
  );

  constructor(
    private readonly accountTreeService: AccountTreeService,
    private readonly route: ActivatedRoute,
  ) {}

  hasChild = (_: number, node: FlatNode) => node.expandable;

  ngOnInit() {
    this.accountingEntity = this.route.snapshot.params.accountingEntity;
    if (this.accountingEntity) {
      this.dataSource.loadRoot(this.accountingEntity);
    }
  }
}

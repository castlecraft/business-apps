export class JournalEntry {
  uuid?: string;
  tenantId?: string;
  date?: Date;
  accountingEntity?: string;
  accounts?: JournalAccount[];
}

export class JournalAccount {
  account?: string;
  accountNameEn?: string;
  debit?: number;
  credit?: number;
  debitInAccountCurrency?: number;
  creditInAccountCurrency?: number;
  transactionLink?: string;
  remark?: string;
}

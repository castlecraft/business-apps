import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject } from 'rxjs';
import { JournalAccount } from './journal.interface';

export class JournalEntryAccountsDataSource extends DataSource<JournalAccount> {
  itemSubject = new BehaviorSubject<JournalAccount[]>([]);

  constructor() {
    super();
  }

  connect() {
    return this.itemSubject.asObservable();
  }
  disconnect() {
    this.itemSubject.complete();
  }

  loadItems(items) {
    this.itemSubject.next(items);
  }

  data() {
    return this.itemSubject.value;
  }

  update(data) {
    this.itemSubject.next(data);
  }
}

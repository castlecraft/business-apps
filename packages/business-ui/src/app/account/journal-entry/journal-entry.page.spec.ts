import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { MatAutocomplete } from '@angular/material/autocomplete';
import { MatDialog } from '@angular/material/dialog';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../../material.module';

import { JournalEntryPage } from './journal-entry.page';

describe('JournalEntryPage', () => {
  let component: JournalEntryPage;
  let fixture: ComponentFixture<JournalEntryPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [JournalEntryPage],
        imports: [
          HttpClientTestingModule,
          MaterialModule,
          NoopAnimationsModule,
        ],
        providers: [
          {
            provide: MatDialog,
            useValue: {},
          },
          {
            provide: MatAutocomplete,
            useValue: {},
          },
        ],
      }).compileComponents();

      fixture = TestBed.createComponent(JournalEntryPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

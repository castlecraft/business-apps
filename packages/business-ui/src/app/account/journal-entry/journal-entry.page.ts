import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { DataListComponent } from '../../shared/components/data-list/data-list.component';
import { environment } from '../../../environments/environment';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, switchMap } from 'rxjs/operators';
import { ListingService } from '../../shared/components/data-list/listing.service';
import {
  LIST_ACCOUNTING_ENTITIES,
  LIST_JOURNAL_ENTRIES,
} from '../../constants/url-strings';
import { StorageService } from '../../auth/storage/storage.service';
import {
  DEFAULT_ACCOUNTING_ENTITY,
  SELECTED_TENANT,
} from '../../auth/token/constants';
import { X_TENANT_ID } from '../../constants/strings';

@Component({
  selector: 'app-journal-entry',
  templateUrl: './journal-entry.page.html',
  styleUrls: ['./journal-entry.page.scss'],
})
export class JournalEntryPage implements OnInit {
  @ViewChild(DataListComponent)
  listComponent: DataListComponent;

  linkCol = 'uuid';
  fields = ['uuid', 'name', 'date'];
  journalEndpoint = environment.accountsServerUrl + LIST_JOURNAL_ENTRIES;
  entityEndpoint = environment.accountsServerUrl + LIST_ACCOUNTING_ENTITIES;
  linkPrefix = '/journal-entry/edit';
  filters = {};

  uuid = new FormControl();
  uuids: Observable<any>;
  accountingEntity = new FormControl();
  accountingEntities: Observable<any>;
  fromDate = new FormControl();
  toDate = new FormControl();
  defaultEntity = '';
  headers = {};

  constructor(
    private readonly list: ListingService,
    private readonly storage: StorageService,
  ) {}

  ngOnInit() {
    this.setAutoComplete();
    this.fromDate.disable();
    this.toDate.disable();
    this.storage.getItem(DEFAULT_ACCOUNTING_ENTITY).then(entity => {
      if (entity) {
        const defaultEntity = JSON.parse(entity);
        this.defaultEntity = defaultEntity?.name;
      }
    });
    this.storage.getItem(SELECTED_TENANT).then(tenant => {
      if (tenant) {
        const defaultTenant = JSON.parse(tenant);
        this.headers = { [X_TENANT_ID]: defaultTenant?.tenantId };
      }
    });
  }

  displayEntityName(entity?: { name: string; uuid: string }) {
    return entity?.name;
  }

  setFilter() {
    this.filters = {
      ...this.filters,
      uuid: this.uuid.value ? this.uuid.value : undefined,
      accountingEntity: this.accountingEntity.value?.uuid
        ? this.accountingEntity.value?.uuid
        : undefined,
      fromDate: this.fromDate.value
        ? new Date(this.fromDate.value).setHours(0, 0, 0, 0)
        : undefined,
      toDate: this.toDate.value
        ? new Date(this.toDate.value).setHours(23, 59, 59, 59)
        : undefined,
    };

    Object.keys(this.filters).forEach(key => {
      this.filters[key] === undefined && delete this.filters[key];
    });
    this.listComponent.dataSource.loadItems(this.filters);
  }

  clearFilter() {
    this.uuid.setValue('');
    this.accountingEntity.setValue('');
    this.fromDate.setValue('');
    this.toDate.setValue('');
    this.setFilter();
  }

  setAutoComplete() {
    this.uuids = this.uuid.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(uuid => this.list.findModels(this.journalEndpoint, { uuid })),
      map(r => r.docs),
    );

    this.accountingEntities = this.accountingEntity.valueChanges.pipe(
      startWith(''),
      debounceTime(300),
      switchMap(val => {
        return this.list.findModels(
          environment.accountsServerUrl + LIST_ACCOUNTING_ENTITIES,
          { name: val },
          undefined,
          undefined,
          5,
          this.headers,
        );
      }),
      map(r => r.docs),
    );
  }
}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AccountFormPageRoutingModule } from './account-form-routing.module';
import { AccountFormPage } from './account-form.page';

@NgModule({
  imports: [CommonModule, FormsModule, AccountFormPageRoutingModule],
  declarations: [AccountFormPage],
})
export class AccountFormPageModule {}

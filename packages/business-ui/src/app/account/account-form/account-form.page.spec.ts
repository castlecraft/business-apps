import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AccountFormPage } from './account-form.page';

describe('AccountFormPage', () => {
  let component: AccountFormPage;
  let fixture: ComponentFixture<AccountFormPage>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        imports: [RouterTestingModule],
        declarations: [AccountFormPage],
      }).compileComponents();

      fixture = TestBed.createComponent(AccountFormPage);
      component = fixture.componentInstance;
      fixture.detectChanges();
    }),
  );

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

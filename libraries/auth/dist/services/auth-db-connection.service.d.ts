import { Connection } from 'mongoose';
export declare class AuthDbConnectionService {
    readonly connection: Connection;
    constructor(connection: Connection);
}

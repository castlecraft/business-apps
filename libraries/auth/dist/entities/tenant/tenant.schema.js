"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TENANT = exports.Tenant = void 0;
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const schema = new mongoose.Schema({
    uuid: {
        type: String,
        index: true,
        default: uuid_1.v4,
        unique: true,
        sparse: true,
    },
    createdById: { type: String },
    createdByActor: { type: String },
}, { collection: 'tenant', versionKey: false, strict: false });
exports.Tenant = schema;
exports.TENANT = 'Tenant';
//# sourceMappingURL=tenant.schema.js.map
import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { TenantRole } from './tenant-role.interface';
export declare class TenantRoleService {
    readonly model: Model<TenantRole>;
    constructor(model: Model<TenantRole>);
    insertOne(params: any): Promise<TenantRole>;
    insertMany(params: any, options?: InsertManyOptions): Promise<TenantRole[]>;
    findOne(filter?: FilterQuery<TenantRole>, projections?: any, options?: QueryOptions): Promise<TenantRole>;
    find(filter?: FilterQuery<TenantRole>, projections?: any, options?: QueryOptions): Promise<TenantRole[]>;
    updateOne(filter: FilterQuery<TenantRole>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<TenantRole>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<TenantRole>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<TenantRole>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}

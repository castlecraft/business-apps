"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TENANT_ROLE = exports.TenantRole = void 0;
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const schema = new mongoose.Schema({
    uuid: { type: String, index: true, default: uuid_1.v4 },
    roleName: { type: String, index: true, unique: true, sparse: true },
}, { collection: 'tenant_role', versionKey: false });
exports.TenantRole = schema;
exports.TENANT_ROLE = 'TenantRole';
//# sourceMappingURL=tenant-role.schema.js.map
import { Model } from 'mongoose';
import { AddOn } from './add-on.interface';
export declare class AddOnService {
    private readonly addOnModel;
    constructor(addOnModel: Model<AddOn>);
    create(payload: unknown): Promise<AddOn>;
    findOne(param: any, options?: any): Promise<AddOn>;
    find(param: any, options?: any): Promise<AddOn[]>;
    deleteOne(query: any, param?: any): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    updateOne(query: any, param: any): Promise<import("mongoose").UpdateWriteOpResult>;
    list(offset: number, limit: number, search: string, query: any, sortQuery?: any): Promise<{
        docs: AddOn[];
        length: number;
        offset: number;
    }>;
}

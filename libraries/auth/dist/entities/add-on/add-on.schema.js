"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ADD_ON = exports.AddOn = void 0;
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const schema = new mongoose.Schema({
    uuid: { type: String, default: uuid_1.v4 },
    name: String,
    addOnType: String,
    amount: String,
    description: String,
    currency: String,
}, { collection: 'add_on', versionKey: false });
exports.AddOn = schema;
exports.ADD_ON = 'add_on';
//# sourceMappingURL=add-on.schema.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddOnService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const add_on_schema_1 = require("./add-on.schema");
let AddOnService = class AddOnService {
    constructor(addOnModel) {
        this.addOnModel = addOnModel;
    }
    async create(payload) {
        const site = new this.addOnModel(payload);
        return await this.addOnModel.create(site);
    }
    async findOne(param, options) {
        return await this.addOnModel.findOne(param, options);
    }
    async find(param, options) {
        return await this.addOnModel.find(param, options);
    }
    async deleteOne(query, param) {
        return await this.addOnModel.deleteOne(query, param);
    }
    async updateOne(query, param) {
        return await this.addOnModel.updateOne(query, param);
    }
    async list(offset, limit, search, query, sortQuery) {
        if (search) {
            const nameExp = new RegExp(search, 'i');
            query.$or = ['name', 'uuid'].map(field => {
                const out = {};
                out[field] = nameExp;
                return out;
            });
        }
        const data = this.addOnModel
            .find(query)
            .skip(Number(offset))
            .limit(Number(limit))
            .sort(sortQuery);
        return {
            docs: await data.exec(),
            length: await this.addOnModel.estimatedDocumentCount(query),
            offset: Number(offset) || 0,
        };
    }
};
AddOnService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(add_on_schema_1.ADD_ON)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], AddOnService);
exports.AddOnService = AddOnService;
//# sourceMappingURL=add-on.service.js.map
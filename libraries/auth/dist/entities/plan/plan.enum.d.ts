export declare enum ChargePeriod {
    daily = "daily",
    weekly = "weekly",
    monthly = "monthly",
    yearly = "yearly"
}
export declare enum CurrencyOptions {
    INR = "INR",
    USD = "USD"
}

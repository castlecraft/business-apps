"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CurrencyOptions = exports.ChargePeriod = void 0;
var ChargePeriod;
(function (ChargePeriod) {
    ChargePeriod["daily"] = "daily";
    ChargePeriod["weekly"] = "weekly";
    ChargePeriod["monthly"] = "monthly";
    ChargePeriod["yearly"] = "yearly";
})(ChargePeriod = exports.ChargePeriod || (exports.ChargePeriod = {}));
var CurrencyOptions;
(function (CurrencyOptions) {
    CurrencyOptions["INR"] = "INR";
    CurrencyOptions["USD"] = "USD";
})(CurrencyOptions = exports.CurrencyOptions || (exports.CurrencyOptions = {}));
//# sourceMappingURL=plan.enum.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PlanService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("mongoose");
const plan_schema_1 = require("./plan.schema");
let PlanService = class PlanService {
    constructor(planModel) {
        this.planModel = planModel;
    }
    async create(payload) {
        const plan = new this.planModel(payload);
        return await this.planModel.create(plan);
    }
    async findOne(param, options) {
        return await this.planModel.findOne(param, options);
    }
    async find(param, options) {
        return await this.planModel.find(param, options);
    }
    async deleteOne(query, param) {
        return await this.planModel.deleteOne(query, param);
    }
    async updateOne(query, param) {
        return await this.planModel.updateOne(query, param);
    }
    async list(offset, limit, search, query, sortQuery) {
        if (search) {
            const nameExp = new RegExp(search, 'i');
            query.$or = ['name', 'uuid'].map(field => {
                const out = {};
                out[field] = nameExp;
                return out;
            });
        }
        const data = this.planModel
            .find(query)
            .skip(Number(offset))
            .limit(Number(limit))
            .sort(sortQuery);
        return {
            docs: await data.exec(),
            length: await this.planModel.estimatedDocumentCount(query),
            offset: Number(offset) || 0,
        };
    }
};
PlanService = __decorate([
    common_1.Injectable(),
    __param(0, common_1.Inject(plan_schema_1.PLAN)),
    __metadata("design:paramtypes", [mongoose_1.Model])
], PlanService);
exports.PlanService = PlanService;
//# sourceMappingURL=plan.service.js.map
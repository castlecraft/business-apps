"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PLAN = exports.Plan = void 0;
const mongoose = require("mongoose");
const uuid_1 = require("uuid");
const schema = new mongoose.Schema({
    uuid: { type: String, default: uuid_1.v4 },
    name: String,
    numberOfSitesAllowed: Number,
    interval: Number,
    period: String,
    amount: Number,
    currency: String,
    description: String,
    razorPayId: String,
    deactivated: Boolean,
    updatedPlan: String,
}, { collection: 'plan', versionKey: false });
exports.Plan = schema;
exports.PLAN = 'Plan';
//# sourceMappingURL=plan.schema.js.map
import { Model } from 'mongoose';
import { Plan } from './plan.interface';
export declare class PlanService {
    private readonly planModel;
    constructor(planModel: Model<Plan>);
    create(payload: unknown): Promise<Plan>;
    findOne(param: any, options?: any): Promise<Plan>;
    find(param: any, options?: any): Promise<Plan[]>;
    deleteOne(query: any, param?: any): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    updateOne(query: any, param: any): Promise<import("mongoose").UpdateWriteOpResult>;
    list(offset: number, limit: number, search: string, query: any, sortQuery?: any): Promise<{
        docs: Plan[];
        length: number;
        offset: number;
    }>;
}

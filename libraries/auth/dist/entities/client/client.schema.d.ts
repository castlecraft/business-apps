import * as mongoose from 'mongoose';
export declare function randomBytes32(): string;
export declare const Client: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const CLIENT = "Client";

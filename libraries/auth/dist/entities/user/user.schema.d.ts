import * as mongoose from 'mongoose';
export declare const User: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const USER = "User";
export declare const UserModel: mongoose.Model<mongoose.Document<any, any, any>, any, any>;

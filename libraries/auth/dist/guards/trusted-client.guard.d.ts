import { CanActivate, ExecutionContext } from '@nestjs/common';
export declare class TrustedClientGuard implements CanActivate {
    canActivate(context: ExecutionContext): Promise<boolean>;
}

"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenGuard = exports.TOKEN_KEY = void 0;
const common_1 = require("@nestjs/common");
const token_cache_service_1 = require("../entities/token-cache/token-cache.service");
exports.TOKEN_KEY = 'token';
let TokenGuard = class TokenGuard {
    constructor(tokenCacheService) {
        this.tokenCacheService = tokenCacheService;
    }
    async canActivate(context) {
        const httpContext = context.switchToHttp();
        const req = httpContext.getRequest();
        const accessToken = this.getAccessToken(req);
        const cachedToken = await this.tokenCacheService.findOne({ accessToken });
        if (!cachedToken) {
            return false;
        }
        const now = new Date();
        const expiry = cachedToken.creation;
        expiry.setSeconds(expiry.getSeconds() + cachedToken.expiresIn);
        if (now < expiry) {
            req[exports.TOKEN_KEY] = cachedToken;
            return true;
        }
        return false;
    }
    getAccessToken(request) {
        if (!request.headers.authorization) {
            if (!request.query.access_token)
                return null;
        }
        return (request.query.access_token ||
            request.headers.authorization.split(' ')[1] ||
            null);
    }
};
TokenGuard = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [token_cache_service_1.TokenCacheService])
], TokenGuard);
exports.TokenGuard = TokenGuard;
//# sourceMappingURL=token.guard.js.map
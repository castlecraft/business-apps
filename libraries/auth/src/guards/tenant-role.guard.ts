import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Request } from 'express';
import { ROLES } from '../decorators/roles.decorator';
import { User } from '../entities/user/user.interface';

@Injectable()
export class TenantRoleGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const roles = this.reflector.get<string[]>(ROLES, context.getHandler());
    if (!roles) {
      return true;
    }
    const request: Request & { tenantUser: User } = context
      .switchToHttp()
      .getRequest();
    const tenantUser = request.tenantUser;
    const hasRole = () => tenantUser.roles.some(role => roles.includes(role));
    return tenantUser && tenantUser.roles && hasRole();
  }
}

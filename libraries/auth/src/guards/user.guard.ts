import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { UserService } from '../entities/user/user.service';

export const USER_KEY = 'user';
@Injectable()
export class UserGuard implements CanActivate {
  constructor(private readonly user: UserService) {}

  async canActivate(context: ExecutionContext) {
    const httpContext = context.switchToHttp();
    const req = httpContext.getRequest();
    const user = await this.user.findOne({ uuid: req.token?.user });
    if (user) {
      req[USER_KEY] = user;
      return true;
    }
    return false;
  }
}

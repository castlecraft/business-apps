import { Provider } from '@nestjs/common';
import { Connection } from 'mongoose';
import { IDENTITY_CONNECTION } from '../common';
import { AddOn, ADD_ON } from './add-on/add-on.schema';
import { AddOnService } from './add-on/add-on.service';
import { Client, CLIENT } from './client/client.schema';
import { ClientService } from './client/client.service';
import { Plan, PLAN } from './plan/plan.schema';
import { PlanService } from './plan/plan.service';
import { TenantRole, TENANT_ROLE } from './tenant-role/tenant-role.schema';
import { TenantRoleService } from './tenant-role/tenant-role.service';
import { TenantUser, TENANT_USER } from './tenant-user/tenant-user.schema';
import { TenantUserService } from './tenant-user/tenant-user.service';
import { Tenant, TENANT } from './tenant/tenant.schema';
import { TenantService } from './tenant/tenant.service';
import { TokenCache, TOKEN_CACHE } from './token-cache/token-cache.schema';
import { TokenCacheService } from './token-cache/token-cache.service';
import { User, USER } from './user/user.schema';
import { UserService } from './user/user.service';

export const IdentityEntities: Provider[] = [
  {
    provide: TOKEN_CACHE,
    useFactory: (connection: Connection) =>
      connection.model(TOKEN_CACHE, TokenCache),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: USER,
    useFactory: (connection: Connection) => connection.model(USER, User),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: TENANT,
    useFactory: (connection: Connection) => connection.model(TENANT, Tenant),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: TENANT_USER,
    useFactory: (connection: Connection) =>
      connection.model(TENANT_USER, TenantUser),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: CLIENT,
    useFactory: (connection: Connection) => connection.model(CLIENT, Client),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: TENANT_ROLE,
    useFactory: (connection: Connection) =>
      connection.model(TENANT_ROLE, TenantRole),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: PLAN,
    useFactory: (connection: Connection) => connection.model(PLAN, Plan),
    inject: [IDENTITY_CONNECTION],
  },
  {
    provide: ADD_ON,
    useFactory: (connection: Connection) => connection.model(ADD_ON, AddOn),
    inject: [IDENTITY_CONNECTION],
  },
];

export const IdentityEntityServices: Provider[] = [
  TokenCacheService,
  UserService,
  TenantRoleService,
  TenantService,
  TenantUserService,
  ClientService,
  PlanService,
  AddOnService,
];

import { Test, TestingModule } from '@nestjs/testing';
import { ClientService } from './client.service';
import { CLIENT } from './client.schema';

describe('ClientService', () => {
  let service: ClientService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ClientService,
        {
          provide: CLIENT,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<ClientService>(ClientService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

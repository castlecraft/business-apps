import { Document } from 'mongoose';
import { AddOnType } from './add-on-type.enum';

export interface AddOn extends Document {
  uuid?: string;
  name?: string;
  addOnType?: AddOnType;
  amount?: string;
  description?: string;
  currency?: string;
}

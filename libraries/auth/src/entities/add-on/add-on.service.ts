import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { ADD_ON } from './add-on.schema';
import { AddOn } from './add-on.interface';

@Injectable()
export class AddOnService {
  constructor(@Inject(ADD_ON) private readonly addOnModel: Model<AddOn>) {}

  async create(payload: unknown) {
    const site = new this.addOnModel(payload);
    return await this.addOnModel.create(site);
  }

  async findOne(param, options?) {
    return await this.addOnModel.findOne(param, options);
  }

  async find(param, options?) {
    return await this.addOnModel.find(param, options);
  }

  async deleteOne(query, param?) {
    return await this.addOnModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.addOnModel.updateOne(query, param);
  }

  async list(
    offset: number,
    limit: number,
    search: string,
    query: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.addOnModel
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.addOnModel.estimatedDocumentCount(query),
      offset: Number(offset) || 0,
    };
  }
}

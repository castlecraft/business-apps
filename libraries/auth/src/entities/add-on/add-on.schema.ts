import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    name: String,
    addOnType: String,
    amount: String,
    description: String,
    currency: String,
  },
  { collection: 'add_on', versionKey: false },
);

export const AddOn = schema;

export const ADD_ON = 'add_on';

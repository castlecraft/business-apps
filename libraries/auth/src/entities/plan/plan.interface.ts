import { Document } from 'mongoose';
import { ChargePeriod } from './plan.enum';

export interface Plan extends Document {
  uuid?: string;
  name?: string;
  numberOfSitesAllowed?: number;
  interval?: number;
  period?: ChargePeriod;
  amount?: number;
  currency?: string;
  description?: string;
  razorPayId?: string;
  deactivated: boolean;
  updatedPlan: string;
}

export interface PlanItemInterface {
  name?: string;
  amount?: number;
  currency?: string;
  description?: string;
}

export class RazorPayPlan {
  item: PlanItemInterface;
  period: string;
  interval: number;
  notes: {
    [key: string]: string;
  };
}

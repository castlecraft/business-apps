import * as mongoose from 'mongoose';
import { v4 as uuidv4 } from 'uuid';

const schema = new mongoose.Schema(
  {
    uuid: { type: String, default: uuidv4 },
    name: String,
    numberOfSitesAllowed: Number, // 0 is infinite
    interval: Number, // number of days
    period: String, // ChargePeriod
    amount: Number,
    currency: String,
    description: String,
    razorPayId: String,
    deactivated: Boolean,
    updatedPlan: String,
  },
  { collection: 'plan', versionKey: false },
);

export const Plan = schema;

export const PLAN = 'Plan';

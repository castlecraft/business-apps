
export enum ChargePeriod {
    daily = 'daily',
    weekly = 'weekly',
    monthly = 'monthly',
    yearly = 'yearly',
}

export enum CurrencyOptions {
    INR = 'INR',
    USD = 'USD',
  }
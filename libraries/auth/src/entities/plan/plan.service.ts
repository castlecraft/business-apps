import { Injectable, Inject } from '@nestjs/common';
import { Model } from 'mongoose';
import { PLAN } from './plan.schema';
import { Plan } from './plan.interface';

@Injectable()
export class PlanService {
  constructor(@Inject(PLAN) private readonly planModel: Model<Plan>) {}

  async create(payload: unknown) {
    const plan = new this.planModel(payload);
    return await this.planModel.create(plan);
  }

  async findOne(param, options?) {
    return await this.planModel.findOne(param, options);
  }

  async find(param, options?) {
    return await this.planModel.find(param, options);
  }

  async deleteOne(query, param?) {
    return await this.planModel.deleteOne(query, param);
  }

  async updateOne(query, param) {
    return await this.planModel.updateOne(query, param);
  }

  async list(
    offset: number,
    limit: number,
    search: string,
    query: any,
    sortQuery?: any,
  ) {
    if (search) {
      // Search through multiple keys
      // https://stackoverflow.com/a/41390870
      const nameExp = new RegExp(search, 'i');
      query.$or = ['name', 'uuid'].map(field => {
        const out = {};
        out[field] = nameExp;
        return out;
      });
    }

    const data = this.planModel
      .find(query)
      .skip(Number(offset))
      .limit(Number(limit))
      .sort(sortQuery);

    return {
      docs: await data.exec(),
      length: await this.planModel.estimatedDocumentCount(query),
      offset: Number(offset) || 0,
    };
  }
}

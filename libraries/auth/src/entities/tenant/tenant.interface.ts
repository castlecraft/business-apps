import { Document } from 'mongoose';

export interface Tenant extends Document {
  uuid?: string;
  createdById?: string;
  createdByActor?: CreatedByActor;
  [key: string]: any;
}

export enum CreatedByActor {
  User = 'User',
  Client = 'Client',
}

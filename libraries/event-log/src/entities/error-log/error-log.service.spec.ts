import { Test, TestingModule } from '@nestjs/testing';
import { ERROR_LOG } from './error-log.schema';
import { ErrorLogService } from './error-log.service';

describe('ErrorLogService', () => {
  let service: ErrorLogService;
  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ErrorLogService,
        {
          provide: ERROR_LOG,
          useValue: {}, // provide mock values
        },
      ],
    }).compile();
    service = module.get<ErrorLogService>(ErrorLogService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

import { Inject, Injectable } from '@nestjs/common';
import { Connection } from 'mongoose';
import { EVENTLOG_CONNECTION } from '../common';

@Injectable()
export class EventsDbConnectionService {
  constructor(
    @Inject(EVENTLOG_CONNECTION)
    public readonly connection: Connection,
  ) {}
}

import { FilterQuery, InsertManyOptions, Model, QueryOptions } from 'mongoose';
import { ErrorLog } from './error-log.interface';
export declare class ErrorLogService {
    readonly model: Model<ErrorLog>;
    constructor(model: Model<ErrorLog>);
    insertOne(params: any): Promise<ErrorLog>;
    insertMany(params: any, options?: InsertManyOptions): Promise<ErrorLog[]>;
    findOne(filter?: FilterQuery<ErrorLog>, projections?: any, options?: QueryOptions): Promise<ErrorLog>;
    find(filter?: FilterQuery<ErrorLog>, projections?: any, options?: QueryOptions): Promise<ErrorLog[]>;
    updateOne(filter: FilterQuery<ErrorLog>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    updateMany(filter: FilterQuery<ErrorLog>, update: any, options?: QueryOptions): Promise<import("mongoose").UpdateWriteOpResult>;
    deleteOne(filter: FilterQuery<ErrorLog>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
    deleteMany(filter: FilterQuery<ErrorLog>, options?: QueryOptions): Promise<{
        ok?: number;
        n?: number;
    } & {
        deletedCount?: number;
    }>;
}

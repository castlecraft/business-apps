"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EventsEntityServices = exports.EventsEntities = void 0;
const common_1 = require("../common");
const domain_event_schema_1 = require("./domain-event/domain-event.schema");
const domain_event_service_1 = require("./domain-event/domain-event.service");
const error_log_schema_1 = require("./error-log/error-log.schema");
const error_log_service_1 = require("./error-log/error-log.service");
exports.EventsEntities = [
    {
        provide: error_log_schema_1.ERROR_LOG,
        useFactory: (connection) => connection.model(error_log_schema_1.ERROR_LOG, error_log_schema_1.ErrorLog),
        inject: [common_1.EVENTLOG_CONNECTION],
    },
    {
        provide: domain_event_schema_1.DOMAIN_EVENT,
        useFactory: (connection) => connection.model(domain_event_schema_1.DOMAIN_EVENT, domain_event_schema_1.DomainEvent),
        inject: [common_1.EVENTLOG_CONNECTION],
    },
];
exports.EventsEntityServices = [
    domain_event_service_1.DomainEventService,
    error_log_service_1.ErrorLogService,
];
//# sourceMappingURL=index.js.map
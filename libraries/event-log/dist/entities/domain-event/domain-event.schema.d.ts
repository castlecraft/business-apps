import * as mongoose from 'mongoose';
export declare function randomBytes32(): string;
export declare const DomainEvent: mongoose.Schema<mongoose.Document<any, any, any>, mongoose.Model<mongoose.Document<any, any, any>, any, any>, undefined, {}>;
export declare const DOMAIN_EVENT = "DomainEvent";

import { Connection } from 'mongoose';
export declare class EventsDbConnectionService {
    readonly connection: Connection;
    constructor(connection: Connection);
}
